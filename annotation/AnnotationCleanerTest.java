package annotation;

import java.io.*;
import java.util.*;

import edu.columbia.opinion.process.*;

public class AnnotationCleanerTest {
	
	/**
	  /proj/nlp/users/sara/agreement/res/annotations/train/Chris.csv /proj/nlp/users/sara/agreement/res/annotations/train/Greg.csv /proj/nlp/users/sara/agreement/res/annotations/train/Spencer.csv 
	  /proj/nlp/users/sara/agreement/res/annotations/dev/Chris.csv /proj/nlp/users/sara/agreement/res/annotations/dev/Greg.csv /proj/nlp/users/sara/agreement/res/annotations/dev/Spencer.csv 
	  /proj/nlp/users/sara/agreement/res/annotations/test/Greg-Test.csv /proj/nlp/users/sara/agreement/res/annotations/test/Chris-Test.csv
	 * @param args
	 */
	public static void main (String args[]) {
		try {
			AnnotationCleaner cleaner = new AnnotationCleaner();
			
			for (int i = 0; i < args.length; i++) {
			
				System.out.println(args[i]);
				
				List<String> lines = Utils.readLines(args[i]);
				List<String> newLines = new ArrayList<String>();
				
				PrintWriter out = new PrintWriter(args[i],"UTF-8");
				
				for (String line : lines) {
					String newLine = cleaner.cleanJSON(line);
					newLine = cleaner.fixSubTar(newLine);
					out.print(newLine + "\n");
					newLines.add(newLine);
				}
				out.close();
				
				System.out.println("SPLITTING TRAIN/DEV/TEST");
				
				if (new File(args[i]).getName().indexOf("Test") >= 0) cleaner.printTest(newLines, "/proj/nlp/users/sara/agreement/res/annotations", new File(args[i]).getName());
				else cleaner.SplitDev(Utils.readLines("/proj/nlp/users/sara/agreement/res/entries.dev.txt"), newLines, 
						"/proj/nlp/users/sara/agreement/res/annotations", new File(args[i]).getName());
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
}
