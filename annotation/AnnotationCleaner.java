package annotation;


import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import blog.threaded.livejournal.Blog;
import blog.threaded.livejournal.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AnnotationCleaner {
	
	public AnnotationCleaner() {		
	}
	
	public String cleanJSON(String annotations) {
		
			if (annotations.isEmpty()) return annotations;
			String [] parts = annotations.split(";", 3);
			
			if (!parts[2].startsWith("[{"))
				parts = annotations.split(";", 4);

			String annotation = "";
			
			if (parts.length == 4) {
				annotation = parts[3];
			}
			else {
				annotation = parts[2];
			}
			
			if (annotation.isEmpty()) return annotations;
			
			JSONArray a = null;
			
			try {
	    		a = JSONArray.fromObject(annotation);
	    	} catch (Exception e) {
	    		System.err.println(annotation);
	    		e.printStackTrace();
	    		System.exit(0);
	    	}
			
			for (int i = 0; i < a.size(); i++) {

				JSONObject o = a.getJSONObject(i);
		
		        String subject = removeDups(o.getString("subject"));
		        String target = removeDups(o.getString("target"));
		        
		        if (!o.getString("subject").equals(subject)) System.out.println("N: " + subject + "\nO: " + o.getString("subject"));
		        if (!o.getString("target").equals(target)) System.out.println("N: " + target + "\nO: " + o.getString("target"));
		        o.element("subject", subject);
		        o.element("target", target);
		        a.set(i, o);
			}
			if (parts.length == 3 ) return parts[0] + ";" + parts[1] + ";" + a.toString();
			return parts[0] + ";" + parts[1] + ";" + parts[2] + ";" + a.toString();
	}
	
	/**
	 * Clean up annotations to:
	 * a. preserve special characters
	 * b. keep order of sentences in post
	 * c. make sure subject and target refer to correct text
	 * @param annotations
	 * @return
	 */
	public String fixSubTar(String annotations) {
		
		if (annotations.isEmpty()) return annotations;
		String [] parts = annotations.split(";", 3);
		
		if (parts[2].startsWith("[{")) {
			if (parts[0].indexOf("livejournal") >= 0) {
				try {
					annotations = parts[0] + ";" + parts[1] + ";/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/" + 
						parts[0].substring(parts[0].indexOf(".com/")+5,parts[0].indexOf(".html")) + ".xml;" + parts[2];
				} catch (Exception e) {
					System.err.println("[AnnotationCleaner.fixSubTar] " + parts[0]);
					e.printStackTrace();
				}
			}
			else return annotations;
		}
		parts = annotations.split(";", 4);

		parts[2] = parts[2].replace("/proj/nlp/users/orb/unannotated-entries/", "/proj/nlp/fluke/SCIL/corpora/livejournal/agreement-batch1/");
		
		Entry entry = getEntry(parts[2]);
				
		String annotation = parts[3];
		
		if (annotation.isEmpty() || entry == null) return annotations;
		
		JSONArray a = null;
		
		try {
    		a = JSONArray.fromObject(annotation);
    	} catch (Exception e) {
    		System.err.println(annotation);
    		e.printStackTrace();
    		System.exit(0);
    	}
		
		for (int i = 0; i < a.size(); i++) {

			JSONObject o = a.getJSONObject(i);
	
	        String subject = normalizeSentence(o.getString("subject")).trim();
	        String target = normalizeSentence(o.getString("target")).trim();
	        
	        String sId = parts[2].indexOf("livejournal") >= 0 ? entry.getURL() + "?thread=" + o.getString("subjectId").substring(1) + "#" + o.getString("subjectId")
	        		: o.getString("subjectId");
	        String tId = parts[2].indexOf("livejournal") >= 0 ? entry.getURL() + "?thread=" + o.getString("targetId").substring(1) + "#" + o.getString("targetId")
	        		: o.getString("targetId");
	        
	        String subjectText = normalizeSentence(o.getString("subjectId").equals("root") ? entry.getEntryText() : 
	        	entry.getComment(sId).getCommentText());
	        String targetText = normalizeSentence(o.getString("targetId").equals("root") ? entry.getEntryText() : 
	        	entry.getComment(tId).getCommentText());

	        subject = fixString(subject, subjectText);
	        target = fixString(target, targetText);
	        if ((subjectText.indexOf(normalizeSentence(subject)) >= 0 && targetText.indexOf(normalizeSentence(target)) >= 0)
	        		|| (unordered(subject,subjectText) && unordered(target,targetText))) {
	        	o.element("subject", subject);
	        	o.element("target", target);
	        	//System.out.println("OUT OF ORDER");
	        }
	        else if ((subjectText.indexOf(normalizeSentence(target)) >= 0 && targetText.indexOf(normalizeSentence(subject)) >= 0) 
	        		|| (unordered(target,subjectText) && unordered(subject,targetText))) {
	        	o.element("subject", target);
	        	o.element("target", subject);
	        	
	        	// flip ids
	        	String subjectId = o.getString("subjectId");
		        o.element("subjectId", o.getString("targetId"));
		        o.element("targetId", subjectId);
		        System.out.println("FLIP");
	        }
	        else {
	        	System.err.println("Error");
	        	System.err.println("S: " + normalizeSentence(subject) + ", Entry: " + subjectText);
	        	System.err.println("T: " + normalizeSentence(target) + ", Entry: " + targetText + "\n");	        	
	        }
	        a.set(i, o);
		}
		if (parts.length == 3 ) return parts[0] + ";" + parts[1] + ";" + a.toString();
		return parts[0] + ";" + parts[1] + ";" + parts[2] + ";" + a.toString();
	}
	
	private boolean unordered(String string, String postSentence) {
		String [] stringSentences = string.substring(2,string.length()-2).split("\",\"");
		
		for (String s : stringSentences) {
			if (postSentence.indexOf(s) == -1) return false;
		}
		return true;
	}
	
	/**
	 * Place string in regex to try and match it
	 * 
	 * @param string
	 * @param post
	 * @return
	 */
	private boolean toUTF8(String string, String postSentence) {
		
		String [] sentences = string.split(SENT_DELIMITERS);
		
		for (String sentence : sentences) {
			
			Pattern p = Pattern.compile(sentence.replaceAll("(?=[]\\[+&|!(){}^\"~*:\\\\-])", "\\\\").replaceAll("\\?+", ".*"));
			Matcher m = p.matcher(postSentence);
			
			if (m.find()) {
				return true;
			}
		}
		return false;
	}
	
	public static final String SENT_DELIMITERS = "(?<=[.!?])\\s|\\n+";
	
	private String fixString(String string, String post) {
		if (post.indexOf(string) >= 0) return "[\"" + string + "\"]";
		
		String ordered = "\"";
		
		String [] postSentences = post.split(SENT_DELIMITERS);
		String postDelimited = "\"";
		
		boolean found = false;
		
		for (String s : postSentences) {
			if (string.indexOf(s) >= 0 || (string.indexOf("?") >= 0 && toUTF8(string,s))) {
				ordered += s + "\",\"";
				string = string.replace(s, "");
				found = true;
			}
			postDelimited += s + "\",\"";
		}
		
		if (!found) return "[\"" + string + "\"]"; 
		
		ordered = ordered.substring(0,ordered.length()-2);
		postDelimited = postDelimited.substring(0,postDelimited.length()-2);
		
		return "[" + ordered + "]";
		//String sentences = string.split("\",\"");
		
		/*System.err.println("Sentence: " + ordered);
		System.err.println("Post:     " + post);
		System.err.println("------");*/
		//return string;
	}
	
	private String normalizeSentence(String s) {
		s = s.replaceAll("^\\[\"|\",\"|\"\\]$", " ");
		s = s.replaceAll("`", "'");
		s = s.replaceAll("`", "'");
		s = s.replaceAll("\"", "''");
		return s.trim();
	}
	
	private Entry getEntry(String id) {
		Blog blog = null;
        
        try {
        	if (new File(id).exists()) blog = Blog.processBlog(id);
        } catch(Exception e) {
            blog = null;
        }

        if(blog == null || !blog.getEntries().hasNext()) {
            System.out.println("CAN'T READ ENTRY WITH ID " + id);
            return null;
        } else {
        	System.out.println("Read entry with ID " + id);
        }
        
        // fetch corresponding blog for text extraction
        return
            (blog.threaded.livejournal.Entry)blog.getEntry((String)blog.getEntries().next());
	}
	
	private String removeDups(String string) {
		HashSet<String> unique = new HashSet<String>();
		
		String [] sentences = string.split("^\\[\"|\",\"|\"\\]$");
		List<String> keep = new ArrayList<String>();
		
		for (String sentence : sentences) {
			if (unique.contains(sentence)) {
				keep.remove(sentence);
				unique.remove(sentence);
			}
			else {
				unique.add(sentence);
				keep.add(sentence);
			}
		}
		
		String fixed = "";
		
		for (String sentence : keep) {
			if (sentence.isEmpty()) continue;
			fixed += "\",\"" + sentence;
		}
		return "[\"" + fixed.substring(3) + "\"]";
	}
				
	public String clean(String ann) throws IOException {
		int prev = 0;
		int start = 0;
		String newAnn = ann;
		boolean check = true;
		String newArc;
		while (start<ann.length() && check == true) {
			start = ann.indexOf("[", prev)+1;		
			if (ann.substring(start, ann.length()-1).contains("[")) {
				String arc = ann.substring(start, ann.indexOf("]", start));
				if (arc.contains("[")) {
					String trueArc = ann.substring(ann.indexOf("[", start+1)+1, ann.indexOf("]",start));
					arc = trueArc;
				}
				String[] messages = arc.split("\",\"");
				int size = messages.length;
				if (messages[0].length()>1 && messages[0].substring(0,1).equals("\"")) {
					messages[0] = messages[0].substring(1);
					}
				
				if (messages[size-1].length()>2 &&
						messages[size-1].substring(messages[size-1].length()-1).equals("\"")) {
					messages[size-1] = messages[size-1].substring(0,messages[size-1].length()-1);}
				for (int i=0; i<size-1; i++) {
					//System.out.println(messages[i]);
					//System.out.println("~~~~~~~~~~~~~~~~~~~~~~");
					//System.out.println("CURRENT: " + messages[i]);				
					int instances = 1;
					for (int j=i+1; j<size; j++){
						if (messages[i].equals(messages[j])) {
							instances++;
							//System.out.println("J DELETED: " + messages[j]);
							messages[j] = "";
						}
					}
					if (instances%2 == 0)
						//System.out.println("I DELETED: " + messages[i]);
						messages[i] = "";
						
				}
				if (!messages[0].equals("")) {
					newArc = "\"" + messages[0]; }
				else {
					newArc = messages[0];
				}
				for (int i=1;i<size;i++) {
					if (!messages[i].equals("")) {
						newArc = newArc + "," + messages[i];
						}
				
					else {
						newArc = newArc + messages[i];
					}
				}
				newArc = newArc + "\"";
				newAnn = newAnn.replace(arc, newArc);
				
				if (!arc.equals(newArc)) {
					System.out.println("OLD " + arc);
					System.out.println("NEW " + newArc);
				}
				prev = start+1;
			}	
			else {
				check = false;
			}
		}
		return newAnn;
	}
	
	public void SplitDev(List<String> devFiles, List<String> annotations, String output, String fileName) throws Exception {
		
		HashSet<String> files = new HashSet<String>();
		
		// add dev files to a hashmap
		for (String file : devFiles) {
			files.add(file);
		}
		
		PrintWriter dev = new PrintWriter(output + "/dev/" + fileName,"UTF-8");
		PrintWriter train = new PrintWriter(output + "/train/" + fileName,"UTF-8");
		
		for (String annotation : annotations) {
			String [] parts = annotation.split(";", 4);
			
			if (files.contains(parts[2])) {
				dev.println(annotation);
			}
			else {
				train.println(annotation);
			}
		}
		dev.close();
		train.close();
	}
	
	public void printTest(List<String> annotations, String output, String fileName) throws Exception {
		
		PrintWriter test = new PrintWriter(output + "/test/" + fileName,"UTF-8");
		
		for (String annotation : annotations) {
			test.println(annotation);
		}
		test.close();
	}
}

	//Check first array element against others
	//Remove duplicates