/**
 * 
 */
package agreement.crf;

import cc.mallet.grmm.learning.ACRF;
import cc.mallet.grmm.learning.ACRF.UnrolledGraph;
import cc.mallet.grmm.types.Variable;
import cc.mallet.grmm.util.LabelsAssignment;
import cc.mallet.types.Alphabet;
import cc.mallet.types.AugmentableFeatureVector;
import cc.mallet.types.FeatureVector;
import cc.mallet.types.FeatureVectorSequence;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author sara
 *
 */
public class ResponseTemplate extends ACRF.SequenceTemplate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 865609459963300798L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	int factor = 0;
	ArrayList<String []> ids = null;
	
	public ResponseTemplate(int factor) {
		this.factor = factor;
	}
	
	private String [] findId(FeatureVector fv) {
		
		String pattern = "^WORD=/.*;.*;.*$";
		Alphabet dict = fv.getAlphabet();
		for (int i = 0; i < dict.size(); i++) {
			String feature = dict.lookupObject(i).toString();
			if (fv.contains(dict.lookupObject(i)) && feature.matches(pattern)) return feature.substring(5).split(";");
		}
		return null;
	}
	
	private HashMap<String,Integer> threadStructure(FeatureVectorSequence fvs) {
		HashMap<String,Integer> idList = new HashMap<String,Integer> ();
		ids = new ArrayList<String []>();
		
		for (int i = 0; i < fvs.size(); i++) {
			String [] id = findId(fvs.get(i));
			ids.add(id);
			idList.put(id[2],i);
		}
		
		return idList;
	}
	
	private ACRF.UnrolledVarSet addClique(UnrolledGraph graph, FeatureVectorSequence fvs, LabelsAssignment lblseq, int i, int j) {
        
		Variable v1 = lblseq.varOfIndex (i, factor);
		Variable v2 = lblseq.varOfIndex (j, factor);
		
        Variable[] vars = new Variable[] { v1, v2 };
        
        FeatureVector fv1 = fvs.getFeatureVector (i);
       // FeatureVector fv2 = fvs.getFeatureVector (j);
        
        AugmentableFeatureVector afv = new AugmentableFeatureVector (fv1.getAlphabet(),true);
        
        afv.add(fv1);
        
        /*if (source) {
        	afv.add(fv2);
        }
        if (addSharedFeatures) {
        	afv.add
        }
        */
        
	   // System.err.println("Making Connection: " + Arrays.toString(findId(fv1)) + " , " + Arrays.toString(findId(fv2)));
        
        //
        //afv.add(fv1);
        //afv.add(fv2);
        
        assert v1 != null : "Couldn't get label factor "+factor+" time "+i;
        assert v2 != null : "Couldn't get label factor "+factor+" time "+ j;

        ACRF.UnrolledVarSet clique = new ACRF.UnrolledVarSet (graph, this, vars, afv);
        return clique;
	}
	
	@Override
	protected void addInstantiatedCliques(UnrolledGraph graph,
			FeatureVectorSequence fvs, LabelsAssignment lblseq) {
		
		HashMap<String, Integer> thread = threadStructure(fvs);
		
		//System.err.println("Processing list of size "+lblseq.maxTime());
			
		for (int i = 1; i < lblseq.maxTime(); i++) {
			
			String [] id = ids.get(i); //findId(fvs.get(i));
			int j = thread.get(id[1]);
	        
	        //System.out.println("Add Clique: " + i + " " + j);
	        graph.addClique(addClique(graph, fvs, lblseq, i, j));
	   }
	}
	
	//@Override
	/*protected void addInstantiatedCliques(UnrolledGraph graph,
			FeatureVectorSequence fvs, LabelsAssignment lblseq) {
		
		String [][] ids = idList(fvs);
		
		blog.Blog blog = BlogReader.readBlog(ids[0][0],ids[0][0]);
        
        
        blog.Entry entry =
                (blog.Entry)blog.getEntry((String)blog.getEntries().next());
		
        Object [] orderedComments = entry.getSortedComments();
        
		System.err.println("Processing list of size "+lblseq.maxTime());
		for (int i = 0; i < lblseq.maxTime() - 1; i++) {
			
			String [] id1 = ids[i];
			
			String id1S = id1[1];
			String id1T = id1[2];
	           
            String parentId1S = id1S.equals("root") ? "NA" : ((blog.threaded.Comment)entry.getComment(id1S)).getParentId();
            String parentId1T = id1T.equals("root") ? "NA" : ((blog.threaded.Comment)entry.getComment(id1T)).getParentId();
            //if (id1S.equals("root")) id1S = entry.getURL().substring(0,3);
           // if (id1T.equals("root")) id1T = entry.getURL().substring(0,3);
            if (!isSiblingOrChild(entry, orderedComments,parentId1S,id1S,parentId1T,id1T)) continue;
            
            // start with i + 2 because i + 1 is part of the linear chain
	        for (int j = i+2; j < lblseq.maxTime(); j++) {
	        	
				String [] id2 = ids[j];
				
				String id2S = id2[1];
				String id2T = id2[2];
		           
	            String parentId2S = id2S.equals("root") ? "NA" : ((blog.threaded.Comment)entry.getComment(id2S)).getParentId();
	            String parentId2T = id2T.equals("root") ? "NA" : ((blog.threaded.Comment)entry.getComment(id2T)).getParentId();
	            //if (id2S.equals("root")) id2S = entry.getURL().substring(0,3);
	            //if (id2T.equals("root")) id2T = entry.getURL().substring(0,3);
	        
	            // if two of the ids are the same and the other two ids share a parent, add connection
	            // eg AB BD -- B & B are same, if A & D have same parent add
	            if (id1S.equals(id2S) && parentId1T.equals(parentId2T) && isSiblingOrChild(entry, orderedComments,parentId2S,id2S,parentId2T,id2T) 
	            		 && isSequential(orderedComments,id1T,id2T)) {     	
	            	System.out.println("Add Clique: " + Arrays.toString(id1) + "," + Arrays.toString(id2));
	            	graph.addClique(addClique(graph, fvs, lblseq, i, j));
	            }
	        }
		}
	}*/
	
	/*private boolean isSiblingOrChild(Entry entry, Object [] comments, String id1Parent, String id1, String id2Parent, String id2) {
		
		//child
		if (id2Parent.equals(id1) || id1.equals("root") && entry.getComment(id2Parent) == null) return true;
		// ancestor other than parent
		if (id1.equals("root")) return false;
		
		HashSet<Object> siblings = new HashSet<Object>();
		
		for (int i = 0; i < comments.length; i++) {
			if (((Comment)comments[i]).getParentId().equals(id1Parent) || 
					(id1Parent.equals("root") && entry.getComment(((Comment)comments[i]).getParentId()) == null)) {
				siblings.add(comments[i]);
			}
		}
		
		boolean found1 = false;
		boolean found2 = false;
		
		for (Object comment : siblings) {
			if (((Comment)comment).getID().equals(id1)) found1 = true;
			if (((Comment)comment).getID().equals(id2)) found2 = true;
			if (found1 && found2) return true;
		}
		return false;
	}*/
	
	/*private boolean isSequential(Object [] comments, String id1, String id2) {
		
		
		for (int i = 0; i < comments.length; i++) {
			if (((Comment)comments[i]).getID().equals(id1)) {
				int j = i+1;
				
				for (; j < comments.length; j++) {
					if (((Comment)comments[j]).getParentId().equals(((Comment)comments[i]).getParentId()))
						break;
				}
				if (j < comments.length && ((Comment)comments[j]).getID().equals(id2)) return true;
				return false;
			}
		}
		return false;
	}*/
}
