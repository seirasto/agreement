/**
 * 
 */
package agreement.crf;

import cc.mallet.grmm.learning.ACRF;
//import cc.mallet.grmm.learning.ACRF.SequenceTemplate;
import cc.mallet.grmm.learning.ACRF.UnrolledGraph;
import cc.mallet.grmm.types.Variable;
import cc.mallet.grmm.util.LabelsAssignment;
import cc.mallet.types.Alphabet;
//import cc.mallet.types.AugmentableFeatureVector;
import cc.mallet.types.FeatureVector;
import cc.mallet.types.FeatureVectorSequence;
//import java.util.Arrays;
import java.util.HashMap;

/**
 * @author sara
 *
 */
public class SameSentenceTemplate extends ACRF.SequenceTemplate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 865609459963300798L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	int factor = 0;
	
	public SameSentenceTemplate(int factor) {
		this.factor = factor;
	}
	
	private String [] findId(FeatureVector fv) {
		
		String pattern = "^WORD=/.*;.*_[0-9]+;.*[0-9]+$";
		Alphabet dict = fv.getAlphabet();
		for (int i = 0; i < dict.size(); i++) {
			String feature = dict.lookupObject(i).toString();
			if (fv.contains(dict.lookupObject(i)) && feature.matches(pattern)) return feature.split(";");
		}
		return null;
	}

	private HashMap<String,HashMap<String,Integer>> orderById(FeatureVectorSequence fvs) {
		
		HashMap<String,HashMap<String,Integer>> ordered = new HashMap<String,HashMap<String,Integer>>();
		
		for (int i = 0; i < fvs.size(); i++) {
			FeatureVector fv1 = fvs.getFeatureVector (i);
			String [] sId = findId(fv1);
			
	        if (!ordered.containsKey(sId[1])) {
	        	ordered.put(sId[1], new HashMap<String,Integer>());
	        }
	        ordered.get(sId[1]).put(sId[2], i);
		}
		return ordered;
	}
	
	private ACRF.UnrolledVarSet addClique(UnrolledGraph graph, FeatureVectorSequence fvs, LabelsAssignment lblseq, int i, int j) {
        
		Variable v1 = lblseq.varOfIndex (i, factor);
		Variable v2 = lblseq.varOfIndex (j, factor);
		
        Variable[] vars = new Variable[] { v1, v2 };
        
        FeatureVector fv1 = fvs.getFeatureVector (i);
        //FeatureVector fv2 = fvs.getFeatureVector (j);
        
	   // System.err.println("Making Connection: " + Arrays.toString(findId(fv1)) + " , " + Arrays.toString(findId(fv2)));
        
        /*AugmentableFeatureVector afv = new AugmentableFeatureVector (fv1.getAlphabet(),true);
        afv.add(fv1);
        afv.add(fv2);*/
        
        assert v1 != null : "Couldn't get label factor "+factor+" time "+i;
        assert v2 != null : "Couldn't get label factor "+factor+" time "+ j;

        ACRF.UnrolledVarSet clique = new ACRF.UnrolledVarSet (graph, this, vars, fv1);
        return clique;
	}
	
	@Override
	protected void addInstantiatedCliques(UnrolledGraph graph,
			FeatureVectorSequence fvs, LabelsAssignment lblseq) {
		
		//System.err.println("Processing list of size "+lblseq.maxTime());
		
		// generate hash to iterate easily
		HashMap<String,HashMap<String,Integer>> ordered = orderById(fvs);
		
		for (int i = 0; i < lblseq.maxTime() - 1; i++) {
			//Variable v1 = lblseq.varOfIndex (i, factor);
			
			FeatureVector fv1 = fvs.getFeatureVector (i);
			String [] sId = findId(fv1);
			
			String sIdS = sId[1].substring(0,sId[1].indexOf("_"));
			String sIdT = sId[2].substring(0,sId[2].indexOf("_"));
			int sIdPosS = Integer.valueOf(sId[1].substring(sId[1].indexOf("_")+1));
	        int sIdPosT = Integer.valueOf(sId[2].substring(sId[2].indexOf("_")+1));
	        
	        //for A1B1: keep A1B2, A2B1, A2B2
	        if (ordered.containsKey((sIdS + "_" + sIdPosS)) && ordered.get(sIdS + "_" + sIdPosS).containsKey(sIdT + "_" + (sIdPosT+1)))
	        	graph.addClique(addClique(graph,fvs,lblseq,i,ordered.get(sIdS + "_" + sIdPosS).get(sIdT + "_" + (sIdPosT+1))));
	        if (ordered.containsKey((sIdS + "_" + (sIdPosS+1))) && ordered.get(sIdS + "_" + (sIdPosS+1)).containsKey(sIdT + "_" + sIdPosT))
	        	graph.addClique(addClique(graph,fvs,lblseq,i,ordered.get(sIdS + "_" + (sIdPosS+1)).get(sIdT + "_" + sIdPosT)));
	        if (ordered.containsKey((sIdS + "_" + (sIdPosS+1))) && ordered.get(sIdS + "_" + (sIdPosS+1)).containsKey(sIdT + "_" + (sIdPosT+1)))
	        	graph.addClique(addClique(graph,fvs,lblseq,i,ordered.get(sIdS + "_" + (sIdPosS+1)).get(sIdT + "_" + (sIdPosT+1))));
		}
	}
}
