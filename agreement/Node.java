package agreement;

import edu.columbia.opinion.io.*;
import edu.stanford.nlp.trees.*;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import processing.*;

public class Node {
	
    //start (0), middle (1), or end (2)
    public static final int START = 0;
    public static final int MIDDLE = 1;
    public static final int END = 2;
    
    public static final String NEGATION = "not|no|nobody|nothing|neither|nowhere|never|hardly|scarcely|barely|n't";
    public static final String AGREEMENT = "acknowledge|admit|allow|comply|concede|concur|grant|recognize|grant|recognize|consent|accede|okay|ok|permit|assent";
    public static final String DISAGREEMENT = "disagree|deny|disapprove|dispute|dissent|contradict|oppose|refuse|reject|repudiate|resist|protest|decline|differ";
    
    private String _id;
    List<String> _body;    
    List<String> _pos;
    private String _author;
    private int _depth;
   
    private int _approxPosition;
	private int _exactPosition;
	private int _responseCount;

	List<ResultSentence> _sentiment;	
	List<ResultSentence> _polarity;

	Tree _parse;
	
	private static Contractions _contractions;

	public static void main(String [] args) {
	}
	
    public String toString() {
        return _id + ": " + _body;
    }
    
    /*public Node(String id, String body, String author, 
    		int depth) {
    	this(id, body, author, depth, -1, -1, -1, null, null, null);
    }*/
    
    public Node(String id, String body, String author, 
    		int depth, int approxPosition, int exactPosition, int responseCount, 
    		String sentiment, String polarity, String pos) {
    	_id = id;
        _body = new ArrayList<String>();
        _body.add(body);
        stripQuotesList(_body);
        _author = author;
        _depth = depth;
        _approxPosition = approxPosition;
        _exactPosition = exactPosition;
        _responseCount = responseCount;
        _sentiment = new ArrayList<ResultSentence>();
        if (sentiment != null) _sentiment.add(SentimentResult.ProcessLabeledSentence(sentiment));
        _polarity = new ArrayList<ResultSentence>();
        if (polarity != null) _polarity.add(SentimentResult.ProcessLabeledSentence(polarity));
        _pos = new ArrayList<String>();
        if (polarity != null) _pos.add(pos);
    }
    
    public Node(String id, List<String> body, String author, 
    		int depth, int approxPosition, int exactPosition, int responseCount, 
    		List<String> sentiment, List<String> polarity, List<String> pos) {
        _id = id;
        _body = stripQuotesList(body);
        _author = author;
        _depth = depth;
        _approxPosition = approxPosition;
        _exactPosition = exactPosition;
        _responseCount = responseCount;
        
        if (sentiment != null) this.addOpinion(sentiment);
        if (polarity != null) this.addPolarity(polarity);
        if (pos != null) this.addPOS(pos);
    }
    
    public boolean hasNegation(boolean first) {
    	
    	if (first) {
    		if (_body.isEmpty()) return false;
    		if (_body.get(0).toLowerCase().matches(".*\\b(" + NEGATION + ")\\b.*")) return true;
    		return false;
    	}
    	
    	for (String sentence : _body) {
    		if (sentence.toLowerCase().matches(".*\\b(" + NEGATION + ")\\b.*")) return true;
    	}
    	return false;
    }
    
    public boolean hasAgreement(boolean first) {
    	if (first) {
    		if (_body.isEmpty()) return false;
    		if (_body.get(0).toLowerCase().matches(".*\\b(" + AGREEMENT + ")\\b.*")) return true;
    		return false;
    	}

    	for (String sentence : _body) {
    		if (sentence.toLowerCase().matches(".*\\b(" + AGREEMENT + ")\\b.*")) return true;
    	}
    	return false;
    }

    public boolean hasDisagreement(boolean first) {
    	if (first) {
    		if (_body.isEmpty()) return false;
    		if (_body.get(0).toLowerCase().matches(".*\\b(" + DISAGREEMENT + ")\\b.*")) return true;
    		return false;
    	}
    	
    	for (String sentence : _body) {
    		if (sentence.toLowerCase().matches(".*\\b(" + DISAGREEMENT + ")\\b.*")) return true;
    	}
    	return false;
    }
    
    public Node(String id, String body, String author, 
    		int depth, int approxPosition, int exactPosition, int responseCount) {
        this(id, body, author, depth, approxPosition, exactPosition, responseCount, null, null, null);
    }
    
    public Node(String id, List<String> body, String author, 
    		int depth, int approxPosition, int exactPosition, int responseCount) {
        this(id, body, author, depth, approxPosition, exactPosition, responseCount, null, null, null);
    }
    
    public Node copy() throws Exception {
    	Node n = new Node(_id,_body,_author,_depth, _approxPosition, _responseCount, _exactPosition);
    	n.addOpinionSentences(_sentiment);
    	n.addParse(_parse);
    	n.addPolaritySentences(_polarity);
    	n.addPOS(_pos);
    	return n;
    }
    
    /**
     * Used when splitting sentences into their own ars
     * @param sbj
     * @param tar
     */
    public void setBody(String body) {
    	_body = new ArrayList<String>();
    	_body.add(body);
    	_body = stripQuotesList(_body);
    }
    
    private List<String> stripQuotesList(List<String> sentences) {
    	for (int i = 0; i < sentences.size(); i++) {
    		sentences.set(i, stripQuotes(sentences.get(i)));
    	}
    	return sentences;
    }
    
    public boolean hasQuestion(boolean first) {
    	
    	if (first) {
    		if (_body.isEmpty()) return false;
    		if (_body.get(0).endsWith("?")) return true;
    		return false;
    	}
    	
    	for (int i = 0; i < _body.size(); i++) {
    		if (_body.get(i).endsWith("?")) return true;
    	}
    	return false;
    }
    
    private String stripQuotes(String string) {
    	
    	try {
    		if (_contractions == null) {
    			_contractions = new Contractions();
    		} 
    		string = _contractions.swapContractions(string);
    	} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	if (string.matches("\".*\"")) {
    		return string.substring(1,string.length()-1).trim();
    	}
    	return string.trim();
    }
    
    public void addParse(String parse) throws Exception {
    	_parse = (new PennTreeReader(new StringReader(parse)).readTree());
    }
    
    public void addParse(Tree parse) throws Exception {
    	_parse = parse;
    }
    
    public void addSinglePOS(String pos) {
    	List<String> p = new ArrayList<String>();
    	p.add(pos);
    	addPOS(p);
    }
    
    public void addPOS(List<String> pos) {
    	
    	_pos = new ArrayList<String>();
    	
    	for (String p : pos)
	    _pos.add(stripText(p));
    }
    
    public String stripText(String text) {
  
    	if (text == null) return null;
    	
		String [] words = text.split(" ");
		String pos = "";
		
		for (int index = 0; index < words.length; index++) {
			String posTag = words[index].substring(words[index].lastIndexOf("/") + 1);
			pos += posTag + " ";
		}
		return pos;
    }
    
    public void addOpinion(List<String> sentiment) {
    	_sentiment = new ArrayList<ResultSentence>();
    	
    	for (String s : sentiment) {
    		_sentiment.add(SentimentResult.ProcessLabeledSentence(s));
    	}
    	
    }
    
    public void addSingleOpinion(ResultSentence sentiment) {
    	_sentiment = new ArrayList<ResultSentence>();
    	_sentiment.add(sentiment);
    }
    
    public void addOpinionSentences(List<ResultSentence> sentiment) {
    	_sentiment = sentiment;
    }
    
    public void addPolarity(List<String> polarity) {
    	_polarity = new ArrayList<ResultSentence>();
    	
    	for (String s : polarity) {
    		_polarity.add(SentimentResult.ProcessLabeledSentence(s));
    	}
    }
    
    public void addSinglePolarity(ResultSentence polarity) {
    	_polarity = new ArrayList<ResultSentence>();
    	_polarity.add(polarity);
    }
    
    public void addPolaritySentences(List<ResultSentence> polarity) {
    	_polarity = polarity;
    }

    
    public String getId() {
        return _id;
    }

    public String getBody() {
    	String body = "";
    	
    	for (String s : _body) {
    		body += s + " ";
    	}
        return body.trim();
    }
    
    public List<String> getSentences() {
    	return _body;
    }
    
    public int getNumSentences() {
    	return _body.size();
    }
    
    public int getNumSentencesBinned() {
    	if (getNumSentences() < 5) return 0;
    	else if (getNumSentences() > 10) return 2;
    	return 1;
    }
    
    public int getNumCharsBinned() {
    	if (getBody().length() < (5*30)) return 0;
    	else if (getBody().length() > (10*30)) return 2;
    	return 1;
    }

    public String getPOS(boolean first) {
    	String pos = "";
    	
    	if (_pos == null) return pos;
    	
    	for (String p : _pos) {
    		pos += p + " ";
    		if (first) return pos;
    	}
    	
        return pos;
    }
    
    public String getUniquePOS() {
    	HashSet<String> pos = new HashSet<String>();
    	
    	if (_pos == null) return "";
    	
    	for (String pSentence : _pos) {
    		for (String p : pSentence.split("\\s+")) {
    			pos.add(p);
    		}
    	}
    	
    	String posString = "";
    	
    	for (Object p : pos.toArray()) {
    		posString += p + " ";
    	}
    	return posString.trim();
    }

    public String getAuthor() {
        return _author;
    }

    public int getDepth() {
        return _depth;
    }
    
    public int getDepthBinned() {
       	if (_depth == 0) return 0;
    	else if (_depth == 1) return 1;
    	else if (_depth < 5) return 2;
    	return 3;
    }
    
    public boolean isRoot() {
    	return _depth == 0;
    }

    /*public void addDocumentFeatures(int approxPosition, int responseCount, int exactPosition) {
    	_approxPosition = approxPosition;
    	_responseCount = responseCount;
    	_exactPosition = exactPosition;
    }*/
    
    public int getApproxSentencePosition() {
    	return _approxPosition;
    }
    
    
    public int getExactSentencePosition() {
    	return _exactPosition;
    }
    
    /**
     * For splitting sentences
     * @return
     */
    public void setExactSentencePosition(int exactPosition) {
    	_exactPosition = exactPosition;
    }
    
    public int getNumResponses() {
    	return _responseCount;
    }
    
    public int getNumResponsesBinned() {
    	if (_responseCount == 0) return 0;
    	else if (_responseCount == 1) return 1;
    	else if (_responseCount < 5) return 2;
    	return 3;
    }
    
    public String getSentimentString() {
    	
    	String sentiment = "";
    	
    	if (_sentiment == null) return sentiment;
    	
    	for (ResultSentence s : _sentiment) {
    		sentiment += s.getSentenceSubjectivity() + " ";
    	}
    	
		return sentiment.trim();
    }
    
    public String getPolarityString() {
    	String polarity = "";
    	
    	if (_polarity == null) return polarity;
    	
    	for (ResultSentence s : _polarity) {
    		polarity += s.getSentenceSubjectivity() + " ";
    	}
    	
		return polarity.trim();
    }

    public List<ResultSentence> getSentiment() {
		return _sentiment;
    }
    
    public List<ResultSentence> getPolarity() {
		return _polarity;
    }

    public int getNumPhrases() {
    	
    	int numPhrases = 0;
    	
    	if (_sentiment == null) return 0;
    	
    	for (ResultSentence s : _sentiment) {
    		numPhrases += s.getPhrases().size();
    	}
    	
    	return numPhrases;
    	
    	/*if (_sentiment.getPhrases().size() != _polarity.getPhrases().size()) {
    		System.err.println("INCONSISTENCY!!");
    		return -1;
    	}*/
    }

    
    public List<String> getSubjectiveWords(boolean first) {
    	
    	List<String> words = new ArrayList<String>();
    	
    	if (_sentiment == null) return words;
    	
    	for (ResultSentence s : _sentiment) {
    		for (Phrase phrase : s.getPhrases()) {
    			if (phrase.sentiment().equals("s")) words.add(phrase.getPhrase());
    		}
    		if (first) return words;
    	}
    	return words;
    }
	
	public double getSentimentCount(boolean normalized) {
		
		int numPhrases = getNumPhrases();
		if (_sentiment == null || numPhrases == 0) return 0;
		
		double count = 0;
		
		for (ResultSentence s : _sentiment) {
			for (Phrase phrase : s.getPhrases()) {
				if (phrase.sentiment().equals("s")) count++;
			}
		}
		
		return normalized ? count/numPhrases : count;
	}

	
	public boolean hasSentiment() {
		
		if (_sentiment == null) return false;
		
		for (ResultSentence s : _sentiment) {
			for (Phrase phrase : s.getPhrases()) {
				if (phrase.sentiment().equals("s")) return true;
			}
		}
		return false;
	}
	
   public List<String> getPolarityWords(boolean positive, boolean first) {
    	
    	List<String> words = new ArrayList<String>();
    	
    	if (_polarity == null) return words;
    	
    	for (ResultSentence s : _polarity) {
	    	for (Phrase phrase : s.getPhrases()) {
	    		if (positive && phrase.sentiment().equals("+")) words.add(phrase.getPhrase());
	    		else if (!positive && phrase.sentiment().equals("-")) words.add(phrase.getPhrase());
	    	}
	    	if (first) return words;
    	}
    	return words;
    }
	
	public double getPolarityCount(boolean normalized, boolean positive, boolean first) {
		
		int numPhrases = getNumPhrases();
		if (_polarity == null || numPhrases == 0) return 0;
		
		double count = 0;
		
		for (ResultSentence s : _polarity) {
			for (Phrase phrase : s.getPhrases()) {
				if (positive && phrase.sentiment().equals("+")) count++;
				else if (!positive && phrase.sentiment().equals("-")) count++;
			}
			if (first) return normalized ? count/numPhrases : count;
		}		
		return normalized ? count/numPhrases : count;
	}

	
	public boolean hasPolarity(boolean positive) {

		if (_polarity == null) return false;

		for (ResultSentence s : _polarity) {
			for (Phrase phrase : s.getPhrases()) {
				if (positive && phrase.sentiment().equals("+")) return true;
				else if (!positive && phrase.sentiment().equals("-")) return true;
			}
		}
		return false;
	}
}