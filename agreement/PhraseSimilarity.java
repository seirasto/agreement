package agreement;

public class PhraseSimilarity {
	
	int _subjectSentence = 0;
	int _targetSentence = 0;
	double _similarity = 0;
	int _subjectPhrase = 0;
	int _targetPhrase = 0;
	String _string;
	
	public PhraseSimilarity(String string, int i, int j) {
		_string = string;
		int dash = string.indexOf("-");
		_subjectSentence = i;
		_subjectPhrase = Integer.valueOf(string.substring(0,dash));
		string = string.substring(dash+1);
		dash = string.indexOf("-");
		_targetSentence = j;
		_targetPhrase = Integer.valueOf(string.substring(0,dash));
		string = string.substring(dash+1);
		_similarity = Double.valueOf(string);
	}
	
	public String toString() {
		return _string;
	}
	
	public double getSimilarity() {
		return _similarity;
	}
	
	public int getSubjectSentence() {
		return _subjectSentence;
	}
	
	public int getTargetSentence() {
		return _targetSentence;
	}
	
	public int getSubjectPhrase() {
		return _subjectPhrase;
	}
	
	public int getTargetPhrase() {
		return _targetPhrase;
	}
}