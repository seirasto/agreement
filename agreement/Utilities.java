package agreement;

import java.util.*;

public class Utilities {

	public static HashSet<String> ngrams(List<Thread> threads, int size, int min, int max, boolean twoWay) {
		HashMap<String, int []> terms = new HashMap<String, int []>();
		int [] totalCounts = {0,0,0};
		
		for (Thread thread : threads ) {
			for (Arc arc : thread.getArcs()) {
				if (arc.getType() == Arc.UNKNOWN) continue;
				if (twoWay && arc.getType() == Arc.NONE) continue;
				HashSet<String> arcTerms = new HashSet<String>();
				String [] words = arc.getTarget().getBody().split("\\s+");
				
				for (int i = 0; i < words.length; i++) {
					for (int j = min; j <= max; j++) {
						String phrase = getPhrase(words,i,j);
						if (phrase == null || phrase.trim().isEmpty()) continue;
						arcTerms.add(phrase);
					}
				}
				
				for (String term : arcTerms) {
					int [] counts = {0,0,0};
					if (terms.containsKey(term)) counts = terms.get(term);
					counts[arc.getType()]++;
					terms.put(term, counts);
				}
				totalCounts[arc.getType()]++;
			}
		}
		HashMap<String,Integer> selectedTerms = selectFeatures(terms,totalCounts, twoWay);
		// get top n
		return sortByValue(selectedTerms,size);
	}
	
	private static HashMap<String,Integer> selectFeatures(HashMap<String, int []> terms, int [] totalCounts, boolean twoWay) {
		
		int n = totalCounts[0] + totalCounts[1] + totalCounts[2];
		HashMap<String,Integer> selectedTerms = new HashMap<String,Integer>();
		for (String term : terms.keySet()) {
			int [] counts = terms.get(term);
			int total = counts[0] + counts[1] + counts[2];
			
			/*if ((term.equals("that") || term.equals("the") || term.equals("agree") || term.equals("not")) && total > 10) {
				System.out.println(term);
			}*/
			
			double agree = calculateChiSquare(n,counts[Arc.AGREEMENT],counts[Arc.DISAGREEMENT]+counts[Arc.NONE],
					totalCounts[Arc.AGREEMENT] - counts[Arc.AGREEMENT],(totalCounts[Arc.DISAGREEMENT] + totalCounts[Arc.NONE]) - (counts[Arc.DISAGREEMENT]+counts[Arc.NONE]));
			double disagree = calculateChiSquare(n,counts[Arc.DISAGREEMENT],counts[Arc.NONE]+counts[Arc.AGREEMENT],
					totalCounts[Arc.DISAGREEMENT] - counts[Arc.DISAGREEMENT],(totalCounts[Arc.NONE] + totalCounts[Arc.AGREEMENT]) - (counts[Arc.NONE]+counts[Arc.AGREEMENT]));
			// prominence in agree vs disagree (without none)
			//double a_d = calculateChiSquare(n,counts[Arc.AGREEMENT],counts[Arc.DISAGREEMENT],
			//		totalCounts[Arc.AGREEMENT] - counts[Arc.AGREEMENT],(totalCounts[Arc.DISAGREEMENT]) - (counts[Arc.DISAGREEMENT]));
			/*double none = calculateChiSquare(n,counts[Arc.NONE],counts[Arc.DISAGREEMENT]+counts[Arc.AGREEMENT],
					totalCounts[Arc.NONE] - counts[Arc.NONE],(totalCounts[Arc.DISAGREEMENT] + totalCounts[Arc.AGREEMENT]) - (counts[Arc.DISAGREEMENT]+counts[Arc.AGREEMENT]));*/
					
			if (twoWay && (agree < 3.841 && disagree < 3.841)) continue;
			else if (!twoWay && ((agree > 3.841 && disagree > 3.841) || (agree < 3.841 && disagree < 3.841))) continue;
			selectedTerms.put(term,total);
			
		}
		return selectedTerms;	
	}
	
	/**
	 * n_tc
	 * @param n11 term t in class
	 * @param n10 term t not in class
	 * @param n01 not term t in class
	 * @param n00 not term t not in class
	 * @return
	 */
	private static double calculateChiSquare(double n, double n11, double n10, double n01, double n00) {
		double chiSquare = -1;
	    if (n11 + n01 == 0 || n11 + n10 == 0 || n10 + n00 == 0 || n01 + n00 == 0) {
	      // Boundary cases.
	      return -1;
	    }
	 
	    //double n = n11 + n01 + n10 + n00;
	    // An arithmetically simpler way of computing chi-square
	    chiSquare = (n * Math.pow(((n11*n00) - (n10 * n01)), 2))
	                / ((n11 + n01) * (n11 + n10) * (n10 + n00) * (n01 + n00));
	    return chiSquare;
	  }
		
	public static String getPhrase(String [] words, int start, int size)	{
		if (start + size > words.length) return null;
		
		String phrase = "";
	
		for (int i = start; i < start+size && i < words.length; i++) {
			if (words[i].matches("\\p{Punct}+")) continue;
			phrase += words[i].replaceAll("\\p{Punct}+", "") + "_";
		}
		if (phrase.isEmpty()) return null;
		return phrase.substring(0,phrase.length()-1).toLowerCase();
	}
	
	public static HashSet<String> pos(List<Thread> threads, int size, int min, int max, boolean twoWay) {
		HashMap<String, int []> terms = new HashMap<String, int []>();
		int [] totalCounts = {0,0,0};
		
		HashSet<String> single = new HashSet<String>();
		
		for (Thread thread : threads ) {
			for (Arc arc : thread.getArcs()) {
				if (arc.getType() == Arc.UNKNOWN) continue;
				if (twoWay && arc.getType() == Arc.NONE) continue;
				HashSet<String> arcTerms = new HashSet<String>();
				String [] words = arc.getTarget().getPOS(false).replaceAll("\\p{Punct}+", "").split("\\s+");
				
				for (int i = 0; i < words.length; i++) {
					single.add(words[i]);
					for (int j = min; j < max; j++) {
						String phrase = getPhrase(words,i,j);
						if (phrase == null || phrase.isEmpty()) continue;
						arcTerms.add(phrase);
					}
				}
				
				for (String term : arcTerms) {
					int [] counts = {0,0,0};
					if (terms.containsKey(term)) counts = terms.get(term);
					counts[arc.getType()]++;
					terms.put(term, counts);
				}
				totalCounts[arc.getType()]++;
			}
		}
		HashMap<String,Integer> selectedTerms = selectFeatures(terms,totalCounts, twoWay);
		
		// get top n
		HashSet<String> pos = sortByValue(selectedTerms, size);
		
		// add all single pos
		for (String p : single) {
			if (p.trim().isEmpty()) continue;
			pos.add(p.toLowerCase());
		}
		return pos;
	}
	
	public static HashSet<String> sortByValue(Map<String, Integer> map, int size) {
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {

            public int compare(Map.Entry<String, Integer> m1, Map.Entry<String, Integer> m2) {
                return (m2.getValue()).compareTo(m1.getValue());
            }
        });

        int count = 0;
        
        HashSet<String> result = new HashSet<String>();
        for (Map.Entry<String, Integer> entry : list) {
        	if (entry.getValue() == 1) continue;  
        	if (count >= size) break;
        	result.add(entry.getKey());
        	count++;
        }
        return result;
    }

}
