/**
 * 
 */
package agreement;

import java.util.*;

/**
 * @author sara
 *
 * A thread consists of a list of nodes and arcs. 
 */
public class Thread {
	
	String _docId;
	List<Node> _nodes;
	List<Arc> _arcs;
	int _depth;
	
	/**
	 * 
	 */
	public Thread(String docId) {
		
		_docId = docId;
		_depth = 0;
		_nodes = new ArrayList<Node>();
		_arcs = new ArrayList<Arc>();
	}
	
	public String getDocID() {
		return _docId;
	}
	
	public int getDepth() {
		return _depth;
	}
	
	public void addNode(Node n) {
		if (n.getDepth() > _depth) _depth = n.getDepth(); 
		_nodes.add(n);
	}
	
	public void addArc(Arc a) {
		_arcs.add(a);
	}
	
	public List<Arc> getArcs() {
		return _arcs;
	}
	
	public int numArcs() {
		return _arcs.size();
	}
	
	public List<Node> getNodes() {
		return _nodes;
	}
	
	public int numNodes() {
		return _nodes.size();
	}
	
	public double averageSentenceCount() {
		double value = 0;
		
		for (Node node : _nodes) {
			value += node.getSentences().size();
		}
		return value/_nodes.size();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
