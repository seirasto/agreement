
package agreement;

import java.util.*;
import java.io.File;
import java.io.Serializable;

import agreement.classifier.AgreementClassifier;
import agreement.classifier.DataGenerator;

import edu.columbia.opinion.io.*;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
//import edu.stanford.nlp.trees.*;
//import java.io.StringReader;

import processing.*;

@SuppressWarnings("serial")
public class Arc implements Serializable {
	
	public static final int UNKNOWN = -1;
    public static final int NONE = 0;
    public static final int AGREEMENT = 1;
    public static final int DISAGREEMENT = 2;
    public static final int BOTH = 3;
    
    /*//start (0), middle (1), or end (2)
    public static final int START = 0;
    public static final int MIDDLE = 1;
    public static final int END = 2;*/
    
    private static final String SENT_DELIMITERS = "(?<=[.!?])\\s|\\n+";

    private Node _subject;
    private Node _target;

    private boolean _isParent;

    private int _type;
    
    private List<Double> _sentenceSimilarity;
    private ArrayList<PhraseSimilarity> _phraseSimilarities;
	
	public static MaxentTagger _tagger;
	
	public static void main(String [] args) {
		
    	DataGenerator dataGenerator = new DataGenerator("",false,true);
    	// String f = "Barack_Obama__Archive_70___Response_to_Gulf_Oil_Spill.xml";
    	// String f = "Barack_Obama__Archive_70___No_criticism.3F.xml";
    	//String f = "Catholic_Church__Archive_15___Addressing_peer_reviewer_comments.xml";
    	//String f = "Catholic_Church__Archive_15___Dubious_points.xml";
    	//String f = "/proj/fluke/SCIL/corpora/wikipedia/WD_batch1LJ/Catholic_Church__Archive_15___Inquisition_change.xml";
    	//String f = "Basque_language___Pre-Indo-European.3F_Are_Indo-Europeans_the_center_of_the_world.3F.xml";
    	//String f = "Catholic_Church__Archive_15___Here_is_an_article_to_balance_the_abuse_scandal.xml";
    	//String f = "Barack_Obama__Archive_70___Liberal_Bias.xml";
    	//String f = "Catholic_Church__Archive_15___Bias_athiest.2Fprotestant.2Fsecular_centric_sentence_in_the_lead.xml";
		//String f = "Catholic_Church__Archive_15___Child_sex_abuse_and_aboriginal_abuse_scandals.xml";
    	
    	//String f = "Atheism__Archive_50___Etymology_section___relevance_of_Sam_Harris_quote.xml";
    	//String f = "Centrifugal_force__Archive_10___Introductory_material.xml";
    	String f = "Buddhism__Archive_13___Theravada_2C__22the_oldest_surviving_branch_22_3F.xml";
    	String testFile = "/proj/fluke/SCIL/corpora/wikipedia/WD_batch1LJ/" + f; 
		
		AgreementClassifier.processFile(dataGenerator, testFile, "/proj/nlp/users/sara/agreement/res/", "res", true, true, true, true, false);
		
    	String fullProcessingPath = "/proj/nlp/users/sara/agreement/res/processed/processed-" + new File(testFile).getName() + "/" + new File(testFile).getName();
    	dataGenerator.computePhraseSimilarity(fullProcessingPath + ".txt.emo.con.chk", fullProcessingPath + ".txt.phrase");
    	
		// generate whether we use or not.
    	List<String> sentences = processing.GeneralUtils.readLines(fullProcessingPath + ".txt");
		//List<String> ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
		List<String> opinion = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.con.chk.emo.sbj");
		List<String> polarity = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.con.chk.emo.op");
		List<String> pos = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.con.pos");	
		List<String>ss = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss");
		List<List<String []>> phraseSS = DataGenerator.readPhraseSimilarityFromFile(fullProcessingPath + ".txt.phrase.ss");

		//for (int i = 0; i < sentences.size(); i++) {
			//for (int j = i+1; j < sentences.size(); j++) {
			
			
			int txtPosSBJ = 41; //26;
			int txtPosTAR = 48; //28;
			
			//int sbjPos = 1;
			//int tarPos = 1;
			
			/*if (s == 0) sbjPos = 0;
			if (s == subject.size()-1) sbjPos = 2;
			
			if (t == 0) tarPos = 0;
			if (t == target.size()-1) tarPos = 2;*/
			
			//a.addDocumentFeatures(sbjPos,tarPos,arc.getNumResponsesToSubject(), arc.getNumResponsesToTarget());
			// add whether we use or not.
			Node s = new Node("",sentences.get(txtPosSBJ),"",-1,-1,-1,-1);
			Node t = new Node("",sentences.get(txtPosTAR),"",-1,-1,-1,-1);
			s.addSingleOpinion(SentimentResult.ProcessLabeledSentence(opinion.get(txtPosSBJ)));
			t.addSingleOpinion(SentimentResult.ProcessLabeledSentence(opinion.get(txtPosTAR)));
			t.addSinglePolarity(SentimentResult.ProcessLabeledSentence(polarity.get(txtPosSBJ))); 
			t.addSinglePolarity(SentimentResult.ProcessLabeledSentence(polarity.get(txtPosTAR)));
			t.addSinglePOS(pos.get(txtPosSBJ));
			t.addSinglePOS(pos.get(txtPosTAR));
			
			Arc a = new Arc(s, t, false, Arc.NONE); //, testFile);
			List<Double> ssScore = new ArrayList<Double>();
			ssScore.add(SentenceSimilarity.cosineSimilarity(ss.get(txtPosSBJ+1).split(" "), ss.get(txtPosTAR + 1).split(" ")));
			a.addSentenceSimilarity(ssScore);
			// add check for size of phrases to = size of sentiment
			//if (a.getNumPhrases(true) == phraseSS.get(txtPosSBJ).size() && 
			//		a.getNumPhrases(false) == phraseSS.get(txtPosTAR).size()) {
			//	continue;
			//}
			System.out.println("sbj: " + txtPosSBJ + " tar: " + txtPosTAR);
			System.out.println("SBJ: " + a.getSubject().getSentiment());
			System.out.println("TAR: " + a.getTarget().getSentiment());
			//System.out.println("CHECK: " + a.getNumPhrases(true) + "=" + phraseSS.get(txtPosSBJ).size() + "?");
			//System.out.println("CHECK: " + a.getNumPhrases(false) + "=" + phraseSS.get(txtPosTAR).size() + "?");
			List<String> phrases = new ArrayList<String>();
			phrases.add(dataGenerator.computePhraseSimilarity(phraseSS.get(txtPosSBJ), phraseSS.get(txtPosTAR)));
			a.addPhraseSimilarityFromString(phrases);
			System.out.println(a);
			System.out.println(Arrays.toString(a.getPhraseSimilarity(.6, false)));
			//System.out.println(a.getPhraseSimilarityUniqueWords(.66));
			//System.out.println(a.getPhraseSimilaritySentiment(.66));
			//}
		//}
	}
	
    public String toString() {
        return typeStr(_type) + ":::" + getKey();
    }
    
    public String getKey() {
    	return _subject.getId() + ";" + _target.getId() + ";" + _subject.getBody() + ";" + _target.getBody();
    }

    public static String typeStr(int type) {
        switch(type) {
            case NONE : return "none";
            case AGREEMENT : return "agreement";
            case DISAGREEMENT : return "disagreement";
            case BOTH: return "both";
            case UNKNOWN : return "unknown";
            default : return "none";
        }
    }
    
    /**
     * tab delimited sentence containing all the information
     * @param sentence
     */
    public Arc(String sentence) {
    	String [] arc = sentence.split("\t");
    	
    	_subject = new Node(arc[1], arc[3], arc[5], Integer.valueOf(arc[7]),-1,-1,-1);
    	_target = new Node(arc[2], arc[4], arc[6], Integer.valueOf(arc[8]),-1,-1,-1);
    	
    	_isParent = Boolean.valueOf(arc[9]);
    	
    	_type = arc[0].equals("agreement") ? AGREEMENT : DISAGREEMENT;
    	_sentenceSimilarity = new ArrayList<Double>();
    	_phraseSimilarities = new ArrayList<PhraseSimilarity>();
    }
    
    public Arc(String subjectId, String targetId, String subjectBody, String targetBody, String sbjAuthor, String tarAuthor, 
    		int depthSbj, int depthTar, boolean isAncestor, int type) { //, String docId) {

    	this(new Node(subjectId, subjectBody, sbjAuthor, depthSbj,-1,-1,-1), 
    			new Node(targetId, targetBody, tarAuthor, depthTar,-1,-1,-1), isAncestor, type);
    }
    
    public Arc(Node subject, Node target, boolean isParent, int type) {
    	_subject = subject;
    	_target = target;
    	_isParent = isParent;
    	_type = type;
    	_sentenceSimilarity = new ArrayList<Double>();
    	_phraseSimilarities = new ArrayList<PhraseSimilarity>();
    }
    
    public Arc copy() throws Exception {
    	Arc a = new Arc(_subject.copy(), _target.copy(), _isParent,_type);
    	a.addSentenceSimilarity(_sentenceSimilarity);
    	return a;
    }
    
	public static int computeType(List<Integer> types, List<String> targets) {
    	
    	int agreement = 0;
    	int disagreement = 0;
    	int agreementTargetLength = 0;
    	int disagreementTargetLength = 0;
    	
    	for (int i = 0; i < types.size(); i++) {
    		if (types.get(i) == Arc.AGREEMENT) {
    			agreement++;
    			agreementTargetLength += targets.get(i).length();
    		}
    		else if (types.get(i) == Arc.DISAGREEMENT) {
    			disagreement++;
    			disagreementTargetLength += targets.get(i).length();
    		}
    	}
    	
    	if (agreement > disagreement) return Arc.AGREEMENT;
    	if (disagreement > agreement) return Arc.DISAGREEMENT;
    	if (agreement > 0) {
    		System.out.println("Conflicting Post: " + Arrays.toString(targets.toArray()));
    		// for now 
    		if (disagreementTargetLength > agreementTargetLength) return Arc.DISAGREEMENT;
    		else return Arc.AGREEMENT;
    		
    	}
    	return Arc.NONE;
	}
    
    public Node getSubject() {
    	return _subject;
    }
    
    public Node getTarget() {
    	return _target;
    }
    
    public void setExactSentencePosition(int s, int t) {
    	_subject.setExactSentencePosition(s);
    	_target.setExactSentencePosition(t);
    }
     
    private void initPOS() {
    	try {
	    	if (_tagger == null) 
	    		_tagger = new MaxentTagger("edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger");
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    public void addSentenceSimilarity(List<Double> ss) {
    	if (ss == null) return;
    	
    	for (double s : ss) {
    		_sentenceSimilarity.add(s > 0 ? s : 0);
    	}
    }
    
    public void addSingleSentenceSimilarity(double ss) {
    	ArrayList<Double> s = new ArrayList<Double>();
    	s.add(ss);
    	addSentenceSimilarity(s);
    }
    
    public void addSinglePhraseSimilarityFromString(String similarities) {
    	ArrayList<String> s = new ArrayList<String>();
    	s.add(similarities);
    	addPhraseSimilarityFromString(s);
    }
    
    public void addPhraseSimilarityFromString(List<String> similarities) {
    	
    	if (similarities == null) return;
    	
    	for (int index = 0; index < similarities.size(); index++) {
    		String [] data = similarities.get(index).trim().split("\\s+");
    		
    		String [] sentencePosition = new String [] {"0","0"};
    		int i = 0;
    		if (data[0].indexOf(":") > 0) {
    			i = 1;
    			sentencePosition = data[0].split(":");
    		}
    	
	    	for (; i < data.length; i++) {
	    		String similarity = data[i];
	    		try {
		    		if (similarity.trim().isEmpty()) continue;
		    		_phraseSimilarities.add(new PhraseSimilarity(similarity,Integer.valueOf(sentencePosition[0]),Integer.valueOf(sentencePosition[1])));
	    		} catch(Exception e) {
	    			e.printStackTrace();
	    			System.err.println("[Arc.addPhraseSimilarity] " + similarity);
	    		}
	    	}
    	}
    }
    
    public void addPhraseSimilarity(ArrayList<PhraseSimilarity> similarities) {
    	_phraseSimilarities = similarities;
    }
    
    public int getNumPhrases() {
    	return _phraseSimilarities.size();
    }
    
    public int distance() {
        return _isParent ? (_target.getDepth() - _subject.getDepth()) : -1;
    }

    public int getDepthDiff() {
        return Math.abs(_subject.getDepth() - _target.getDepth());
    }

    public boolean isParent() {
        return distance() == 1 ? true : false;
    }
    
    public boolean isAncestor() {
    	return _isParent;
    }
    
    public boolean isBoth() {
    	if (_type == Arc.BOTH) return true;
    	return false;
    }

    public int getType() {
        return _type;
    }
    
    public int getOverlap() {
        HashSet<String> set = new HashSet<String>(Arrays.asList(_subject.getBody().split(SENT_DELIMITERS)));

        int count = 0;

        for(String tok : _target.getBody().split(SENT_DELIMITERS)) {
            if(set.contains(tok))
                count++;
        }

        return count;
    }

    public int getStemmedOverlap() {
        return -1;
    }

   /* public String getDocId() {
        return docId;
    }
    */
    public List<Double> getSentenceSimilarity() {
    	return _sentenceSimilarity;
    }
    
    public int getMeanSentenceSimilarity(double threshold) {
    	
    	double total = 0;
    	
    	for (double ss : _sentenceSimilarity) {
    		total += ss;
    	}
    	if ((total/_sentenceSimilarity.size()) >= threshold) return 1;
    	return 0;
    }
    
    public int getMaxSentenceSimilarity(double threshold) {
    	
    	double max = 0;
    	
    	for (double ss : _sentenceSimilarity) {
    		if (ss > max) max = ss;
    	}
    	if (max >= threshold) return 1;
    	return 0;
    }
    
    public int getFirstSimilar(double threshold) {
    	if (_sentenceSimilarity == null || _sentenceSimilarity.isEmpty()) return 0;
    	if (_sentenceSimilarity.get(0) >= threshold) return 1;
    	return 0;
    }
    
    
    public int getPhraseSimilaritySentiment(double threshold, boolean first) {
    	double maxSimilarity = 0;
    	double maxNoPolaritySimilarity = 0;
    	//int maxSentenceIndex = 0;
    	PhraseSimilarity max = null;
    	PhraseSimilarity maxNoPolarity = null;
    	
    	
		for (PhraseSimilarity p : _phraseSimilarities) {
			if (first && p.getTargetSentence() > 0) continue;
			
			if (p.getSubjectPhrase() > (_subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size()-1)) continue;
			if (p.getTargetPhrase() > (_subject.getPolarity().get(p.getSubjectPhrase()).getPhrases().size() + _target.getPolarity().get(p.getTargetPhrase()).getPhrases().size()-1)) continue;
	
			if (p.getSimilarity() > maxSimilarity) {
				maxNoPolarity = p;
				maxNoPolaritySimilarity = p.getSimilarity();
				try {
	    			if (!_subject.getPolarity().get(p.getSubjectSentence()).getPhrase(p.getSubjectPhrase()).sentiment().isEmpty()
	    				&& !_target.getPolarity().get(p.getTargetSentence()).getPhrase(p.getTargetPhrase() - _subject.getPolarity().get(p.getSubjectPhrase()).getPhrases().size() - 1).sentiment().isEmpty()) {
		    			max = p;
		    			//maxSentenceIndex = i;
		    			maxSimilarity = p.getSimilarity();
	    			}
	   			} catch (Exception e) {
					System.out.println(p + "\n" + p.getSubjectPhrase() + ": " + _subject.getPolarity() + "\n" + 
	   			(p.getTargetPhrase() - _subject.getPolarity().get(p.getSubjectPhrase()).getPhrases().size() - 1) + ": " + _target.getPolarity());
					e.printStackTrace();
				}
			}
		}
    	
    	
    	if (maxSimilarity < threshold) {
    		max = maxNoPolarity;
    		maxSimilarity = maxNoPolaritySimilarity;
    	}
    	if (maxSimilarity < threshold) {
    		return 0;
    	}
    	
    	try {
    		return convertToInt(_subject.getPolarity().get(max.getSubjectSentence()).getPhrase(max.getSubjectPhrase()).sentiment() + "_" 
    			+ _target.getPolarity().get(max.getSubjectSentence()).getPhrase(max.getTargetPhrase() - _subject.getPolarity().get(max.getSubjectSentence()).getPhrases().size() - 1).sentiment());
    	} catch (Exception e) {
    		return 0;
    	}
    }
    
    protected int convertToInt(String p) {
    	
    	if (p.equals("+_-")) return 1;
    	else if (p.equals("-_+")) return 1;
    	else if (p.equals("+_+")) return 0;
    	else if (p.equals("-_-")) return 0;
    	else if (p.equals("_")) return 2;
    	else if (p.equals("+_")) return 2;
    	else if (p.equals("-_")) return 2;
    	else if (p.equals("_+")) return 2;
    	else if (p.equals("_-")) return 2;
    	return 0;
    }
    
    public String getPhraseSimilarityWords(double threshold, boolean first) {
    	String words = "";
    	
    	for (PhraseSimilarity p : _phraseSimilarities) {
    		
    		if (first && p.getTargetSentence() > 0) continue;
    		
    		if (p.getSimilarity() > threshold) {
    			try {
	    			if (p.getSubjectPhrase() > (_subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size()-1)) continue;
	    			if (p.getTargetPhrase() > (_subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size() + _target.getPolarity().get(p.getTargetSentence()).getPhrases().size()-1)) continue;
	    			if (_subject.getSentiment().get(p.getSubjectSentence()).getPhrase(p.getSubjectPhrase()).getConfidence() < threshold) continue;
	       			if (_target.getSentiment().get(p.getTargetSentence()).getPhrase(p.getTargetPhrase() - _subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size() - 1).getConfidence() < threshold) continue;
	       			
	       			// keep words that occur in both phrases
	    			words +=  intersection(_subject.getSentiment().get(p.getSubjectSentence()).getPhrase(p.getSubjectPhrase()).getPhrase(),
	    					_target.getSentiment().get(p.getTargetSentence()).getPhrase(p.getTargetPhrase() - _subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size() - 1).getPhrase()) + " ";
    			} catch (Exception e) {
    				System.out.println(p + "\n" + p.getSubjectPhrase() + ": " + _subject.getPolarity() + "\n" + (p.getTargetPhrase() - _subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size() - 1) + ": " + _target.getPolarity());
					e.printStackTrace();
    				continue;
    			}
    		}
    	}
    	return words;
    }
    
    public String getPhraseSimilarityUniqueWords(double threshold, boolean first) {
    	HashSet<String> words = new HashSet<String>();
    	
    	for (PhraseSimilarity p : _phraseSimilarities) {
				
			if (first && p.getTargetSentence() > 0) continue;
			
			if (p.getSimilarity() > threshold) {
				
				//int numSubjectPhrases = _subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size();
				
				try {
					if (p.getSubjectPhrase() > (_subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size())) continue;
	    			if (p.getTargetPhrase() > (_target.getPolarity().get(p.getTargetSentence()).getPhrases().size()-1)) continue;
	    			if (_subject.getSentiment().get(p.getSubjectSentence()).getPhrase(p.getSubjectPhrase()).getConfidence() < threshold) continue;
	       			if (_target.getSentiment().get(p.getTargetSentence()).getPhrase(p.getTargetPhrase()).getConfidence() < threshold) continue;
	       			
	       			// keep words that occur in both phrases
	    			words.addAll(unique(_subject.getSentiment().get(p.getSubjectSentence()).getPhrase(p.getSubjectPhrase()).getPhrase(),
	    					_target.getSentiment().get(p.getTargetSentence()).getPhrase(p.getTargetPhrase()).getPhrase()));
				} catch (Exception e) {
					System.out.println(p);
					if (_subject != null && _subject.getPolarity().size() > p.getSubjectSentence()) 
						System.out.println(p.getSubjectPhrase() + ": " + _subject.getPolarity().get(p.getSubjectSentence()));
					if (_target != null && _target.getPolarity().size() > p.getTargetSentence())
						System.out.println(p.getTargetPhrase() + ": " + _target.getPolarity().get(p.getTargetSentence()));
					e.printStackTrace();
					continue;
				}
			}
		}
    	
    	String uniqueWords = "";
    	
    	for (String word : words) uniqueWords += word.replaceAll("\\p{Punct}+", "").replaceAll("\\s+", "_") + " ";
    	return uniqueWords.trim();
    }
    
    public int [] getPhraseSimilarity(double threshold, boolean first) {
    	int [] count = {0,0,0};
    	
    	for (PhraseSimilarity p : _phraseSimilarities) {
			try {
				
				if (first && p.getTargetSentence() > 0) continue;
				
				if (p.getSubjectPhrase() > (_subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size())) continue;
    			if (p.getTargetPhrase() > (_target.getPolarity().get(p.getTargetSentence()).getPhrases().size()-1)) continue;

				
				if (p.getSimilarity() > threshold) {
					
					//int numSubjectPhrases = _subject.getPolarity().get(p.getSubjectSentence()).getPhrases().size();
					
					if (hasComplexPOS(_subject.getPolarity().get(p.getSubjectSentence()).getPhrase(p.getSubjectPhrase()).getPhrase())
							&& hasComplexPOS(_target.getPolarity().get(p.getTargetSentence()).getPhrase(p.getTargetPhrase()).getPhrase())) {
				
						//this._subject.getPolarity().getPhrase(p.getSubject()).
						count[convertToInt(_subject.getPolarity().get(p.getSubjectSentence()).getPhrase(p.getSubjectPhrase()).sentiment() + "_" 
    	    			+ _target.getPolarity().get(p.getTargetSentence()).getPhrase(p.getTargetPhrase()).sentiment())]++;
					}
				}
			} catch (Exception e) {
				System.out.println(p + "\n" + p.getSubjectPhrase() + ": " + _subject.getPolarity().get(p.getSubjectSentence())
						+ "\n" + (p.getTargetPhrase()) + ": " + _target.getPolarity().get(p.getTargetSentence()));
				e.printStackTrace();
			}
    	}
    	return count;
    }
   
    
    private boolean hasComplexPOS(String text) {
    	if (_tagger == null) this.initPOS();
    	//String pos = getPosOfPhrase(phraseIndex,posString,sentiment);
    	
    	String pos = _tagger.tagString(text);
    	return pos.matches(".*(JJ|NN|VB|RB).*");
    }
    
    /*private String getPosOfPhrase(String text) {
    	String tagged = MaxentTagger.tokenizeText(new StringReader(text)).toString();
    	String [] pos = posString.split("\\s+");
    	
    	int wordPos = 0;
    	
    	for (int i = 0; i < phraseIndex; i++) {
    		wordPos += sentiment.getPhrase(i).getPhrase().trim().split("\\s+").length;
    	}
    	
    	String sentimentPOS = "";
    	
    	for (int i = wordPos; i < (wordPos + sentiment.getPhrase(phraseIndex).getPhrase().trim().split("\\s+").length); i++) {
    		sentimentPOS += pos[i] + " ";
    	}
    	return sentimentPOS;
    }*/
    
    public String intersection(String sbj, String tar) {

    	String intersection = "";
    	
    	String [] s = sbj.split("\\s++");
    	String [] t = tar.split("\\s++");
    	
    	HashSet<String> words = new HashSet<String>();
    	HashSet<String> intersectionWords = new HashSet<String>();
    	
    	for (String word : s) {
    		if (words.contains(word)) {
    			intersectionWords.add(word);
    		}
    		words.add(word);
    	}
    	
    	for (String word : t) {
    		if (words.contains(word)) {
    			intersectionWords.add(word);
    		}
    		words.add(word);
    	}
    	
    	for (String word : intersectionWords) {
    		if (hasComplexPOS(word)) intersection += word + " ";
    	}
    	
    	return intersection;
    }
    
    public HashSet<String> unique(String sbj, String tar) {

    	HashSet<String> unique = new HashSet<String>();
    	
    	String [] s = sbj.split("\\s++");
    	String [] t = tar.split("\\s++");
    	
    	HashMap<String,String> words = new HashMap<String,String>();
    	HashSet<String> intersectionWords = new HashSet<String>();
    	
    	for (String word : s) {
    		word = word.toLowerCase();
    		Stemmer stemmer = new Stemmer();
    		stemmer.add(word.toCharArray(),word.length());
    		stemmer.stem();
    		String stemmed = stemmer.toString();
    		
    		if (words.containsKey(stemmed)) {
    			intersectionWords.add(stemmed);
    		}
    		words.put(stemmed,word);
    	}
    	
    	for (String word : t) {
    		word = word.toLowerCase();
    		Stemmer stemmer = new Stemmer();
    		stemmer.add(word.toCharArray(),word.length());
    		stemmer.stem();
    		String stemmed = stemmer.toString();
    		
    		if (words.containsKey(stemmed)) {
    			intersectionWords.add(stemmed);
    		}
    		words.put(stemmed,word);
    	}
    	
    	for (String stemmed : words.keySet()) {
    		if (intersectionWords.contains(stemmed)) continue;
    		String word = words.get(stemmed);
    			if (hasComplexPOS(word)) unique.add(word);
    	}
    	
    	return unique;
    }

    
    /*public String getSentimentString(boolean subject) {
		Sentence sentiment = subject ? _subject.getSentiment() : _target.getSentiment();
		return sentiment.getSentenceSubjectivity();
    }
    
    public String getPolarityString(boolean subject) {
		Sentence sentiment = subject ? _subject.getPolarity() : _target.getPolarity();
		return sentiment.getSentenceSubjectivity();
    }

    public Sentence getSentiment(boolean subject) {
		Sentence sentiment = subject ? _subject.getSentiment() : _target.getSentiment();
		return sentiment;
    }
    
    public Sentence getPolarity(boolean subject) {
		Sentence sentiment = subject ? _subject.getPolarity() : _target.getPolarity();
		return sentiment;
    }

    
    public int getNumPhrases(boolean subject) {
    	Sentence sentiment = subject ? _subject.getSentiment() : _target.getSentiment();
    	Sentence polarity = subject ? _subject.getPolarity() : _target.getPolarity();
    	
    	if (sentiment.getPhrases().size() != polarity.getPhrases().size()) {
    		System.err.println("INCONSISTENCY!!");
    		return -1;
    	}
    	return sentiment.getPhrases().size();
    }

    
    public String getSubjectiveWords(boolean subject) {
    	Sentence sentiment = subject ? _subject.getSentiment() : _target.getSentiment();
    	
    	String words = "";
    	
    	for (Phrase phrase : sentiment.getPhrases()) {
    		if (phrase.sentiment().equals("s")) words += phrase.getPhrase() + " ";
    	}
    	return words.trim();
    }
	
	public double getSentimentCount(boolean subject, boolean normalized) {
		
		Sentence sentiment = subject ? _subject.getSentiment() : _target.getSentiment(); 
		
		if (sentiment.getPhrases() == null || sentiment.getPhrases().isEmpty()) return 0;
		
		double count = 0;
		
		for (Phrase phrase : sentiment.getPhrases()) {
			if (phrase.sentiment().equals("s")) count++;
		}
		
		return normalized ? count/sentiment.getPhrases().size() : count;
	}

	
	public boolean hasSentiment(boolean subject) {
		
		Sentence sentiment = subject ? _subject.getSentiment() : _target.getSentiment();
		
		for (Phrase phrase : sentiment.getPhrases()) {
			if (phrase.sentiment().equals("s")) return true;
		}
		return false;
	}
	
	   public String getPolarityWords(boolean subject, boolean positive) {
	    	Sentence sentiment = subject ? _subject.getPolarity() : _target.getPolarity();
	    	
	    	String words = "";
	    	
	    	for (Phrase phrase : sentiment.getPhrases()) {
	    		if (positive && phrase.sentiment().equals("+")) words += phrase.getPhrase() + " ";
	    		else if (!positive && phrase.sentiment().equals("-")) words += phrase.getPhrase() + " ";
	    	}
	    	return words.trim();
	    }
		
		public double getPolarityCount(boolean subject, boolean normalized, boolean positive) {
			
			Sentence sentiment = subject ? _subject.getPolarity() : _target.getPolarity(); 
			
			if (sentiment.getPhrases() == null || sentiment.getPhrases().isEmpty()) return 0;
			
			double count = 0;
			
			for (Phrase phrase : sentiment.getPhrases()) {
				if (positive && phrase.sentiment().equals("+")) count++;
				else if (!positive && phrase.sentiment().equals("-")) count++;
			}
			
			return normalized ? count/sentiment.getPhrases().size() : count;
		}

		
		public boolean hasPolarity(boolean subject, boolean positive) {
			
			Sentence sentiment = subject ? _subject.getPolarity() : _target.getPolarity();
			
			for (Phrase phrase : sentiment.getPhrases()) {
				if (positive && phrase.sentiment().equals("+")) return true;
				else if (!positive && phrase.sentiment().equals("-")) return true;
			}
			return false;
		}
		*/
}
