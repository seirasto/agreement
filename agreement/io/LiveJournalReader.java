package agreement.io;

import java.io.*;
import blog.threaded.livejournal.*;

public class LiveJournalReader {


    public static blog.Blog readBlog(String dir, String id, boolean livejournal) throws IOException {

        BufferedReader eReader = new BufferedReader(new FileReader(dir));

        String l;
        while((l = eReader.readLine()) != null) {
        	
        	if (!new File(new File(l).getParent() + "-pos/" + new File(l).getName()).exists())  {
        		blog.threaded.livejournal.Blog.addPOSFlatEntry(l,"libs/POSTagger/wsj-0-18-bidirectional-distsim.tagger");	
        	}
        	
        	l =  new File(new File(l).getParent() + "-pos/" + new File(l).getName()).toString();
        	
            Blog blog = Blog.processBlog(l);
            
            String e = (String)blog.getEntries().next();
            if(e.equals(id)) {
            	if (!livejournal && e.indexOf("livejournal") >= 0) return null;
                return blog;
            }
        }

        return null;
    }
}
