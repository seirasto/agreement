package agreement.io;

import java.util.*;
import java.io.*;

import org.w3c.dom.*;
import dom.Writer;

import blog.Blog;
import blog.XMLUtilities;

public class ProcessBlog {

	public static Blog readBlog(String file, String docId, String type) {
   		Blog b = null;
        
   		//String type = "";
   		if (type.indexOf("livejournal") >= 0 || type.indexOf("wikipedia") >= 0) type = "livejournal";
   		if (type.indexOf("create_debate") >= 0 ||  type.indexOf("internet_argument_corpus") >=0 || type.indexOf("iac") >= 0) type = "createdebate";
   		else type = "livejournal";
   		
    	try {
    		if (type.equals("livejournal")) b = blog.threaded.livejournal.Blog.processBlog(file);
    		else if (type.equals("createdebate")) b = blog.threaded.create_debate.Blog.processBlog(file);
    		else System.err.println("Invalide file type: " + type);
        } catch(Exception e) {
           e.printStackTrace();
        }
    	return b;
	}
	
	/**
	 * Add agreement side to blog file
	 * @param file
	 * @param annotations
	 */
	public static String writeBlog(String file, String outputDirectory, ArrayList<String> annotations) {
		
		Document blog = XMLUtilities.getDocument(file);
		
		// first annotation is original post
		Element root = (Element)blog.getElementsByTagName("description").item(0);
		root.setAttribute("side", annotations.get(0));

		// rest of the annotations are comments (in order)
		NodeList comments = blog.getElementsByTagName("comment");
		int count = 1;
		
		for (int i = 0; i < comments.getLength(); i++) {
			Element comment = (Element)comments.item(i);
			NodeList text = comment.getElementsByTagName("text");
			if (text.getLength() == 1 && ((Element)text.item(0)).getTextContent().isEmpty()) {
				comment.setAttribute("side", "none");
			}
			else {
				comment.setAttribute("side", annotations.get(count));
				count++;
			}
		}
		
		try {
			String output = outputDirectory + ".agreement";
	    	System.err.println("[ProcessBlog.writeBlog] writing to " + output);
	    	FileOutputStream xml = new FileOutputStream(output);
			Writer writer = new Writer();
			writer.setOutput(xml,"UTF-8");
			writer.write(blog);
			return output;
		} catch (Exception e) {
			System.err.println("[ProcessBlog.writeBlog] " + e);
			e.printStackTrace();
		}
		return null;
	}
	
	public static HashMap<String,String> getAnnotations(String file) {
		
		HashMap<String,String> annotations = new HashMap<String,String>();
		
		Document blog = XMLUtilities.getDocument(file);
		
		// first annotation is original post
		Element root = (Element)blog.getElementsByTagName("description").item(0);
		annotations.put("-1",root.getAttribute("side"));

		// rest of the annotations are comments (in order)
		NodeList comments = blog.getElementsByTagName("comment");
		
		for (int i = 0; i < comments.getLength(); i++) {
			Element comment = (Element)comments.item(i);
			annotations.put(comment.getAttribute("url"),comment.getAttribute("side"));
		}
		return annotations;    	
	}
}
