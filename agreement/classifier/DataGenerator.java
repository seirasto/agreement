/**
 * 
 */
package agreement.classifier;

import java.io.*;
//import java.nio.file.Files;
//import java.nio.file.attribute.BasicFileAttributeView;
//import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import edu.columbia.opinion.process.*;
import edu.columbia.opinion.module.*;
import edu.columbia.opinion.io.*;
import edu.columbia.opinion.classifications.*;
import agreement.Arc;
import agreement.Node;
import agreement.Thread;
import agreement.io.ProcessBlog;
import agreement.io.LiveJournalReader;
import blog.*;
import blog.Sentence;
import processing.GeneralUtils;
import processing.SentenceSimilarity;

/**
 * @author sara
 *
 * ss + sentiment: store highest ss score for each phrase with its pair.
 * have polaritysrc and polaritytar feature for phrases with highest ss scores.
 */
public class DataGenerator {

	private String _dataDirectory = null;
    private boolean _livejournal = false;
    private boolean _wikipedia = true;
    
    public static final String SENT_DELIMITERS = "(?<=[.!?])\\s|\\n+";
    
    //public static final String RES_TRAINING_FILE = 
     //   "res/Spencer.csv"; //train2.csv";
    public static final String RES_ENTRIES = 
        "res/entries.txt";

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
		String dataDirectory = "/proj/nlp/users/sara/corpora/discussion_forums/create_debate/religion/training/";
		//DataGenerator dataGenerator = new DataGenerator(dataDirectory,false,true);
		//String outputDirectory = args[1];
		
		String [] files = new File(dataDirectory).list();
		
		double depth = 0;
		
		for (String docId : files) {
			
			try {
				Blog blog = ProcessBlog.readBlog(docId, docId, "create_debate");
				Entry e = blog.getEntry(blog.getEntries().next());
				depth += DataGenerator.depth((blog.threaded.Comment) e.getComment(e.getComments().next()), e);
			}
        	catch (Exception e) {
        		System.err.println(e);
        		e.printStackTrace();
        		continue;
        	}
		}
		System.out.println("DEPTH: " + (depth/files.length));
	}
	
	/*public static void moveIfSameSize(String newPath, String oldPath, String file) throws Exception {
		
		List<String> text = processing.GeneralUtils.readLines(newPath + "/" + file + ".txt");
    	List<String> pos = processing.GeneralUtils.readLines(oldPath + "/" + file + ".txt.emo.phrase.emo");

    	if (pos.size() == text.size()) {
    		
    		File [] fileList = new File(oldPath).listFiles();
    		
    		for (File f : fileList) {
    			if (new File(newPath + "/" + f.getName()).exists()) continue;
    			Files.move(f.toPath(), new File(newPath + "/" + f.getName()).toPath());

    			ssUpdate(f,newPath);

    		}
    		return;
    	}
    	
    	else if (pos.size() == (text.size()*2)) {
    		
    		// write half of file
    		File [] fileList = new File(oldPath).listFiles();
	    	
    		for (File f : fileList) {
    			if (new File(newPath + "/" + f.getName()).exists()) continue;
	    		List<String> data = processing.GeneralUtils.readLines(f.toString());
	    		
	    		if (data.size() == text.size() || data.size() + 1 == text.size()) {
	    			Files.move(f.toPath(), new File(newPath + "/" + f.getName()).toPath());	
	    		}
	    		else {
	    			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newPath + "/" + f.getName()),"UTF-8"));
	    			
	    			for (int i = 0; i < data.size()/2; i++) {
	    				writer.write(data.get(i) + "\n");
	    			}
	    			writer.close();
	    			f.delete();
	    			
	    			ssUpdate(f,newPath);
	    		}
	    	}  
    	}
    	else {
    		// delete and rerun!
    		System.out.println("NO GOOD!");
    	}
	}
	
	private static void ssUpdate(File f, String newPath) throws Exception {
		if (f.getName().endsWith(".ss")) {
			List<String> data = processing.GeneralUtils.readLines(newPath + "/" + f.getName());
			boolean good = false;
			for (int i = 1; i < data.size(); i++) {
				if (!data.get(i).matches("(0 )+")) {
					good = true;
					break;
				}
			}
				
			if (!good) {
				new File(newPath + "/" + f.getName()).delete();
				return;
			}
		}
		if (f.getName().endsWith("phrase.ss")) {
			Files.move(new File(newPath + "/" + f.getName()).toPath(), new File(newPath + "/" + f.getName().replace("phrase.ss", "phrase.emo.ss")).toPath());	
		}
	}*/
	
	public DataGenerator(String dataDirectory, boolean livejournal, boolean wikipedia) {
		_dataDirectory = dataDirectory;
		_livejournal = livejournal;
		_wikipedia = wikipedia;
	}
	
	public HashMap<String,HashMap<String,Arc>> generate(boolean balanced, String trainingDirectory) {
		
		String [] files = new File(trainingDirectory).list();	
		return generate(balanced, trainingDirectory, files);
	}
	
	public HashMap<String,HashMap<String,Arc>> generate(boolean balanced, String folder, String [] trainingFiles) {
		
		Hashtable<String,String> annotations = new Hashtable<String,String>();
		
		for (int index = 0; index < trainingFiles.length; index++) {
			System.out.println("[DataGenerator.generate] load " + trainingFiles[index]);
			List<String> lines = GeneralUtils.readLines(folder + trainingFiles[index]);
			
			for (String line : lines) {
				if (line.isEmpty()) continue;
				String [] parts = line.split(";", 3);
				
				if (!parts[2].startsWith("[{"))
					parts = line.split(";", 4);

				String id = parts[0];
				String annotation = "";
				
				if (parts.length == 4) {
					id = parts[2];
					annotation = parts[3];
				}
				else {
					annotation = parts[2];
				}
				
				if (annotation.isEmpty()) continue;
				
				// concatenate the two JSON arrays
				if (annotations.get(id) != null) 
					annotations.put(id, annotations.get(id).substring(0,annotations.get(id).length()-1) + "," + annotation.substring(1));
				else annotations.put(id, annotation);
			}
		}
		
		//return generateWithNone(balanced, annotations);
		return generate(balanced, annotations);
	}
	
	private Arc createArc(String docId, JSONObject annotation, Entry entry, Blog blog) {

		String targetId = (String)annotation.get("targetId");
        String subjectId = (String)annotation.get("subjectId");
        String type = (String)annotation.get("type");
               
        String commentURL = subjectId.equals("root") ? entry.getURL() : entry.getURL().indexOf("livejournal") >= 0 ? entry.getURL() + "?thread=" + subjectId.substring(1) + "#" + subjectId : subjectId;
        Object subject = subjectId.equals("root") ? entry : (blog.threaded.Comment)entry.getComment(commentURL);
        commentURL = targetId.equals("root") ? entry.getURL() : entry.getURL().indexOf("livejournal") >= 0 ? entry.getURL() + "?thread=" + targetId.substring(1) + "#" + targetId : targetId;
        Object target = targetId.equals("root") ? entry : (blog.threaded.Comment)entry.getComment(commentURL);
        String subjectText = stripQuotes(annotation.get("subject").toString()).replaceAll("`", "'");
        String targetText = stripQuotes(annotation.get("target").toString()).replaceAll("`", "'");
        
        Object [] pair = {subject,target};
        
        boolean isAncestor = isAncestor(pair, entry);
        
        if (!subject.equals(pair[0])) {
	        subject = pair[0];
	        target = pair[1];
	        subjectId = targetId;
	        targetId = (String)annotation.get("subjectId");
	        String tmp = subjectText;
	        subjectText = targetText;
	        targetText = tmp;
        }
        
    	int depthSbj = subject.equals(entry) ? 0 : depth((blog.threaded.Comment)subject, entry);
       	int depthTar = target.equals(entry) ? 0 : depth((blog.threaded.Comment)target, entry);   
    	
        
        subjectText = preFormat(subjectText);
        targetText = preFormat(targetText);
        
        int arcType = 0; 
        
        if (type.equals("agreement")) { arcType = Arc.AGREEMENT; }
        else if (type.equals("disagreement")) { arcType = Arc.DISAGREEMENT; }
        else {arcType = Arc.NONE;}
        
        blog.threaded.Comment sbj = (blog.threaded.Comment)entry.getComment(subjectId);
        blog.threaded.Comment tar = (blog.threaded.Comment)entry.getComment(targetId);
        
        // position as start (0), middle (1), or end (2)
        int sbjPos = getSentencePosition(subjectId.equals("root") ? entry.getEntryText() : sbj.getCommentText(),subjectText);
        int tarPos = getSentencePosition(targetId.equals("root") ? entry.getEntryText() : tar.getCommentText(),targetText);
     
        int sbjResponses = getNumResponses(entry, sbj);
        int tarResponses = getNumResponses(entry, tar);
        
        Node s = new Node(subjectId, subjectText, subjectId.equals("root") ? blog.getUsername() : ((Comment)subject).getUsername(),depthSbj,
        		sbjPos, sbjResponses, -1);
        Node t = new Node(targetId, targetText, targetId.equals("root") ? blog.getUsername() : ((Comment)target).getUsername(),depthTar,
        		tarPos, tarResponses, -1);
		
        Arc a = new Arc(s, t, isAncestor, arcType);
        return a;
	}
	
    /**
     * This function will read all the annotated arcs and add an equivalent number (or all)
     * of unannotated arcs to create a annotated data file.
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    /*private ArrayList<Arc> generateWithNone(boolean balanced, Hashtable<String,String> annotationLines) {

        int docId = 0;

        ArrayList<Arc> arcs = new ArrayList<Arc>();

        //String l;
        int count = 0;
        int none = 0, agreement = 0, disagreement = 0;
        
        for (String id : annotationLines.keySet()) {

        	JSONArray annotations = null;
        	
        	try {
        		annotations = JSONArray.fromObject(annotationLines.get(id));
        	} catch (Exception e) {
        		System.err.println(annotationLines.get(id));
        		e.printStackTrace();
        		System.exit(0);
        	}
            
            if (id.indexOf("wikipedia") < 0 && !_livejournal) continue;
            else if (id.indexOf("wikipedia") >= 0 && !_wikipedia) continue;
            
            Blog blog;
            
            try {
            	if (new File(id).exists()) blog = Blog.processBlog(id);
            	else blog = readBlog(id);
            } catch(Exception e) {
                blog = null;
            }

            if(blog == null || !blog.getEntries().hasNext()) {
                System.out.println("CAN'T READ ENTRY WITH ID " + id);
                continue;
            } else  System.out.println("Read entry with ID " + id);
            
            count++;

            // fetch corresponding blog for text extraction
            Entry entry =
                (blog.livejournal.Entry)blog.getEntry((String)blog.getEntries().next());
            
            HashMap<String, String> seenAnns = new HashMap<String, String>();
            
            // add all the annotated arcs to the arc array
            for(int i = 0; i < annotations.size(); i++) {
            	Arc a = createArc(docId, (JSONObject)annotations.get(i), seenAnns, entry, blog);
                
                if (a.getType() == Arc.NONE) agreement++;
                else if (a.getType() == Arc.DISAGREEMENT) disagreement++;
                else none++;
                
                arcs.add(a);
            }
            docId++;
        }

        System.out.println(count + " files read. ");
        System.out.println("agreement: " + agreement + " disagreement: " + disagreement + " none: " + none);
        //reader.close();

        return arcs;

    }*/
	
	/*public List<Arc> generateAllArcs(HashMap<String,Arc> arcList, String documentId, String subjectId, String targetId,
			List<String> opinion, List<String> polarity, List<String> pos, List<String> ss, List<List<String []>> phraseSS) {
		List<Arc> arcs = new ArrayList<Arc>();
		
		Blog blog;
        
        try {
        	if (new File(documentId).exists()) blog = Blog.processBlog(documentId);
        	else blog = readBlog(documentId);
        } catch(Exception e) {
            blog = null;
        }
        
        return arcs;
	}*/

	/*public List<Arc> generatePostArcs(String id, Arc arc, HashMap<String,Arc> arcList, String processingDirectory,
			List<String> opinion, List<String> polarity, List<String> pos, List<String> ss, List<List<String []>> phraseSS)  {
		
		List<Arc> arcs = new ArrayList<Arc>();
		
		//String id = arc.getDocId();
		
		Blog blog;
        
        try {
        	blog = Blog.processBlog(processingDirectory + "/processed-" + new File(id).getName() + "/" + new File(id).getName() + ".sentences");
        } catch(Exception e) {
            blog = null;
        }

        // fetch corresponding blog for text extraction
        Entry entry =
            (blog.livejournal.Entry)blog.getEntry((String)blog.getEntries().next());
       
        ArrayList<blog.livejournal.Sentence> subjectSentences = arc.getSubject().getId().equals("root") ? entry.getSentences() : //.getEntryText() : 
        	((blog.livejournal.Comment)entry.getComment(arc.getSubject().getId())).getSentences(); // .getCommentText();
        ArrayList<blog.livejournal.Sentence> targetSentences = arc.getTarget().getId().equals("root") ? entry.getSentences() : 
        	((blog.livejournal.Comment)entry.getComment(arc.getTarget().getId())).getSentences();
  
        for (int s = 0; s < subjectSentences.size(); s++) {
        	String sentenceS = preFormat(subjectSentences.get(s).getText().trim()); //+ (s < subject.length-1 ? "." : "");
			int txtPosSBJ = this.getDocumentPositionById(processingDirectory, new File(id).getName(), arc.getSubject().getId(), s);
        	
        	for (int t = 0; t < targetSentences.size(); t++) {
        		String sentenceT = preFormat(targetSentences.get(t).getText().trim()); //+ (t < target.length-1 ? "." : "");
    			int txtPosTAR = this.getDocumentPositionById(processingDirectory, new File(id).getName(), arc.getTarget().getId(), t);
        		
        		if (arcList.containsKey(arc.getKey())) {
        			//System.err.println("[DataGenerator.generatePostArcs] added annotated pair: " + arc.getDocId() + ";" + arc.getSubject().getId() + ";" + arc.getTarget().getId()
        			//		+ ";" + sentenceS + ";" + sentenceT);
        			Arc a = arcList.get(arc.getKey());
        			a.setExactSentencePosition(s,t);
        			arcs.add(a);
        		}
        		// if subject and target got reversed somehow
        		else if (arcList.containsKey(arc.getSubject().getId() + ";" + arc.getTarget().getId() + ";" + sentenceT + ";" + sentenceS)) {
        			System.err.println("[DataGenerator.generatePostArcs] subject and target reversed: " + id + ";" + arc.getSubject().getId() + ";" + arc.getTarget().getId());
        			Arc a = arcList.get(arc.getSubject().getId() + ";" + arc.getTarget().getId() + ";" + sentenceT + ";" + sentenceS);
        			a.setExactSentencePosition(t, s);
        			arcs.add(a);
        		}
        		else {
        			
	        		int sbjPos = 1;
	        		int tarPos = 1;
	        		
	        		if (s == 0) sbjPos = 0;
	        		if (s == subjectSentences.size()-1) sbjPos = 2;
	        		
	        		if (t == 0) tarPos = 0;
	        		if (t == targetSentences.size()-1) tarPos = 2;
	        		
        			Node subject = new Node(arc.getSubject().getId(), sentenceS, arc.getSubject().getAuthor(), arc.getSubject().getDepth(), 
        					sbjPos, arc.getSubject().getNumResponses(),s);
        			Node target = new Node(arc.getTarget().getId(), sentenceS, arc.getTarget().getAuthor(), arc.getTarget().getDepth(), 
        					tarPos, arc.getTarget().getNumResponses(),t);
	        		
        			// add whether we use or not.
    				subject.addSingleOpinion(SentimentResult.ProcessLabeledSentence(opinion.get(txtPosSBJ))); 
    				target.addSingleOpinion(SentimentResult.ProcessLabeledSentence(opinion.get(txtPosTAR)));
    				subject.addSinglePolarity(SentimentResult.ProcessLabeledSentence(polarity.get(txtPosSBJ)));
    				target.addSinglePolarity(SentimentResult.ProcessLabeledSentence(polarity.get(txtPosTAR)));
    				subject.addSinglePOS(pos.get(txtPosSBJ));
    				target.addSinglePOS(pos.get(txtPosTAR));
    				
    				Arc a = new Arc(subject, target, arc.isAncestor(), Arc.NONE);
    				a.addSentenceSimilarity(SentenceSimilarity.cosineSimilarity(ss.get(txtPosSBJ+1).split(" "), ss.get(txtPosTAR + 1).split(" ")));
           			a.addPhraseSimilarity(this.computePhraseSimilarity(phraseSS.get(txtPosSBJ), phraseSS.get(txtPosTAR)));
	        		
	        		arcs.add(a);
        		}
        	}
        }
        return arcs;
	}*/
    
	public static String normalizeSentence(String s) {
		s = s.replaceAll("\",\"", "DELIM");
		s = preFormat(s);
		s = s.replaceAll("(" + SENT_DELIMITERS + ")", "$1DELIM");
		return s;
	}
	
    /**
     * This function will read all the annotated arcs and add an equivalent number (or all)
     * of unannotated arcs to create a annotated data file.
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private HashMap<String,HashMap<String,Arc>> generate(boolean balanced, Hashtable<String,String> annotationLines) {

        //int docId = 0;

        HashMap<String,HashMap<String,Arc>> arcs = new HashMap<String,HashMap<String,Arc>>();

        //String l;
        int count = 0;
        int none = 0, agreement = 0, disagreement = 0;
        
        for (String id : annotationLines.keySet()) {
        	int a = 0; int d = 0; int n = 0;

        	if (id == null || annotationLines.get(id) == null) {
        		continue;
        	}
        	
        	JSONArray annotations = null;
        	
        	try {
        		annotations = JSONArray.fromObject(annotationLines.get(id));
        	} catch (Exception e) {
        		System.err.println(id + ": " + annotationLines.get(id));
        		e.printStackTrace();
        		System.exit(0);
        	}
            
            if (id.indexOf("wikipedia") < 0 && !_livejournal) continue;
            else if (id.indexOf("wikipedia") >= 0 && !_wikipedia) continue;
            
            Blog b;
            
            try {
            	if (new File(id).exists()) b = blog.threaded.livejournal.Blog.processBlog(id);
            	else b = LiveJournalReader.readBlog(id, _dataDirectory + "/" + RES_ENTRIES, false);
            } catch(Exception e) {
                b = null;
            }

            if(b == null || !b.getEntries().hasNext()) {
                System.out.println("CAN'T READ ENTRY WITH ID " + id);
                continue;
            } else {
            	System.out.println("Read entry with ID " + id);
            }
            
            count++;

            // fetch corresponding blog for text extraction
            Entry entry =
                (Entry)b.getEntry((String)b.getEntries().next());
            
            HashMap<String, Arc> seenAnns = new HashMap<String, Arc>();
            //int averageNumSentences = 0;
            
            // add all the annotated arcs to the arc array
            // check for duplicates - remove inconsistencies
            for(int i = 0; i < annotations.size(); i++) {
            	Arc arc = createArc(id, (JSONObject)annotations.get(i), entry, b);
            	
            	String key = arc.getKey();
            	key = key.trim();
            	
                if (!seenAnns.containsKey(key)) seenAnns.put(key,arc);
                else if (seenAnns.get(key) == null || seenAnns.get(key).getType() != arc.getType()) {
                	System.err.println("Disagreement between annotators. Discarding: " + (seenAnns.get(key) != null ? seenAnns.get(key).getType() : "#") + 
                			"<->" + arc.getType() + ", " + key);
                	
                	seenAnns.put(key,null);
                }
                else {
                	System.err.println("Agreement between annotators: " + seenAnns.get(key).getType() + 
                			"<->" + arc.getType() + ", " + key);
                }

                //averageNumSentences += MaxentTagger.tokenizeText(new StringReader(subjectText)).size();
                //averageNumSentences += MaxentTagger.tokenizeText(new StringReader(targetText)).size();
                
            	//averageNumSentences += a.getSubjectSentences().length + a.getTargetSentences().length;
            	
                //subjectText = preFormat(subjectText);
                //targetText = preFormat(targetText);
                
                if (arc.getType() == Arc.AGREEMENT) { agreement++; a++; }
                else if (arc.getType() == Arc.DISAGREEMENT) { disagreement++; d++; }
                else { none++; n++; }
                
                //arcs.add(arc);
            }
            
            arcs.put(id,seenAnns);
            
            // only add random "none pair" annotations if none exist
            if (n < a + d) {
            	System.err.println("Missing " + ((a+d) - n) + " none annotations: " + id);
            }           
        }

        System.out.println(count + " files read. ");
        System.out.println("agreement: " + agreement + " disagreement: " + disagreement + " none: " + none);
        //reader.close();

        return arcs;

    }
    
    /*public ArrayList<Arc> addDocumentFeatures(ArrayList<Arc> arcs) {
    	
    	for (Arc arc : arcs) {
    		
    		Blog blog = null;
    		
    		try {
    			blog = readBlog(Integer.valueOf(arc.getDocId()).toString());
    		} catch (Exception e) {
    			e.printStackTrace();
    			continue;
    		}
            
            // fetch corresponding blog for text extraction
            Entry entry =
                (blog.livejournal.Entry)blog.getEntry((String)blog.getEntries().next());
            
            Comment sbj = (blog.livejournal.Comment)entry.getComment(arc.getSubject().getId());
            Comment tar = (blog.livejournal.Comment)entry.getComment(arc.getTarget().getId());
            
            // position as start (0), middle (1), or end (2)
            int sbjPos = getSentencePosition(sbj.getCommentText(),arc.getSubjectBody());
            int tarPos = getSentencePosition(tar.getCommentText(),arc.getTargetBody());
         
            //int sbjResponses = getNumResponses(entry, sbj);
            int tarResponses = getNumResponses(entry, tar);
    	}
    	
    	return arcs;
    }*/
    
    private String stripQuotes(String string) {
    	if (string.matches("\\[.*\\]")) {
    		string = string.substring(1,string.length()-1);
    	}
    	if (string.matches("\".*\"")) {
    		return string.substring(1,string.length()-1);
    	}
    	return string;
    }
    
    private int getNumResponses(Entry e, blog.threaded.Comment s) {
    	int count = 0;
    	
		Iterator<String> comments = e.getComments();
    	
    	// root, return # of comments
    	if (s == null) return e.getNumComments();
    	
    	while (comments.hasNext()) {
    		String url = comments.next();
    		
    		blog.threaded.Comment c  = (blog.threaded.Comment)e.getComment(url);
    		
    		if (c.getParentURL().equals(s.getParentURL())) count++;
    	}
    	
    	return count;
    }
    
    private int getSentencePosition(String comment, String sentence) {
    	if (comment.startsWith(sentence)) return 0;
    	if (comment.endsWith(sentence)) return 2;
    	return 1;
    }
    
    /*public List<Arc> generateAllArcs(String file, HashMap<String,Arc> annotated, List<String> ids, List<String> opinion, 
    		List<String> polarity, List<String> pos, List<String> ss, List<List<String []>> phraseSS, boolean differentiatePosts) {
        
    	Blog blog = null;
        
    	try {
            blog = Blog.processBlog(file);
        } catch(Exception e) {
           e.printStackTrace();
        }
    	
    	HashMap<String,Integer> idHash = hashId(ids);
        
        // fetch corresponding blog for text extraction
        Entry entry =
            (blog.livejournal.Entry)blog.getEntry((String)blog.getEntries().next());
        
        List<Arc> arcs = new ArrayList<Arc>();
        
        // add all the sentences to a list and then create all possible arcs
        List<blog.livejournal.Sentence> entrySentences = entry.getSentences();
        
        Object [] comments = entry.getSortedComments();
       // int [] commentSize = new int [comments.length+1];
        //commentSize[0] = entrySentences.size();
        
        for (int index = 0; index < comments.length; index++) {
        	
        	Comment subject = (blog.livejournal.Comment)comments[index];
        	// check this
        	List<blog.livejournal.Sentence> subjectSentences = subject.getSentences();
        	String subId = shortEntryId(subject.getURL());
        	int depthSbj = depth(subject, entry);
        	//if (index == 0) commentSize[index+1] = commentSize[index] + subjectSentences.size();

            int entryResponses = comments.length;
            int subResponses = getNumResponses(entry, subject);
        	
        	//1. create entry, comment arc for each sentence
        	int ei = 0;
        	for (blog.livejournal.Sentence entrySentence : entrySentences) {
        		
        		int si = 0;
        		for (blog.livejournal.Sentence subjectSentence : subjectSentences) {
        			String sText = preFormat(entrySentence.getText());
        			String tText = preFormat(subjectSentence.getText());
        			
        			String key = "root;" + subId + ";" + sText + ";" + tText;
        			
        			if (annotated != null && annotated.containsKey(key)) {
        				Arc a = annotated.get(key);
            			a.getSubject().addDocumentFeatures(a.getSubject().getApproxSentencePosition(), a.getSubject().getNumResponses(), ei);
            			a.getTarget().addDocumentFeatures(a.getTarget().getApproxSentencePosition(), a.getTarget().getNumResponses(), si);
        				arcs.add(a);
        				continue;
        			}
        			
        			int sPosition = idHash.get(subId + "-"+ si);
        			int ePosition = idHash.get("root-" + ei);
	        		
           			int sbjPos = 1;
	        		int tarPos = 1;
	        		
	        		if (ei == 0) sbjPos = 0;
	        		if (ei == entrySentences.size()-1) sbjPos = 2;
	        		
	        		if (si == 0) tarPos = 0;
	        		if (si == subjectSentences.size()-1) tarPos = 2;
	        		
	        		Node s = new Node("root", sText, blog.getUsername(), 0, sbjPos, entryResponses, 
	        				ePosition, opinion.get(ePosition), polarity.get(ePosition), pos.get(ePosition));
        			Node t = new Node(subId, tText, subject.getUsername(), depthSbj, tarPos,subResponses, 
        					sPosition, opinion.get(sPosition), polarity.get(sPosition), pos.get(sPosition));
        			
        			Arc a = new Arc(s, t, true, annotated == null ? Arc.UNKNOWN : Arc.NONE);
	        		a.addSentenceSimilarity(SentenceSimilarity.cosineSimilarity(ss.get(ePosition+1).split(" "), ss.get(sPosition+1).split(" ")));
           			a.addPhraseSimilarity(this.computePhraseSimilarity(phraseSS.get(ePosition), phraseSS.get(sPosition)));
        			arcs.add(a);
        			if (arcs.size() > 100000) return null;
        			si++;
        		}
        		ei++;
        	}
        	
        	if (differentiatePosts) arcs.add(null);
        	
        	//2. create comment, commment arc for each sentence
        	for (int jindex = index+1; jindex < comments.length; jindex++) {
        		Comment target = (blog.livejournal.Comment)comments[jindex];
        		
        		// do this first to ensure the subject and target are in the correct order.
                Object [] pair = {subject,target};
                
                boolean isParent = isParent(pair, entry);
                
                subject = (Comment)pair[0];
                target = (Comment)pair[1];

        		String sId = shortEntryId(subject.getURL());
        		String tId = shortEntryId(target.getURL());
                // depth from root
               	int depthTar = depth(target, entry);  
               	
               	List<blog.livejournal.Sentence> tSentences = target.getSentences();
               	List<blog.livejournal.Sentence> sSentences = subject.getSentences();
            	//if (index == 0) commentSize[jindex+1] = commentSize[jindex] + tSentences.size();
               	
                int tarResponses = getNumResponses(entry, target);
                int sbjResponses = getNumResponses(entry, subject);
                
            	int si = 0;
               	for (blog.livejournal.Sentence subjectSentence : sSentences) {
               		int ti = 0;
            		for (blog.livejournal.Sentence targetSentence : tSentences) {
            			
            			String sText = preFormat(subjectSentence.getText());
            			String tText = preFormat(targetSentence.getText());
            			
            			String key = sId + ";" + tId + ";" + sText + ";" + tText;
            			String reverseKey = tId + ";" + sId + ";" + tText + ";" + sText;
            			
            			if (annotated != null && annotated.containsKey(key)) {
                			Arc a = annotated.get(key);
                			a.addDocumentFeatures(a.getSubjectSentencePosition(), a.getTargetSentencePosition(), a.getNumResponsesToSubject(), a.getNumResponsesToTarget(), si, ti);
            				arcs.add(a);
            				continue;
            			}
            			else if (annotated != null && annotated.containsKey(reverseKey)) {
            				Arc a = annotated.get(reverseKey);
                			a.addDocumentFeatures(a.getSubjectSentencePosition(), a.getTargetSentencePosition(), a.getNumResponsesToSubject(), a.getNumResponsesToTarget(), ti, si);
            				arcs.add(a);
            			}
            			
            			int sPosition = idHash.get(sId + "-" + si);
            			int tPosition = idHash.get(tId + "-" + ti);
            			
            			Arc a = new Arc(sId, tId, sText, tText,
            					subject.getUsername(), target.getUsername(),
            					depthSbj, depthTar, isParent, annotated == null ? Arc.UNKNOWN : Arc.NONE, file);
             			a.addOpinion(SentimentResult.ProcessLabeledSentence(opinion.get(sPosition)), 
            					SentimentResult.ProcessLabeledSentence(opinion.get(tPosition)));
             			a.addPolarity(SentimentResult.ProcessLabeledSentence(polarity.get(sPosition)), 
            					SentimentResult.ProcessLabeledSentence(polarity.get(tPosition)));
             			a.addPOS(pos.get(sPosition), pos.get(tPosition));
             			
                		
               			int sbjPos = 1;
    	        		int tarPos = 1;
    	        		
    	        		if (si == 0) sbjPos = 0;
    	        		if (si == sSentences.size()-1) sbjPos = 2;
    	        		
    	        		if (ti == 0) tarPos = 0;
    	        		if (ti == tSentences.size()-1) tarPos = 2;
    	        		
    	        		a.addDocumentFeatures(sbjPos,tarPos,sbjResponses,tarResponses,sPosition,tPosition);
        				a.addSentenceSimilarity(SentenceSimilarity.cosineSimilarity(ss.get(sPosition+1).split(" "), ss.get(tPosition+1).split(" ")));
               			a.addPhraseSimilarity(this.computePhraseSimilarity(phraseSS.get(sPosition), phraseSS.get(tPosition)));
            			arcs.add(a);
            			ti++;
            		}
            		si++;
            	}
            	if (differentiatePosts) arcs.add(null);
               	if (arcs.size() > 100000) return null;
        	}
        }
        
        return arcs;
    } */
    
    private Arc generateArcForPost(String id, HashMap<String,Arc> annotated, Blog blog, String sId, String tId, List<String> ids, List<String> opinion, 
    		List<String> polarity, List<String> pos, List<String> ss, List<List<String []>> phraseSS, boolean isParent) {
    	
    	HashMap<String,Integer> idHash = hashId(ids);
    	Entry entry = (Entry)blog.getEntry(blog.getEntries().next());
    	
    	Object subject = sId.equals("root") ? entry : entry.getComment(sId);
    	Object target = blog.getEntry(blog.getEntries().next()).getComment(tId);
    			
    	List<blog.Sentence> sSentences = !sId.equals("root") ? ((blog.threaded.Comment)subject).getSentences() : 
    		((Entry)subject).getSentences();
    	List<blog.Sentence> tSentences = ((blog.threaded.Comment)target).getSentences();
    	
    	int depthSbj = sId.equals("root") ? 0 : depth((blog.threaded.Comment)subject, entry);
    	int depthTar = depth((blog.threaded.Comment)target, entry);
    	int tarResponses = getNumResponses(entry, (blog.threaded.Comment)target);
        int sbjResponses = sId.equals("root") ? entry.getNumComments() : getNumResponses(entry, (blog.threaded.Comment)subject);
        
        String sUserName = subject.getClass().equals(blog.threaded.Comment.class) ? ((blog.threaded.Comment)subject).getUsername() : 
    		blog.getUsername();
        String tUserName = ((blog.threaded.Comment)target).getUsername();
		
        List<String> sBody = new ArrayList<String>();
        List<String> tBody = new ArrayList<String>();
        List<String> sOpinion = new ArrayList<String>();
        List<String> tOpinion = new ArrayList<String>();
        List<String> sPolarity = new ArrayList<String>();
        List<String> tPolarity = new ArrayList<String>();
        List<String> sPOS = new ArrayList<String>();
        List<String> tPOS = new ArrayList<String>();
        List<Integer> types = new ArrayList<Integer>();
        List<Double> ssValues = new ArrayList<Double>(); 
        List<String> phraseSSValues = new ArrayList<String>();
    	
    	for (blog.Sentence subjectSentence : sSentences) {
       		int si = 0;
    		
       		for (blog.Sentence targetSentence : tSentences) {
    			
       			int ti = 0;
    			
    			String sText = preFormat(subjectSentence.getText());
    			String tText = preFormat(targetSentence.getText());
    			

    			int sPosition = idHash.get(sId + "-" + si);
    			int tPosition = idHash.get(tId + "-" + ti);
    			
    			String key = sId + ";" + tId + ";" + sText + ";" + tText;
    			if (annotated != null && annotated.containsKey(key)) {
    				types.add(annotated.get(key).getType());
    			}
    			else types.add(Arc.NONE);
    			
    			sBody.add(sText);
    			tBody.add(tText);
	    		sOpinion.add(opinion.get(sPosition)); 
	    		tOpinion.add(opinion.get(tPosition));
	     		sPolarity.add(polarity.get(sPosition)); 
	    		tPolarity.add(polarity.get(tPosition));
	     		sPOS.add(pos.get(sPosition));
	     		tPOS.add(pos.get(tPosition));
	     		ssValues.add(SentenceSimilarity.cosineSimilarity(ss.get(sPosition+1).split(" "), ss.get(tPosition+1).split(" ")));
	     		phraseSSValues.add(this.computePhraseSimilarity(phraseSS.get(sPosition), phraseSS.get(tPosition)));	
	     		ti++;
    		}
    		si++;
    	}
    	
        Node s = new Node(sId, sBody, sUserName, depthSbj, 0, sbjResponses, 0, sOpinion, sPolarity, sPOS);
    	Node t = new Node(tId, tBody, tUserName, depthTar, 0, tarResponses, 0, tOpinion, tPolarity, tPOS);
    	Arc a = new Arc(s, t, isParent, Arc.computeType(types, tBody));
		a.addSentenceSimilarity(ssValues);
		a.addPhraseSimilarityFromString(phraseSSValues);		
    	return a;
    }
    
    public List<Arc> generateSentenceArcsForPost(String id, HashMap<String,Arc> annotated, Blog blog, String sId, String tId, List<String> ids, List<String> opinion, 
    		List<String> polarity, List<String> pos, List<String> ss, List<List<String []>> phraseSS, boolean isParent) {
    	
    	List<Arc> arcs = new ArrayList<Arc>();
    	
    	HashMap<String,Integer> idHash = hashId(ids);
    	blog.Entry entry = blog.getEntry(blog.getEntries().next());
    	
    	Object subject = sId.equals("root") ? entry : entry.getComment(sId);
    	Object target = blog.getEntry(blog.getEntries().next()).getComment(tId);
    			
    	List<blog.Sentence> sSentences = !sId.equals("root") ? ((Comment)subject).getSentences() : 
    		((Entry)subject).getSentences();
    	List<blog.Sentence> tSentences = ((Comment)target).getSentences();
    	
    	int depthSbj = sId.equals("root") ? 0 : depth((blog.threaded.Comment)subject, entry);
    	int depthTar = depth((blog.threaded.Comment)target, entry);
    	int tarResponses = getNumResponses(entry, (blog.threaded.Comment)target);
        int sbjResponses = sId.equals("root") ? entry.getNumComments() : getNumResponses(entry, (blog.threaded.Comment)subject);
        
        String sUserName = subject.getClass().equals(blog.threaded.Comment.class) ? ((blog.threaded.Comment)subject).getUsername() : 
    		blog.getUsername();
        String tUserName = ((blog.threaded.Comment)target).getUsername();
		
    	for (blog.Sentence subjectSentence : sSentences) {
       		int si = 0;
       		
       		String sText = preFormat(subjectSentence.getText());
       		int sPosition = idHash.get(sId + "-" + si);
       		
       		Node s = new Node(sId, sText, sUserName, depthSbj, 0, sbjResponses, 0, opinion.get(sPosition), polarity.get(sPosition), pos.get(sPosition));
     		
       		for (blog.Sentence targetSentence : tSentences) {
    			
       			int ti = 0;
    			
    			String tText = preFormat(targetSentence.getText());
    			int tPosition = idHash.get(tId + "-" + ti);
    			
    			String key = sId + ";" + tId + ";" + sText + ";" + tText;
    			if (annotated != null && annotated.containsKey(key)) {
    				arcs.add(annotated.get(key));
    			}
    			else {    			
		            Node t = new Node(tId, tText, tUserName, depthTar, 0, tarResponses, 0, opinion.get(tPosition), polarity.get(tPosition), pos.get(tPosition));
		        	Arc a = new Arc(s, t, isParent, Arc.NONE);
		    		a.addSingleSentenceSimilarity(SentenceSimilarity.cosineSimilarity(ss.get(sPosition+1).split(" "), ss.get(tPosition+1).split(" ")));
		    		a.addSinglePhraseSimilarityFromString(this.computePhraseSimilarity(phraseSS.get(sPosition), phraseSS.get(tPosition)));	
		    		ti++;
		    		arcs.add(a);
    			}
    		}
    		si++;
    	}
    		
    	return arcs;
    }
    
    public HashMap<String,List<Arc>> getAgreementByPost(HashMap<String,Arc> annotated) {
    	HashMap<String,List<Arc>> postsWithAgreement = new HashMap<String,List<Arc>>();
    	
    	for (Arc arc : annotated.values()) {
    		boolean reverse = false;
    		List<Arc> arcs = new ArrayList<Arc>();
    		
    		if (postsWithAgreement.containsKey(arc.getSubject().getId() + ";" + arc.getTarget().getId())) {
    				arcs = postsWithAgreement.get(arc.getSubject().getId() + ";" + arc.getTarget().getId());
    		}
    		if (postsWithAgreement.containsKey(arc.getTarget().getId() + ";" + arc.getSubject().getId())) {
				arcs = postsWithAgreement.get(arc.getTarget().getId() + ";" + arc.getSubject().getId());
				reverse = true;
    		}
    		arcs.add(arc);
    		if (reverse) postsWithAgreement.put(arc.getTarget().getId() + ";" + arc.getSubject().getId(), arcs);
    		else	postsWithAgreement.put(arc.getSubject().getId() + ";" + arc.getTarget().getId(), arcs);
    	}
    	return postsWithAgreement;
    }
    
    public List<Arc> generateAllSentenceArcs(String file, HashMap<String,Arc> annotated, List<String> ids, List<String> opinion, 
    		List<String> polarity, List<String> pos, List<String> ss, List<List<String []>> phraseSS, boolean differentiatePosts, String source) {
    	
    	List<Arc> arcs = new ArrayList<Arc>();
    	
    	Blog blog = ProcessBlog.readBlog(file, file, source);
    	
        // fetch corresponding blog for text extraction
        Entry entry =
            (blog.Entry)blog.getEntry((String)blog.getEntries().next());
        
        Object [] comments = entry.getSortedComments();

        for (int index = 0; index < comments.length; index++) {
        	
        	Comment subject = (blog.threaded.Comment)comments[index];
        	
        	String subId = shortEntryId(subject.getURL());
        	
        	// iterate through sentences, generate a arc for each pair of sentences
            arcs.addAll(this.generateSentenceArcsForPost(file, annotated, blog, "root", subId, ids, opinion, polarity, pos, ss, phraseSS, true));
            if (differentiatePosts) arcs.add(null);
            
        	//2. create comment, commment arc for each sentence
        	for (int jindex = index+1; jindex < comments.length; jindex++) {
        		Comment target = (blog.threaded.Comment)comments[jindex];
        		
        		// do this first to ensure the subject and target are in the correct order.
                Object [] pair = {subject,target};
                
                boolean isParent = isParent(pair, entry);
                
                Comment s = (Comment)pair[0];
                Comment t = (Comment)pair[1];

        		String sId = shortEntryId(s.getURL());
        		String tId = shortEntryId(t.getURL());
                
                arcs.addAll(this.generateSentenceArcsForPost(file, annotated, blog, sId, tId, ids, opinion, polarity, pos, ss, phraseSS, isParent));
                if (differentiatePosts) arcs.add(null);
        	}
        }
        
        return arcs;
    }
    
    
    public Thread generateAllPostArcs(String file,  HashMap<String,Arc> annotated, List<String> ids, List<String> opinion, 
    		List<String> polarity, List<String> pos, List<String> ss, List<List<String []>> phraseSS, String source) {
        
    	Thread thread = new Thread(file);
    	
    	Blog blog = ProcessBlog.readBlog(file, file, source);
    	
        // fetch corresponding blog for text extraction
        Entry entry =
            (blog.Entry)blog.getEntry((String)blog.getEntries().next());
        
        Object [] comments = entry.getSortedComments();
    	//String docId = (annotatedByPost.entrySet().iterator().next().getValue().get(0)).getDocId();

        for (int index = 0; index < comments.length; index++) {
        	
        	Comment subject = (blog.threaded.Comment)comments[index];
        	
        	String subId = shortEntryId(subject.getURL());
        	
        	// iterate through sentences and generate one arc containing all sentences
            Arc arc = this.generateArcForPost(file, annotated, blog, "root", subId, ids, opinion, polarity, pos, ss, phraseSS, true);
            
        	thread.addArc(arc);
        	
        	//2. create comment, commment arc for each sentence
        	for (int jindex = index+1; jindex < comments.length; jindex++) {
        		Comment target = (blog.threaded.Comment)comments[jindex];
        		
        		// do this first to ensure the subject and target are in the correct order.
                Object [] pair = {subject,target};
                
                boolean isParent = isParent(pair, entry);
                
                Comment s = (Comment)pair[0];
                Comment t = (Comment)pair[1];

        		String sId = shortEntryId(s.getURL());
        		String tId = shortEntryId(t.getURL());
                
                arc = this.generateArcForPost(file, annotated, blog, sId, tId, ids, opinion, polarity, pos, ss, phraseSS, isParent);
                thread.addArc(arc);
        	}
        }
        
        return thread;
    }
    
    public Thread generateDirectArcsByPost(String file, List<String> ids, List<String> opinion, 
    		List<String> polarity, List<String> pos, List<String> ss, List<List<String []>> phraseSS, HashMap<String,Arc> annotations, String source) {
        
    	//HashMap<String,List<Arc>> annotatedByPost = getAgreementByPost(annotated);
    	
    	Blog blog = ProcessBlog.readBlog(file, file, source);
    	
        // fetch corresponding blog for text extraction
        Entry entry =
            (blog.Entry)blog.getEntry((String)blog.getEntries().next());
        
        Thread thread = new Thread(file);

        Object [] comments = entry.getSortedComments();
        
        List<String> sentences = sentenceToString(entry.getSentences());
        int position = sentences.size();
        
        int defaultArc = Arc.NONE;
        if (file.indexOf("create_debate") < 0 && annotations == null) defaultArc = Arc.UNKNOWN;
        
        // first "arc" is entry, there is no source.
        Node root = new Node("root", sentences, blog.getUsername(), 0, 0, 0, comments.length,
        		opinion != null ? opinion.subList(0, position) : null,polarity != null ? polarity.subList(0, position) : null,pos != null ? pos.subList(0,position) : null);
        thread.addArc(new Arc(null,root, true, defaultArc));
        thread.addNode(root);
        
        int type = defaultArc;
        
        for (int index = 0; index < comments.length; index++) {
        	
        	List<Double> thisSS = new ArrayList<Double>();
        	ArrayList<String> thisPhraseSS = new ArrayList<String>();
        	blog.threaded.Comment target = (blog.threaded.Comment)comments[index];
        	List<String> sentencesT = sentenceToString(target.getSentences());
            int startT = this.getDocumentPositionById(ids,String.valueOf(target.getURL()), 0);
            int endT = startT+sentencesT.size();
            Node t = new Node(String.valueOf(target.getURL()), sentencesT, target.getUsername(), depth(target, entry), 0, 0, getNumResponses(entry,target),
    				opinion != null && startT >= 0 ? opinion.subList(startT,endT) : null, 
    						polarity != null && startT >= 0 ? polarity.subList(startT,endT) : null, 
    								pos != null && startT >= 0 ? pos.subList(startT, endT) : null);
            Node s = null;
            
            if (target.getParentURL() == null || entry.getComment(target.getParentURL()) == null) {
            	s = root;
            	int startS = 0;
            	int endS = startS + sentences.size();
            	thisSS = ss != null && startT >= 0 ? sentenceSimilarityForList(ss.subList(startS+1,endS+1), ss.subList(startT+1,endT+1)) : null;
	    		thisPhraseSS = phraseSS != null && startT >= 0 ? phraseSimilarityForList(phraseSS,startS,endS, phraseSS,startT,endT) : null;
	    		if (annotations != null) {
	    			type = generatePostAnnotation(annotations,"root",target.getURL());
	    		}
	    		type = Arc.NONE;
            }
            else {
            	blog.Comment subject = entry.getComment(target.getParentURL());
            	
        		if (source.indexOf("create_debate") >= 0) {
        	
        			String sideSubject = ((blog.threaded.create_debate.Comment)subject).getCorrectSide();
            		String sideTarget = ((blog.threaded.create_debate.Comment)target).getCorrectSide();
        			
            		if (subject.getUsername().equals(target.getUsername())
            				|| sideSubject.isEmpty() || sideTarget.isEmpty()) {
            			type = Arc.NONE;
            			if (!sideSubject.equals(sideTarget)) System.out.println("contradiction!");
            		}
            		else if (sideTarget.equals("both sides")) type = Arc.BOTH;
            		else if (sideTarget.equals("no side")) type = Arc.BOTH;
            		else if (sideSubject.equals(sideTarget)) type = Arc.AGREEMENT;
            		else if (!sideSubject.equals(sideTarget)) type = Arc.DISAGREEMENT;
            		
            		if (type == Arc.BOTH) {
	            		String sideSub = ((blog.threaded.create_debate.Comment)subject).getSide();
	            		String sideTar = ((blog.threaded.create_debate.Comment)target).getSide();
	            		
	            		
	            		if (subject.getUsername().equals(target.getUsername())
	            				|| sideSub.isEmpty() || sideTar.isEmpty()) {
	            			type = Arc.NONE;
	            			if (!sideSub.equals(sideTar)) System.out.println("contradiction!");
	            		}
	            		else if (sideSub.equals(sideTar)) type = Arc.AGREEMENT;
	            		else if (!sideSub.equals(sideTar)) type = Arc.DISAGREEMENT;
            		}
        		}
        		else if (source.indexOf("internet_argument_corpus") >= 0) {
                	
            		//String sideSubject = ((blog.threaded.create_debate.Comment)subject).getSide();
            		String sideTarget = ((blog.threaded.create_debate.Comment)target).getSide();
            		
            		if (sideTarget.equals("agree")) type = Arc.AGREEMENT;
            		else if (sideTarget.equals("disagree")) type = Arc.DISAGREEMENT;
            		else if (sideTarget.equals("NA")) type = Arc.UNKNOWN;
            		else type = Arc.NONE;
            		
        		}
        		else if (annotations != null) {
        			type = generatePostAnnotation(annotations,subject.getURL(),target.getURL());
        		}
        		else {
        			type = defaultArc;
        		}
        		
        		List<String> sentencesS = sentenceToString(subject.getSentences());
            	int startS = this.getDocumentPositionById(ids,String.valueOf(subject.getURL()), 0);
            	int endS = startS + sentencesS.size();
            	s = new Node(String.valueOf(subject.getURL()), sentencesS, (subject).getUsername(), depth(target, entry), 0, 0, getNumResponses(entry,(blog.threaded.Comment)subject),
        				opinion != null ? opinion.subList(startS,endS) : null,polarity != null ? polarity.subList(startS,endS) : null, pos != null ? pos.subList(startS,endS) : null);
            	
            	
            	thisSS = ss != null ? sentenceSimilarityForList(ss.subList(startS+1,endS+1), ss.subList(startT+1,endT+1)) : null;
	    		thisPhraseSS = phraseSS != null ? phraseSimilarityForList(phraseSS,startS,endS, phraseSS,startT,endT) : null;
            }
            
            Arc a = new Arc(s,t,true,type);
            a.addSentenceSimilarity(thisSS);
            a.addPhraseSimilarityFromString(thisPhraseSS);
            thread.addArc(a);
            thread.addNode(t);
        }
        
        return thread;
    }
    
    private int generatePostAnnotation(HashMap<String,Arc> annotations, String sId, String tId) {
    	
    	int disagreement = 0;
    	int agreement = 0;
    	int agreementLength = 0;
    	int disagreementLength = 0;
    	
    	for (String key : annotations.keySet()) {
    		String [] data = key.split(";");
    		if (data[0].equals(sId) && data[1].equals(tId)) {
    			if (annotations.get(key).getType() == Arc.AGREEMENT) {
    				agreement++;
    				agreementLength +=data[3].length();
    			}
    			if (annotations.get(key).getType() == Arc.DISAGREEMENT) {
    				disagreement++;
    				disagreementLength += data[3].length();
    			}
    		}
    		else if (data[1].equals(sId) && data[0].equals(tId)) {
    			if (annotations.get(key).getType() == Arc.AGREEMENT) {
    				agreement++;
    				agreementLength +=data[2].length();
    			}
    			if (annotations.get(key).getType() == Arc.DISAGREEMENT) {
    				disagreement++;
    				disagreementLength += data[2].length();
    			}
    		}
    	}
    	
    	if (agreement > 0 || disagreement > 0) System.out.println("a: " + agreement + " d: " + disagreement);
    	
    	if (agreement > disagreement) return Arc.AGREEMENT;
    	if (disagreement > agreement) return Arc.DISAGREEMENT;
    	if (agreement > 0) {
    		// for now 
        	System.out.println("-----> Using length: a: " + agreementLength + " d: " + disagreementLength);
    		if (disagreementLength > agreementLength) return Arc.DISAGREEMENT;
    		else return Arc.AGREEMENT;
    		
    	}
    	return Arc.NONE;
    }
    
    private ArrayList<String> phraseSimilarityForList(List<List<String []>> groupQ, int startQ, int endQ, 
    		List<List<String []>> groupR, int startR, int endR) {
    	
    	ArrayList<String> phraseSS = new ArrayList<String>();
    	
    	// don't compute phrase ss if too many phrases too avoid crashing 
    	// (probably less meaningful in long sentences anyhow)
    	if (startQ + startR > 80) return phraseSS;
    	
    	int i = 0;
    	for (int indexA = startR; indexA < endR; indexA++) {
    		int j = 0;
    		for (int indexB = startQ; indexB < endQ; indexB++) {
    			// j: start of quote
    			// i: start of response
    			String similarity = computePhraseSimilarity(groupQ.get(indexB), groupR.get(indexA));
    			
    			// FORMAT: QUOTE_SENTENCE:RESPONSE_SENTENCE (QUOTE_PHRASE-RESPONSE-PHRASE-SIMILARITY%)+
    			phraseSS.add(j + ":" + i + " " + similarity);
    			j++;
    		}
    		i++;
    	}
    	return phraseSS;
    }
    
    private List<Double> sentenceSimilarityForList(List<String> groupQ, List<String> groupR) {
    	List<Double> ss = new ArrayList<Double>();
    	
    	for (String a : groupR) {
    		double max = 0;
    		
    		for (String b : groupQ) {
    			double value = SentenceSimilarity.cosineSimilarity(a.split(" "), b.split(" "));
    			if (value > max) max = value;
    		}
    		ss.add(max);
    	}
    	return ss;
    }
    
    private List<String> sentenceToString(List<Sentence> sentences) {
    	List<String> s = new ArrayList<String>();
    	
    	for (Sentence sentence : sentences) {
    		s.add(sentence.getText());
    	}
    	return s;
    }
    
    /**
     * Feature selection on opinionated words
     * @param threads
     * @return
     */
	/*public static HashMap<String,HashSet<String>> featureSelection(List<Thread> threads) {
		HashMap<String,HashMap<String,Integer>> classTerms = new HashMap<String,HashMap<String,Integer>>();
		HashMap<String,Integer> inAll = new HashMap<String,Integer>();
		HashMap<String,Integer> numThreads = new HashMap<String,Integer>();
		
		for (Thread thread : threads) {
			for (Arc arc : thread.getArcs()) {
				String type = Arc.typeStr(arc.getType());
				if (type.equals("none")) continue;
				HashMap<String,Integer> terms = classTerms.get(type) == null ? new HashMap<String,Integer>() : classTerms.get(type);
				
				for (String term : arc.getTarget().getSubjectiveWords().split("\\s+")) {
					term = term.toLowerCase().replaceAll("\\p{Punct}", "").trim();
					if (terms.containsKey(term)) {
						terms.put(term,terms.get(term)+1);
					}
					else {
						terms.put(term,1);
						inAll.put(term,inAll.containsKey(term) ? inAll.get(term)+1 : 1);
					}
				}
				classTerms.put(type, terms);
			}
		}
		
		HashMap<String,HashSet<String>> featureTerms = new HashMap<String, HashSet<String>>();
		
		for (String label : classTerms.keySet()) {
			HashMap<String,Integer> terms = classTerms.get(label);
			featureTerms.put(label, new HashSet<String>());
			
			for (String term : terms.keySet()) {
				if (inAll.get(term) > 1 || terms.get(term) < 5) continue;
				HashSet<String> uniqueTerms = featureTerms.get(label);
				uniqueTerms.add(term);
				featureTerms.put(label, uniqueTerms);
			}
		}
		return featureTerms;
	}*/

    
    public void writeArcsToFile(String outputFile, boolean balanced, HashMap<String,HashMap<String,Arc>> arcs) {
    	System.err.println("[DataGenerator.writeArcsToFile] Writing to " + outputFile + "agreement.arcs" + " and " + outputFile + "agreement.txt");
		
		try {
			BufferedWriter arcWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile  + "agreement.arcs"),"UTF-8"));
			BufferedWriter sbjtextWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile + "agreement.sbj.txt"),"UTF-8"));
			BufferedWriter tartextWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile + "agreement.tar.txt"),"UTF-8"));
			
			for (String docId : arcs.keySet()) {
				
				HashMap<String,Arc> doc = arcs.get(docId);
				
				for (Arc arc : doc.values()) {
					if (arc == null) continue;
					arcWriter.write(docId + "," + arc.getType() + "," + arc.isAncestor() + "," +
							arc.getSubject().getId() + 	"," + arc.getSubject().getAuthor() + "," + arc.getSubject().getDepth() + "," + 
							arc.getTarget().getId() + "," + arc.getSubject().getAuthor() + "," + arc.getTarget().getDepth() + "," +
							arc.getSubject().getApproxSentencePosition() + "," + arc.getTarget().getApproxSentencePosition() + "," + arc.getTarget().getNumResponses() +
							"," + arc.getSubject().getNumResponses() + "\n");
					
					sbjtextWriter.write(arc.getSubject().getBody() + "\n");
					tartextWriter.write(arc.getTarget().getBody() + "\n");
				}
			}
			
			arcWriter.close();
			sbjtextWriter.close();
			tartextWriter.close();
			
		} catch (Exception e) {
			System.err.println("[SentimentResult.writeToFile] " + outputFile);
			e.printStackTrace();
		}
    }
    
    /**
     * 
     * Target must be before subject. Swap order if incorrect. True if mode is direct. False if indirect.
     * 
     * @param subject
     * @param target
     * @param e
     * @return
     */
    private boolean isAncestor(Object [] pair, blog.Entry e) {

    	
    	if (pair[0].equals(e)) {
    		return true;
    	}
    	if (pair[1].equals(e)) {
    		swap(pair);
    		return true;
    	}
    	
        blog.threaded.Comment p = (blog.threaded.Comment)pair[0];

        while(p != null) {
            if(p.getURL().equals(((Comment)pair[1]).getURL())) {
            	swap(pair);
                return true;
            }
            p = (blog.threaded.Comment)e.getComment(p.getParentURL());
        }
        
        p = (blog.threaded.Comment)pair[1];
        
        while(p != null) {
            if(p.getURL().equals(((Comment)pair[0]).getURL())) {
                return true;
            }
            p = (blog.threaded.Comment)e.getComment(p.getParentURL());
        }

        return false;
    }
    
    private boolean isParent(Object [] pair, blog.Entry e) {

    	
    	if (pair[0].equals(e)) {
    		return true;
    	}
    	if (pair[1].equals(e)) {
    		swap(pair);
    		return true;
    	}
    	
        Comment p = (Comment)pair[0];

       
        if(p.getURL().equals(((Comment)pair[1]).getURL())) {
            swap(pair);
            return true;
        }
        
        p = (Comment)pair[1];
        
        if(p.getURL().equals(((Comment)pair[0]).getURL())) {
                return true;
        }
        return false;
    }
    
    private void swap(Object [] pair) {
    	Object temp = pair[0];
    	pair[0] = pair[1];
    	pair[1] = temp;
    }

    /*private String arcId(String subjectId, String targetId) {
        return subjectId + "////" + targetId;
    }*/
    
    public static int depth(blog.threaded.Comment c, Entry e) {

        int sdepth = 0;
        blog.threaded.Comment p = c;
        while(p != null) {
            p = (blog.threaded.Comment)e.getComment(p.getParentURL());
            sdepth++;
        }

        return sdepth;
    }
    
    private String shortEntryId(String url) {
        String[] parts = url.split("[=#]");
        return parts[parts.length - 1];
    }
    
    /**
     * Return a subset of the sentences
     * 
     * @param tagger
     * @param sentences
     * @return
     */
    /*private String toSentences(List<List<HasWord>> tokenizedSentences, int start, int end) {
    	
        String sentences = "";
        
        int index = 0;
        
        for (List<HasWord> words : tokenizedSentences) {
        	
        	if (index < start) continue;
        	if (index >= end) break;
        	index++;
        	
	        for (HasWord word : words) {
	        	sentences += word.word() + " ";
	        }
        }
        return "\"" + sentences.replaceAll("\\n", "\",\"").trim() + "\"";
    }*/
    
    public static String preFormat(String s) {

    	s = s.replace("/", "*SLASH*");
		s = s.replaceAll("`", "'");
		s = s.replaceAll("`", "'");
		s = s.replaceAll("\"", "''");
        /*s = s.replace("...", " *DOTS* ");
        s = s.replace("?", " *QUERY* ");
        s = s.replace("!", " *BANG* ");
        s = s.replace("\"", " *QUOTE* ");
        s = s.replace("'", " *QUOTE* ");
        s = s.replace("\n", " *NEWLINE* ");
        s = s.replace(",", " *COMMA* ");
        s = s.replace(".", " *PERIOD*" );*/

        return s.trim();
    }
    
   /* public String addParsingandPhrases(String file, int start) {
    	
    	LexicalizedParser lexicalizedParser = LexicalizedParser.loadModel();
	    //TreebankLanguagePack tlp = new PennTreebankLanguagePack();
	    //GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();

		List<String> lines = Utils.readLines(file);
		int count = 0;
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file + ".parse",true));
			BufferedWriter phraseWriter = new BufferedWriter(new FileWriter(file + ".chk",true));
			
			for (String line : lines) {
				
				if (count < start) {
					count++;
					continue;
				}
				
				String [] sentences = (line.length() > 1 && line.startsWith("\"") && line.endsWith("\"")) ? line.substring(1,line.length()-1).split("\",\"") :
						line.split("\",\"");
				
				try {
					String output = "";
					
					for (String sentence : sentences) {
						//System.out.println(sentence.length());
						
						if (sentence.length() > 1000) {
							sentence = sentence.replaceAll("(\\.[\\s']+)","$1DELIM");
							String [] splitUP = sentence.split("DELIM");
							String o = "";
							for (String sub : splitUP) {
								if (sub.length() > 300) {
									String [] comma = sub.split(",");
									
									for (String c : comma) {
										Tree t = (Tree) lexicalizedParser.apply(c);
										printPhraseTree(t,phraseWriter);
										o += t + " ";
									}
								}
								else {
									Tree t = (Tree) lexicalizedParser.apply(sub);
									printPhraseTree(t,phraseWriter);
									o += t + " ";
								}
							}
							output += "\"" + o + "\",";
							phraseWriter.write(" ");
						}
						else {
							Tree tree = (Tree) lexicalizedParser.apply(sentence);
							printPhraseTree(tree,phraseWriter);
							output += "\"" + tree + "\",";
							phraseWriter.write(" ");
						}
					}
					writer.write(output.substring(0,output.length()-1) + "\n");
					phraseWriter.write("\n");
					count++;
				} catch (Exception p) {
					System.err.println("[DataGenerator.addParsing] error parsing: " + line);
					p.printStackTrace();
				}

			    	//System.out.println();
		    }
			writer.close();
			phraseWriter.close();
		} catch (Exception e) {
	    	e.printStackTrace();
	    }
		return file + ".chk";
    }*/
    
   /* public void printPhraseTree(Tree tree, BufferedWriter writer) throws Exception {
    	
    	if (tree.size() <= 15) {
    		
    		int i = 0;
    		
    		for (Tree leaf : tree.getChildrenAsList()) {
    			String tag = "I";
    			
    			if (i == 0) tag = "B"; 
    			
    			for (Label w : leaf.yield()) {
    				CoreLabel word = (CoreLabel)w;
    				if (word.word().matches("\\p{Punct}+")) tag = "O";
    				writer.write(word.word().replaceAll("/", "|") + "/" + leaf.value().replaceAll("/","|") + "/" + tag  + "-" + leaf.value().replaceAll("/","|") + " ");
    				tag = "I";
    				i++;
    			}
    		}
    	}
    	else {
    		for (Tree t : tree.getChildrenAsList()) {
    			printPhraseTree(t,writer);
    		}
    	}
    }
    
    public ArrayList<String> getPhrases(Tree tree) {
    	
    	ArrayList<String> phrases = new ArrayList<String>();
    	String phrase = "";
    	
    	if (tree.size() <= 15) {   		
    		for (Tree leaf : tree.getLeaves()) {
    			for (Word word : leaf.yieldWords()) {
    				phrase += word + " ";
    			}
    		}
    		phrases.add(phrase.trim());
    	}
    	else {
    		for (Tree t : tree.getChildrenAsList()) {
    			phrases.addAll(getPhrases(t));
    		}
    	}
    	return phrases;
    }*/
    
    public void addSentiment(String txtFile, String modelDirectory) {

		OpinionRunner _sentimentRunner = new OpinionRunner(modelDirectory,true,true,true,true,100,false);
		//ArrayList<SentimentResult> sentimentResult = _sentimentRunner.runSubjectivity(txtFile);
		
		TestDataProcessor processor = new SubjectivityDataProcessor(new OpinionClassification(),true,100,false);
		
		OpinionRunner.preprocess(txtFile, false);
		
    	AbstractDataReader reader = _sentimentRunner.getReader(txtFile + ".emo.phrase.emo", Classification.SUBJECTIVITY, true);
    	ArrayList<SentimentResult> results = _sentimentRunner.test(_sentimentRunner.getTrainingModel(true), processor, reader);
		
		try {
			SentimentResult.writePhrasesToFile(txtFile, results, true, .7);
		} catch (Exception e) {
			System.err.println("[DataGenerator.addSentiment] " + e);
			e.printStackTrace();
		}
    }

    public void addPolarity(String file, String modelDirectory) {
    
    	OpinionRunner _sentimentRunner = new OpinionRunner(modelDirectory,true,true,true,true,100,false);
		//ArrayList<SentimentResult> sentimentResult = _sentimentRunner.runPolarity(file);
    	
    	TestDataProcessor processor = new PolarityDataProcessor(new PolarityClassification(true),true,100, false);
		
		OpinionRunner.preprocess(file, false);
		
    	AbstractDataReader reader = _sentimentRunner.getReader(file + ".emo.phrase.emo", Classification.POLARITY, true);
    	ArrayList<SentimentResult> results = _sentimentRunner.test(_sentimentRunner.getTrainingModel(false), processor, reader);
		
		try {
			SentimentResult.writePhrasesToFile(file, results, false, .6);
			SentimentResult.writeSubjectivityAndPolarity(file + ".sbj", file + ".pol", file, .7);
		} catch (Exception e) {
			System.err.println("[DataGenerator.addPolarity] " + e);
			e.printStackTrace();
		}
    }
    
    public void computeSentenceSimilarity(String srcFile, String tarFile, String outputFile) {
    	List<String> src = Utils.readLines(srcFile);
    	List<String> tar = Utils.readLines(tarFile);

    	SentenceSimilarity ss = new SentenceSimilarity();
    	
    	try {
    	
	    	BufferedWriter ssWriter = new BufferedWriter(new FileWriter(outputFile));
	    	
	    	for (int index = 0; index < src.size(); index++) {
	    		double value = ss.runTwoSentence(src.get(index),tar.get(index));
	    		//System.out.println(value);
	    		ssWriter.write(value + "\n");
	    		
	    		if (index % 50 == 0) ssWriter.flush();
	    	}
	    	ssWriter.close();
    	} catch (Exception e) {
    		System.err.println(e);
    		e.printStackTrace();
    	}
    }
    
    /**
     * Take the chunk string (eg. --/--/B-- --/--/I--) and convert it to array of phrases
     * @param string
     * @return
     */
    private String splitChunks(String string) {
    	String chunks = "";
    	
    	String [] words = string.split("\\s+");
    	
    	String chunk = "";
    	
    	for (String word : words) {
    		
    		try {
	    		String [] parts = word.trim().split("/");
	    		
	    		String tag = parts[2].substring(0,1);
	    		
	    		word = parts[0];
	    		
	    		if (!chunk.equals("") && tag.startsWith("B")) {
	    			chunks += chunk + "\n";
	    			chunk = "";
	    		}
	    		
	    		chunk += word + " ";
    		} catch (Exception e) {
    			System.out.println(word);
    			e.printStackTrace();
    		}
    	}
    	
    	if (!chunk.equals("")) chunks += chunk + "\n";
    	
    	return chunks;
    }
    
    /**
     * Read in parse files, and run sentence similarity on phrases. Keep the highest values for each phrase only.
     * @param srcFile
     * @param tarFile
     * @param outputFile
     * 
     * ex: ["I think" "He thinks" .62]
     */
    public void computePhraseSimilarity(String srcFile, String tarFile, String outputFile) {
    	List<String> src = Utils.readLines(srcFile);
    	List<String> tar = Utils.readLines(tarFile);

    	SentenceSimilarity ss = new SentenceSimilarity();
    	
    	int size = new File(outputFile).exists() ? processing.GeneralUtils.readLines(outputFile).size() : 0;
    	
    	try {
	    	BufferedWriter ssWriter = new BufferedWriter(new FileWriter(outputFile, true));
	    	
	    	for (int index = size; index < src.size(); index++) {
	    		
	    		String phrasesSrc = splitChunks(src.get(index));
	    		String phrasesTar = splitChunks(tar.get(index));
	    		
	    		HashMap<String,Double> similarity = ss.run(phrasesSrc, phrasesTar);
	    		
	    		String [] chunksSrc = phrasesSrc.split("\n");
	    		String [] chunksTar = phrasesTar.split("\n");
	    		double max = -1;
	    		int s = -1;
	    		int t = -1;
	    		
	    		// do for the one that has less phrases only... (this should help with overlap)
	    		for (int sIndex = 0; sIndex < chunksSrc.length; sIndex++) {
	    			for (int tIndex = (chunksSrc.length + 1); tIndex < (chunksTar.length + chunksSrc.length); tIndex++) {
	    				double value = similarity.get(sIndex + "-" + tIndex);
	    				
	    				if (value > max) {
	    					max = value;
	    					s = sIndex;
	    					t = tIndex;
	    				}
	    				//System.out.println(value);
	    			}
	    			if (max == -1) continue;
	    			ssWriter.write(s + "-" + (t-s-1) + "-" + max + " ");
	    			s = -1; t = -1; max = -1;
	    		}
	    		ssWriter.write("\n");
	    		if (index % 50 == 0) ssWriter.flush();
	    	}
	    	ssWriter.close();
    	} catch (Exception e) {
    		System.err.println(e);
    		e.printStackTrace();
    	}
    }
    
    /**
     * Read in parse file, and output as phrase for ss
     * @param srcFile
     * @param tarFile
     * @param outputFile
     * 
     * ex: ["I think" "He thinks" .62]
     */
    public void computePhraseSimilarity(String srcFile, String outputFile) {
    	List<String> src = Utils.readLines(srcFile);

    	try {
        	//if (!new File(outputFile).exists()) {
		    	BufferedWriter ssWriter = new BufferedWriter(new FileWriter(outputFile, true));
		    	
		    	for (int index = 0; index < src.size(); index++) {
		    		
		    		String phrasesSrc = splitChunks(src.get(index));
		    		
		    		String [] chunksSrc = phrasesSrc.split("\n");
		    		
		    		for (String phrase : chunksSrc) {
		    			ssWriter.write(phrase + "\n");
		    		}
		    		ssWriter.write("\n");
		    	}
		    	ssWriter.close();
		    	OpinionRunner.postTagData(outputFile);
        	//}
        	//if (!new File(outputFile + ".emo.ss").exists()) {
        		computeSentenceSimilarity(outputFile + ".emo");
        	//}
    	} catch (Exception e) {
    		System.err.println(e);
    		e.printStackTrace();
    	}
    }
    
    public HashMap<String,Integer> hashId(List<String> ids) {
    	HashMap<String,Integer> idHash = new HashMap<String,Integer>();
    	
    	for (int index = 0; index < ids.size(); index++) {
			idHash.put(ids.get(index),index);
		}
    	return idHash;
    }
    
    public int getDocumentPositionById(String processingDirectory, String docId, 
    		String postId, int sentencePosition) {
    	try {
    		List<String> ids = processing.GeneralUtils.readLines(
    				processingDirectory + "/processed-" + docId + "/" + docId + ".ids");
    		
    		for (int index = 0; index < ids.size(); index++) {
    			if ((postId + "-" + sentencePosition).equals(ids.get(index))) {
    				return index;
    			}
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return -1;
    }
    
    public int getDocumentPositionById(List<String> ids,String postId, int sentencePosition) {
    	try {
    		
    		for (int index = 0; index < ids.size(); index++) {
    			if ((postId + "-" + sentencePosition).equals(ids.get(index))) {
    				return index;
    			}
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return -1;
    }
    
    
    public void computeSentenceSimilarity(String file) {
    	SentenceSimilarity ss = new SentenceSimilarity();
    	ss.run(file);
    }
    
    public String computePhraseSimilarity(String file1, int i1, String file2, int i2) {
    	List<String []> lines1 = getPhraseSimilarityFromFile(file1,i1);
    	List<String []> lines2 = getPhraseSimilarityFromFile(file2,i2);
    	return computePhraseSimilarity(lines1,lines2);
    }
    public String computePhraseSimilarity(List<String []> sentencePhrasesQ, List<String []> sentencePhrasesR) {
    	
    	String similarities = "";
    	
    	for (int response = 0; response < sentencePhrasesR.size(); response++) {
    		
    		double max = -1;
    		int quote = -1;
    		
    		for (int j = 0; j < sentencePhrasesQ.size(); j++) {
        		
    			double score = SentenceSimilarity.cosineSimilarity(sentencePhrasesQ.get(j), sentencePhrasesR.get(response));
    			if (score > max) {
    				max = score;
    				quote = j;
    			}
    		}
    		// only keep ones that have some meaningful similarity
    		if (max > .5) {
    			similarities += quote + "-" + response + "-" + max + " ";
    			//similarities += s + "-" + (t+lines1.size()+1) + "-" + max + " ";
    		}
    	}
    	return similarities.trim();
    }
    
    public List<String []> getPhraseSimilarityFromFile(String file, int index) {
    	List<String> lines = processing.GeneralUtils.readLines(file);
       	List<String> phrases = processing.GeneralUtils.readLines(file.substring(0,file.length()-3));
    	return getPhraseSimilarityFromFile(phrases, lines, index);
    }
    
    public static List<List<String []>> readPhraseSimilarityFromFile(String file) {
    	List<List<String []>> sentences = new ArrayList<List<String []>>();
    	try {
    		BufferedReader reader = new BufferedReader(new FileReader(file));
    		
    		List<String> phrases = null;
    		
    		if (new File(file.substring(0,file.length()-3)).exists()) {
    			phrases = processing.GeneralUtils.readLines(file.substring(0,file.length()-3));
    		}
    		else {
    			phrases = processing.GeneralUtils.readLines(file.substring(0,file.length()-7));
    		}
    		
    		List<String []> lines = new ArrayList<String []>();
    		reader.readLine();
    		
    		int i = 0;
    		
    		while (reader.ready()) {
    			String line = reader.readLine();
    			
    			if (phrases.get(i).isEmpty()) {
    				sentences.add(lines);
    				lines = new ArrayList<String []>();
    			}
    			else {
    				lines.add(line.split(" "));
    			}
    			i++;
    		}
    		reader.close();
    	} catch (IOException e) {
    		throw new RuntimeException("Cannot read from file " + file, e);
    	}
    	return sentences;	
    }
	
    
    public List<String []> getPhraseSimilarityFromFile(List<String> phrases, List<String> lines, int index) {	
    	int p = 0;
    	List<String []> vectors = new ArrayList<String []>();
    	
    	for (int i = 0; i < phrases.size() && p < index; i++) {
    		if (phrases.get(i).isEmpty()) p++;
    	}
    	
    	for (int i = p+1; i < phrases.size() && !lines.get(p).isEmpty(); i++) {
    		vectors.add(lines.get(p).split(" "));
    	}
    	return vectors;
    }
}
