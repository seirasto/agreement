package agreement.classifier;

import java.io.*;
import java.util.*;

import org.w3c.dom.Document;

import processing.Contractions;
import processing.EmoticonDictionary;
import processing.GeneralUtils;
import processing.SentenceSimilarity;
import weka.attributeSelection.ChiSquaredAttributeEval;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SMO;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.stemmers.SnowballStemmer;
import weka.core.stemmers.Stemmer;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.Filter;
import weka.filters.supervised.instance.Resample;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.StringToWordVector;
import agreement.Arc;
import agreement.Node;
import blog.XMLUtilities;
import edu.columbia.opinion.io.SentimentResult;
import edu.columbia.opinion.module.OpinionRunner;

public class WekaClassifier extends AgreementClassifier {

	StringToWordVector _ngramFiltSBJ;
    StringToWordVector _ngramFiltTAR;
    HashSet<String> _nGramsSBJ = null;
    HashSet<String> _nGramsTAR = null;

   // StringToWordVector _ngramFilt;
    //HashSet<String> _nGrams = null;

    
    StringToWordVector _ngramFiltSBJPOS;
    StringToWordVector _ngramFiltTARPOS;
    HashSet<String> _nGramsSBJPOS = null;
    HashSet<String> _nGramsTARPOS = null;
    
    StringToWordVector _ngramFiltSBJOP;
    StringToWordVector _ngramFiltTAROP;
    HashSet<String> _nGramsSBJOP = null;
    HashSet<String> _nGramsTAROP = null;
    
    StringToWordVector _ngramFiltSBJPLUS;
    StringToWordVector _ngramFiltTARPLUS;
    HashSet<String> _nGramsSBJPLUS = null;
    HashSet<String> _nGramsTARPLUS = null;
    StringToWordVector _ngramFiltSBJNEG;
    StringToWordVector _ngramFiltTARNEG;
    HashSet<String> _nGramsSBJNEG = null;
    HashSet<String> _nGramsTARNEG = null;
    
    StringToWordVector _ngramFiltPHRASEIntersection;
    HashSet<String> _nGramsPHRASEIntersection;
    StringToWordVector _ngramFiltPHRASEUnique;
    HashSet<String> _nGramsPHRASEUnique;
    

    private Classifier classifier;

    private FastVector nullVector, types, booleans;
    protected FastVector attVector;
    private Attribute subjectBody, targetBody, subjectBodyPOS, targetBodyPOS, subjectBodyOpinion, targetBodyOpinion,
    		subjectBodyNegative, targetBodyNegative, parentIsRoot, /*closestAncestorIsRoot,*/ sameDepth, isAncestor, isParent, sameAuthor,
            /*overlap,*/ type, /*docId,*/ hasSentimentSBJ, sentimentCountSBJ, hasSentimentTAR, sentimentCountTAR, 
            hasPositiveSBJ, positiveCountSBJ, hasPositiveTAR, positiveCountTAR, subjectBodyPositive, targetBodyPositive, 
            hasNegativeSBJ, negativeCountSBJ, hasNegativeTAR, negativeCountTAR, questionSBJ, questionTAR, sentenceSimilarity,
            phraseSimilarityTypes, phraseSimilarityCountSame, phraseSimilarityCountDiff, phraseSimilarityCountNA, /*phraseSimilarityBodyIntersection,*/ phraseSimilarityBodyUnique, sentenceLengthSBJ, sentenceLengthTAR,
            sentencePositionSubject, sentencePositionTarget, numberOfResponsesSubject, numberOfResponsesTarget;
    
    ArrayList<Integer> _featureRanges;
    ArrayList<String> _featureNames;
    
    
    public static void main(String[] args) throws Exception {

        boolean newClassifier = false;
        String processingDirectory = null;
        String  trainingDirectory = null;
        String testDirectory = null;
        boolean thread = false;
        boolean pos = false;
        boolean balanced = false;
        boolean opinion = false;
        boolean polarity = false;
        boolean question = false;
        boolean sentenceLength = false;
        boolean ngram = false;
        boolean sentenceSimilarity = false;
        boolean phraseSimilarity = false;
        String wekaClassifier = "nb";
       // boolean SCIL = false;
        boolean dev = true;
        boolean twoWay = false;
        boolean all = false;
        boolean post = false;
        String file = null;
        List<String> experiments = null;
        
        for (int i = 0; i < args.length; i++) {
        	
        	if (args[i].equals("-retrain")) {
        		newClassifier = true;
        	}
        	else if (args[i].equals("-train")) {
        		trainingDirectory = args[++i];
        	}
        	else if (args[i].equals("-test")) {
        		testDirectory = args[++i];
        		dev = false;
        	}
        	else if (args[i].equals("-file")) {
        		file = args[++i];
        	}
        	else if (args[i].equals("-dev")) {
        		testDirectory = args[++i];
        		dev = true;
        	}
        	else if (args[i].equals("-pos")) {
        		pos = true;
        	}
        	else if (args[i].equals("-balanced")) {
        		balanced = true;
        	}
        	else if (args[i].equals("-opinion")) {
        		opinion = true;
        	}
        	else if (args[i].equals("-polarity")) {
        		polarity = true;
        	}

        	else if (args[i].equals("-question")) {
        		question = true;
        	}
        	else if (args[i].equals("-sl")) {
        		sentenceLength = true;
        	}
        	else if (args[i].equals("-ngram")) {
        		ngram = true;
        	}
        	else if (args[i].equals("-ss")) {
        		sentenceSimilarity = true;
        	}
        	else if (args[i].equals("-phraseSS")) {
        		phraseSimilarity = true;
        	}
        	else if (args[i].equals("-thread")) {
        		thread = true;
        	}
        	else if (args[i].equals("-processing")) {
        		processingDirectory = args[++i];
        	}
        	//else if (args[i].equals("-all")) {
        	//	SCIL = false;
        	//}
        	else if (args[i].equals("-classifier")) {
        		wekaClassifier = args[++i];
        	}
        	else if (args[i].equals("-2way")) {
        		twoWay = true;
        	}
        	else if (args[i].equals("-experiments")) {
        		experiments = Arrays.asList(args[++i].split(","));
        	}
        	else if (args[i].equals("-all")) {
        		all = true;
        	}
        	else if (args[i].equals("-post")) {
        		post = true;
        	}

        }
        
        String dataDirectory = "";
        
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			dataDirectory = prop.getProperty("res");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
        WekaClassifier classifier = new WekaClassifier(dataDirectory, thread, pos, balanced, opinion, polarity, question, 
        		sentenceLength, ngram, sentenceSimilarity, phraseSimilarity, twoWay, false, false); 
        System.out.println("[AgreementClassifier] LOADING TRAINING");
        HashMap<String,HashMap<String,Arc>> trainArcs = classifier.load(newClassifier, trainingDirectory, "res/train/",  "res");
        System.out.println("[AgreementClassifier] LOADED ANNOTATIONS FROM " + trainArcs.size() + " DOCUMENTS.");
        
        System.out.println("[AgreementClassifier] FORMATTING");
        Instances trainInsts = classifier.buildInstances(hashhashToList(trainArcs),"train");
        
        // cross-validation
        if (testDirectory == null && file == null) {
            System.out.println("[AgreementClassifier] CROSSVALIDATION");
        	classifier.crossValidation(trainInsts, experiments, balanced, wekaClassifier, -1);
        }
        
        // run on set of test annotations
        if (testDirectory != null) {
            System.out.println("[AgreementClassifier] LOADING TESTING");
            HashMap<String,HashMap<String,Arc>> testArcs = classifier.load(newClassifier, testDirectory, "res/" + (dev ? "dev/" : "test/"), "res");
            
            System.out.println("[AgreementClassifier] LOADED ANNOTATIONS FROM " + testArcs.size() + " DOCUMENTS.");
            
            List<Arc> arcs = null;
            
            if (all) {
                System.out.println("[AgreementClassifier] LOADING ALL TEST ARCS.");
            	arcs = hashListToList(classifier.loadAll(testArcs, processingDirectory,"res", false));
            }
            else if (post) {
            	System.out.println("[AgreementClassifier] LOADING ALL POST ARCS.");
            	arcs = hashListToList(classifier.loadChain(newClassifier,(dev ? "dev/" : "test/"), testDirectory,"res", false));
            }
            else arcs = hashhashToList(testArcs);
            
            System.out.println("[AgreementClassifier] LOADED " + arcs.size() + " ARCS.");
            
            System.out.println("[AgreementClassifier] FORMATTING");
            Instances testInsts = classifier.buildInstances(arcs,"test");
            
        	System.out.println("[AgreementClassifier] SET OF ANNOTATIONS");
        	classifier.run(trainInsts, testInsts, experiments, balanced, wekaClassifier, -1);
        }
        
        // run on unannotated file
        if (file != null) {
        	classifier.run(processingDirectory, testDirectory, "res");
        }
    }

    public WekaClassifier(String dataDirectory, boolean thread, boolean usePOS, boolean balanced, boolean opinion, boolean polarity,
    		boolean question, boolean sentenceLength, boolean ngram, boolean sentenceSimilarity, boolean phraseSimilarity, boolean twoWay, boolean liwc, boolean sm) {
    	super(dataDirectory, thread, usePOS, balanced, opinion, polarity, question, sentenceLength, ngram, sentenceSimilarity, phraseSimilarity, twoWay, liwc, sm, false);
        getAttributes();
    }
    

    private static List<Arc> hashhashToList(HashMap<String,HashMap<String,Arc>> hashArc) {
    	List<Arc> arcs = new ArrayList<Arc>();
    	
    	for (HashMap<String,Arc> values : hashArc.values()) {
    		for (Arc arc : values.values()) {
    			arcs.add(arc);
    		}
    	}
    	return arcs;
    }
    
    private static List<Arc> hashListToList(HashMap<String,List<Arc>> hashArc) {
    	List<Arc> arcs = new ArrayList<Arc>();
    	
    	for (List<Arc> values : hashArc.values()) {
    		arcs.addAll(values);
    	}
    	return arcs;
    }
    
    protected void getAttributes() {

        nullVector = null;

        booleans = new FastVector();
        booleans.addElement("true");
        booleans.addElement("false");

        types = new FastVector();
        types.addElement("none");
        types.addElement("agreement");
        if (!_2way) types.addElement("disagreement");

        //docId = new Attribute("docId");

        if (_ngram) {
	        subjectBody = new Attribute("subjectBody", nullVector);
	        targetBody = new Attribute("targetBody", nullVector);
        }
        
        if (_POS) {
        	subjectBodyPOS = new Attribute("subjectBodyPOS", nullVector);
            targetBodyPOS = new Attribute("targetBodyPOS", nullVector);
        }
        
        if (_opinion) {
        	subjectBodyOpinion = new Attribute("subjectBodyOpinion", nullVector);
        	targetBodyOpinion = new Attribute("targetBodyOpinion", nullVector);
        	hasSentimentSBJ = new Attribute("-opN-hasSentiment_SBJ");
            sentimentCountSBJ = new Attribute("-opN-sentimentCount_SBJ");
        	hasSentimentTAR = new Attribute("-opN-hasSentiment_TAR");
            sentimentCountTAR = new Attribute("-opN-sentimentCount_TAR");
	    }
        
        if (_polarity) {
        	subjectBodyPositive = new Attribute("subjectBodyPositive", nullVector);
        	targetBodyPositive = new Attribute("targetBodyPositive", nullVector);
        	hasPositiveSBJ = new Attribute("-polN-hasPositive_SBJ");
        	positiveCountSBJ = new Attribute("-polN-positiveCount_SBJ");
        	hasPositiveTAR = new Attribute("-polN-hasPositive_TAR");
        	positiveCountTAR = new Attribute("-polN-positiveCount_TAR");
        	
        	subjectBodyNegative = new Attribute("subjectBodyNegative", nullVector);
        	targetBodyNegative = new Attribute("targetBodyNegative", nullVector);
        	hasNegativeSBJ = new Attribute("-polN-hasNegative_SBJ");
        	negativeCountSBJ = new Attribute("-polN-negativeCount_SBJ");
        	hasNegativeTAR = new Attribute("-polN-hasNegative_TAR");
        	negativeCountTAR = new Attribute("-polN-negativeCount_TAR");
	    }
        
        if (_sentenceSimilarity) {
        	sentenceSimilarity = new Attribute("-ss-sentenceSimilarity");
        }
        
        if (_phraseSimilarity) {
        	phraseSimilarityTypes = new Attribute("-PssN-phraseSimilarity");
        	phraseSimilarityCountSame = new Attribute("-PssN-phraseSimilarityCountSame");
        	phraseSimilarityCountDiff = new Attribute("-PssN-phraseSimilarityCountDiff");
        	phraseSimilarityCountNA = new Attribute("-PssN-phraseSimilarityCountNA");
        	//phraseSimilarityBodyIntersection = new Attribute("phraseSimilarityBodyIntersection", nullVector);
        	phraseSimilarityBodyUnique = new Attribute("phraseSimilarityBodyUnique", nullVector);
        }
        
        if (_thread) {
	        //overlap = new Attribute("-thread-overlap");
	        parentIsRoot = new Attribute("-thread-parentIsRoot");
	        //closestAncestorIsRoot = new Attribute("-thread-closestAncestorIsRoot");
	        sameDepth = new Attribute("-thread-sameDepth");
	        isAncestor = new Attribute("-thread-isAncestor");
	        isParent = new Attribute("-thread-isParent");
	        sameAuthor = new Attribute("-thread-isAuthor");
	        sentencePositionSubject = new Attribute("-thread-sentencePositionSubject");
	        sentencePositionTarget = new Attribute("-thread-sentencePositionTarget");
	        numberOfResponsesSubject = new Attribute("-thread-ResponsesToSubject");
	        numberOfResponsesTarget = new Attribute("-thread-ResponsesToTarget");
	    }
        
        if (_question) {   
	        questionSBJ = new Attribute("-?-questionSBJ");
	        questionTAR = new Attribute("-?-questionTAR");
        }
        
        if (_sentenceLength) {
        	sentenceLengthSBJ = new Attribute("-sl-sentenceLengthSBJ");
        	sentenceLengthTAR = new Attribute("-sl-sentenceLengthTAR");
        }
        
        type = new Attribute("type", types);

        attVector = new FastVector();
        
        attVector.addElement(type);
        
        if (_ngram) {
	        attVector.addElement(subjectBody);
	        attVector.addElement(targetBody);
	        _featureNames.add("-ngram");
        }
        
        if (_POS) {
	        attVector.addElement(subjectBodyPOS);
	        attVector.addElement(targetBodyPOS);
	        _featureNames.add("-pos");
        }
        
        if (_opinion) {
         	attVector.addElement(subjectBodyOpinion);
 	        attVector.addElement(targetBodyOpinion);
 	       _featureNames.add("-opW");
        }
        
        if (_polarity) {
         	attVector.addElement(subjectBodyPositive);
 	        attVector.addElement(targetBodyPositive);
         	attVector.addElement(subjectBodyNegative);
 	        attVector.addElement(targetBodyNegative);
 	       _featureNames.add("-polW");
        }
        
        if (_phraseSimilarity) {
        	//attVector.addElement(phraseSimilarityBodyIntersection);
        	attVector.addElement(phraseSimilarityBodyUnique);
        	_featureNames.add("-PssW");
        }

        if (_thread) {
	        attVector.addElement(parentIsRoot);
	        //attVector.addElement(closestAncestorIsRoot);
	        attVector.addElement(sameDepth);
	        attVector.addElement(isAncestor);
	        attVector.addElement(isParent);
	        attVector.addElement(sameAuthor);
	        attVector.addElement(sentencePositionSubject);
	        attVector.addElement(sentencePositionTarget);
	        attVector.addElement(numberOfResponsesSubject);
	        attVector.addElement(numberOfResponsesTarget);
	        //attVector.addElement(overlap);
	        _featureNames.add("-thread");
        }
        
        if (_sentenceSimilarity) {
        	attVector.addElement(sentenceSimilarity);
        	//_featureRanges.add(1);
        	_featureNames.add("-ss");
        }
        
        if (_phraseSimilarity) {
        	attVector.addElement(phraseSimilarityCountSame);
        	attVector.addElement(phraseSimilarityCountDiff);
        	attVector.addElement(phraseSimilarityCountNA);
        	attVector.addElement(phraseSimilarityTypes);
        	_featureNames.add("-PssN");
        }
        
        if (_opinion) {
            
	        attVector.addElement(hasSentimentSBJ);
	        attVector.addElement(sentimentCountSBJ);
	        attVector.addElement(hasSentimentTAR);
	        attVector.addElement(sentimentCountTAR);
	        
	        //_featureRanges.add(4);
	        _featureNames.add("-opN");
        }
        
        if (_polarity) {
            
	        attVector.addElement(hasPositiveSBJ);
	        attVector.addElement(positiveCountSBJ);
	        attVector.addElement(hasPositiveTAR);
	        attVector.addElement(positiveCountTAR);
	        
	        attVector.addElement(hasNegativeSBJ);
	        attVector.addElement(negativeCountSBJ);
	        attVector.addElement(hasNegativeTAR);
	        attVector.addElement(negativeCountTAR);
	        
	        //_featureRanges.add(4);
	        _featureNames.add("-polN");
        }

        
        if (_question) {
	        attVector.addElement(questionSBJ);
	        attVector.addElement(questionTAR);
	        //_featureRanges.add(2);
	        _featureNames.add("-?");
        }
         
        if (_sentenceLength) {
        	attVector.addElement(sentenceLengthSBJ);
        	attVector.addElement(sentenceLengthTAR);
        	_featureNames.add("-sl");
        }
    }
    
    public void run(String processingDirectory, String testFile, String modelDirectory) {
    	String [] files = null;
    	
    	if (new File(testFile).isDirectory()) {
        	System.out.println("[AgreementClassifier] LOADING TEST DIRECTORY " + testFile);
    		files = new File(testFile).list();
    	}
    	else {
        	System.out.println("[AgreementClassifier] LOADING TEST FILE " + testFile);
    		files = new String [1];
    		files[0] = testFile;
    	}
    	
    	new File(processingDirectory + "/with_agreement/").mkdir();
    	
    	for (String file : files) {
    		
    		String id = new File(file).getName();
    		
    		try {
            	System.out.println("[AgreementClassifier] LOADING FILE " + id);
            	
	        	List<Arc> arcs = loadTest(testFile + "/" + file, processingDirectory,modelDirectory,false);
	        	//List<Arc> outputArcs = null;
	        	
	        	HashMap<String,Arc> testArcs = new HashMap<String,Arc>();
	        	
	        	for (Arc a : arcs) {
	        		testArcs.put(id + ";" + a.getSubject().getId() + ";" + a.getTarget().getId() + ";" + a.getSubject().getBody() + ";" + a.getTarget().getBody(),a);
	        	}
	        	
	            System.out.println("[AgreementClassifier] CLASSIFYING");
	            //outputArcs = 
	            		classify(testArcs,buildInstances(new ArrayList<Arc>(testArcs.values()),"test"));
	        	
    		} catch (Exception e) {
    			System.err.println("[AgreementClassifier] error processing " + file);
    			e.printStackTrace();
    		}
    	}
    }

    /**
     * Process the training files to generate the sentence pairs and tag them with
     * pos, opinion, sentence similarity, etc...
     * @param trainingFiles
     * @param res
     * @return
     */
    protected List<String> process(boolean retrain, String trainingDirectory, String outputDirectory, String modelDirectory) {
    	DataGenerator dataGenerator = new DataGenerator(_dataDirectory ,_livejournal, _wikipedia);

    	boolean balanced = true;
    	int numArcs = 0;
    	
    	if (retrain || !new File(outputDirectory + "agreement.sbj.txt").exists() || 
				!new File(outputDirectory + "agreement.tar.txt").exists() || 
				!new File(outputDirectory + "agreement.arcs").exists()) {
		
			HashMap<String,HashMap<String,Arc>> arcs = dataGenerator.generate(balanced, trainingDirectory);
			//arcs = dataGenerator.addDocumentFeatures(arcs);
			dataGenerator.writeArcsToFile(outputDirectory, balanced, arcs);
			numArcs = arcs.size();
		}
    	else {
    		numArcs = processing.GeneralUtils.readLines(outputDirectory + "agreement.arcs").size();
    	}

    	if (this._sentenceSimilarity) {
			if (retrain || !new File(outputDirectory + "agreement.sbj.txt.ss").exists()) {
				dataGenerator.computeSentenceSimilarity(outputDirectory + "agreement.sbj.txt");
			}
			if (retrain || !new File(outputDirectory + "agreement.tar.txt.ss").exists()) {
				dataGenerator.computeSentenceSimilarity(outputDirectory + "agreement.tar.txt");
			}
    	}
		
		if (this._opinion) {
			if (retrain || !new File(outputDirectory + "agreement.sbj.txt.emo.con.chk.emo.sbj").exists())
				dataGenerator.addSentiment(outputDirectory + "agreement.sbj.txt",modelDirectory);
			if (retrain || !new File(outputDirectory + "agreement.tar.txt.emo.con.chk.emo.sbj").exists())
				dataGenerator.addSentiment(outputDirectory + "agreement.tar.txt",modelDirectory);
		}
		
		if (this._polarity) {
			if (retrain || !new File(outputDirectory + "agreement.sbj.txt.emo.con.chk.emo.op").exists())
				dataGenerator.addPolarity(outputDirectory + "agreement.sbj.txt",modelDirectory);
			if (!new File(outputDirectory + "agreement.tar.txt.emo.con.chk.emo.op").exists())
				dataGenerator.addPolarity(outputDirectory + "agreement.tar.txt",modelDirectory);
		}
		
		if (this._phraseSimilarity) {
		
			int sizeSBJ = new File(outputDirectory + "agreement.sbj.txt.parse").exists() ? 
					processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.parse").size() : 0;
			int sizeTAR = new File(outputDirectory + "agreement.tar.txt.parse").exists() ? 
							processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.parse").size() : 0;
			
			// add parsing
			if (retrain || !new File(outputDirectory + "agreement.sbj.txt.parse").exists() || 
					sizeSBJ < numArcs)
				OpinionRunner.addParsingandPhrases(outputDirectory +  "agreement.sbj.txt",sizeSBJ);
			if (retrain || !new File(outputDirectory + "agreement.tar.txt.parse").exists() || 
					sizeTAR < numArcs)
				OpinionRunner.addParsingandPhrases(outputDirectory + "agreement.tar.txt",sizeTAR);
			
			// phrase similarity
			if (this._phraseSimilarity && (retrain || !new File(outputDirectory + "agreement.txt.phrase.ss").exists() || 
					processing.GeneralUtils.readLines(outputDirectory + "agreement.txt.phrase.ss").size() < numArcs)) {
				dataGenerator.computePhraseSimilarity(outputDirectory + "agreement.sbj.txt.emo.con.chk",
						outputDirectory + "agreement.tar.txt.emo.con.chk",
						outputDirectory + "agreement.txt.phrase.ss");
			}
		}
		List<String> arcs = processing.GeneralUtils.readLines(outputDirectory + "agreement.arcs");	
		
		return arcs;
    }
    
    /**
     * Read all the arc data for training purposes
     * @param res
     * @param arcs
     * @return
     * @throws Exception
     */
    protected HashMap<String,HashMap<String,Arc>> populate(String outputDirectory, List<String> arcs) throws Exception {

    	HashMap<String,HashMap<String,Arc>> trainArcs = new HashMap<String,HashMap<String,Arc>>();
		List<String> phraseSimilarity = null;
		List<String> subjects = null;
		List<String> targets = null;
		// List<String> subjectsParse = null;
		// List<String> targetsParse = null;
		List<String> subjectsOpinion = null;
		List<String> targetsOpinion = null;
		List<String> subjectsPolarity = null;
		List<String> targetsPolarity = null;
   		List<String> subjectsPOS = null;
		List<String> targetsPOS = null;
   		List<String> subjectsSS = null;
		List<String> targetsSS = null;
		
		// boolean balanced = true;
		
	 	subjects = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt");
 		targets = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt");
		//subjectsParse = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.parse");
		//targetsParse = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.parse");
				
		if (_opinion || _phraseSimilarity) {
			subjectsOpinion = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.emo.con.chk.emo.sbj");
    		targetsOpinion = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.emo.con.chk.emo.sbj");
		}
		
		if (_polarity || _phraseSimilarity) {
			subjectsPolarity = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.emo.con.chk.emo.op");
    		targetsPolarity = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.emo.con.chk.emo.op");
		}
		
		
		if (_POS) {
			subjectsPOS = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.emo.con.pos");
    		targetsPOS = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.emo.con.pos");				
		}
		
		if (_sentenceSimilarity) {
			subjectsSS = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.ss");
			targetsSS = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.ss");
		}
		
		if (_phraseSimilarity) {
			phraseSimilarity = processing.GeneralUtils.readLines(outputDirectory + "agreement.txt.phrase.ss");
		}
		
		for (int index = 0; index < arcs.size(); index++) {
			String [] arcData = arcs.get(index).split(",");
			
			Node s = new Node(arcData[3],subjects.get(index),arcData[4],Integer.valueOf(arcData[5]),Integer.valueOf(arcData[9]),Integer.valueOf(arcData[12]),-1);
			Node t = new Node(arcData[6],targets.get(index),arcData[7],Integer.valueOf(arcData[8]),Integer.valueOf(arcData[10]),Integer.valueOf(arcData[11]),-1);
			
			//a.addParse(targetsParse.get(index),subjectsParse.get(index));
			
			if (_opinion || _phraseSimilarity) {
				s.addSingleOpinion(SentimentResult.ProcessLabeledSentence(subjectsOpinion.get(index)));
				t.addSingleOpinion(SentimentResult.ProcessLabeledSentence(targetsOpinion.get(index)));
			}
			if (_polarity | _phraseSimilarity) {
				s.addSinglePolarity(SentimentResult.ProcessLabeledSentence(subjectsPolarity.get(index)));
				t.addSinglePolarity(SentimentResult.ProcessLabeledSentence(targetsPolarity.get(index)));
			}
			if (_POS) {
				s.addSinglePOS(subjectsPOS.get(index));
				t.addSinglePOS(targetsPOS.get(index));
			}
			
			// String subjectId, String targetId, String subjectBody, String targetBody, String sbjAuthor, 
			// String tarAuthor, int depthSbj, int depthTar, boolean isParent, int type, int docId
			Arc a = new Arc(s, t, arcData[2].equals("true") ? true : false,Integer.valueOf(arcData[1]));
			
			if (_sentenceSimilarity) {
				a.addSingleSentenceSimilarity(
						SentenceSimilarity.cosineSimilarity(subjectsSS.get(index+1).split(" "), targetsSS.get(index+1).split(" ")));
			}
			if (_phraseSimilarity) {
				a.addSinglePhraseSimilarityFromString(phraseSimilarity.get(index));
			}						
			
			String docId = arcData[0];
			
			HashMap<String,Arc> docArcs = trainArcs.containsKey(docId) ? trainArcs.get(docId) : new HashMap<String,Arc>();
			docArcs.put(a.getKey(),a);
			trainArcs.put(docId,docArcs);
		}
		return trainArcs;
    }
    
    private HashMap<String,HashMap<String,Arc>> splitSentences(HashMap<String,HashMap<String,Arc>> arcs) throws Exception {
    	
    	List<Arc> toAdd = new ArrayList<Arc>();
    	List<Arc> toRemove = new ArrayList<Arc>();
    	
    	for (String docId : arcs.keySet()) {
    		
    		HashMap<String,Arc> doc = arcs.get(docId);
    		
    		for (Arc a : doc.values()) {
    			String [] subjectText = DataGenerator.normalizeSentence(a.getSubject().getBody()).split("DELIM");
    			String [] targetText = DataGenerator.normalizeSentence(a.getTarget().getBody()).split("DELIM");
    			
    			if (subjectText.length == 1 && targetText.length == 1) continue;
    			
    			Node subject = a.getSubject();
    			Node target = a.getTarget();
				
    			for (int i = 0; i < subjectText.length; i++) {
    				
 					Node s = new Node(subject.getId(),subject.getBody(),subject.getAuthor(),subject.getDepth(),
 							updateSentencePosition(i, subjectText.length-1, subject.getApproxSentencePosition()),subject.getNumResponses(),i);
    				
    				for (int j = 0; j < targetText.length; j++) {
    					
    					Node t = new Node(target.getId(),target.getBody(),target.getAuthor(),target.getDepth(),
     							updateSentencePosition(i, targetText.length-1, target.getApproxSentencePosition()),target.getNumResponses(),j);
    					
    					Arc n = new Arc(s,t,a.isAncestor(),a.getType());
    					toAdd.add(n);
    				}
    			}
    			toRemove.add(a);
    		}
    		
    		for (Arc n : toAdd) {
        		arcs.get(docId).put(n.getKey(), n);
        	}
        	for (Arc n : toRemove) {
        		arcs.get(docId).remove(n.getKey());
        	}
    	}
    	
    	return arcs;
    }
    
    private int updateSentencePosition(int position, int length, int current) {
    	if (position == 0 && current != 2) return current;
    	else if (position == length && current != 0) return current;
    	return 1;
    }
    
    
    //@SuppressWarnings("unchecked")
    public HashMap<String,HashMap<String,Arc>> load(boolean retrain, String trainingDirectory, String outputDirectory, String modelDirectory, boolean splitSentences) throws Exception {
    	
    	HashMap<String,HashMap<String,Arc>> trainArcs = new HashMap<String,HashMap<String,Arc>>();
    	
    	//String res = "/res/train/";
    	
    	// process the training files and populate 
    	// the data into the arc data structure to be used for training
    	//if (retrain) {	
    			
    		List<String> arcs = process(retrain, trainingDirectory, outputDirectory, modelDirectory);
    		trainArcs = populate(outputDirectory, arcs);
    	//}
    	
    	/*else {
	     	ObjectInputStream istream = new ObjectInputStream(
	    	                new FileInputStream(_dataDirectory + "/" + RES_INSTANCES));
	    	     List<Arc> arcs = (List<Arc>)istream.readObject();
	    	     istream.close();
	    	     
	    	     for (Arc a : arcs) {
	    	    	 HashMap<String,Arc> docArcs = trainArcs.containsKey(a.getDocId()) ? trainArcs.get(a.getDocId()) : new HashMap<String,Arc>();
	    	    	 docArcs.put(a.getSubjectId() + ";" + a.getTargetId() + ";" + a.getSubjectBody() + ";" + a.getTargetBody(),a);
	    	    	 trainArcs.put(a.getDocId(), docArcs);
	    	     }
	    }*/
    	if (splitSentences) trainArcs = splitSentences(trainArcs);
		return trainArcs;
    }
    
    public HashMap<String,HashMap<String,Arc>> load(boolean retrain, String trainingDirectory, String outputDirectory, String modelDirectory) throws Exception {
    	return load(retrain, trainingDirectory, outputDirectory, modelDirectory, false);
    }
    
    protected HashMap<String,List<Arc>> loadAll(HashMap<String,HashMap<String,Arc>> arcs, String processingDirectory, String modelDirectory, boolean differentiatePosts) throws Exception {
    	HashMap<String,List<Arc>> allArcs = new HashMap<String,List<Arc>>();
    	DataGenerator dataGenerator = new DataGenerator(_dataDirectory,_livejournal,_wikipedia);
    	
    	for (String docId : arcs.keySet()) {
    		String fullProcessingPath = processingDirectory + "/processed/processed-" + new File(docId).getName() + "/" + new File(docId).getName();
    		System.err.println("LOADING ALL ARCS FROM: " + docId);
    		
    		processFile(dataGenerator,docId, _dataDirectory + "/res/", modelDirectory, true, true, true, true);
    		
    		// generate whether we use or not.
    		List<String> ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
    		List<String> opinion = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.sbj");
    		List<String> polarity = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op");
    		List<String> pos = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.con.pos");	
    		List<String>ss = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss");
    		List<List<String []>> phraseSS = DataGenerator.readPhraseSimilarityFromFile(fullProcessingPath + ".txt.phrase.ss");

        	arcs = splitSentences(arcs);
    		List<Arc> newArcs = dataGenerator.generateAllSentenceArcs(
    				fullProcessingPath + ".sentences", arcs != null ? arcs.get(docId) : null, ids, opinion, polarity, pos, ss, phraseSS, differentiatePosts, processingDirectory);
    		printStats(newArcs,arcs.get(docId));
    		allArcs.put(docId,newArcs);
    		//break;
    	}
    	return allArcs;
    }
    
    protected void printStats(List<Arc> arcs, HashMap<String,Arc> annotated) {
    	int agree = 0;
    	for (Arc a : arcs) {
    		if (a.getType() == Arc.AGREEMENT || a.getType() == Arc.DISAGREEMENT) {
    			agree++;
    	   		//System.out.println(a);
    		}
    	}
    	
    	//System.out.println("\n----\n");
    	
    	int agree_a = 0;
    	
    	for (Arc a : annotated.values()) {
    		if (a.getType() == Arc.AGREEMENT || a.getType() == Arc.DISAGREEMENT) {
    			agree_a++;
        		//System.out.println(a);
    		}
    	}
    	
    	System.out.println(agree + " arcs in list. " + agree_a + " in annotations. " + arcs.size() + " arcs in total.");
    }
    
    protected List<Arc> loadTest(String testFile, String processingDirectory, String modelDirectory, boolean differentiatePosts) {
    	DataGenerator dataGenerator = new DataGenerator(_dataDirectory,_livejournal,_wikipedia);
    	String fullProcessingPath = processingDirectory + "/processed/processed-" + new File(testFile).getName() + "/" + new File(testFile).getName();
    	
		processFile(dataGenerator,testFile,processingDirectory, modelDirectory, true, true, true, true);
		
		// generate whether we use or not.
		List<String> ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
		List<String> opinion = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.sbj");
		List<String> polarity = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op");
		List<String> pos = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.con.pos");	
		List<String>ss = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss");
		List<List<String []>> phraseSS = DataGenerator.readPhraseSimilarityFromFile(fullProcessingPath + ".txt.phrase.ss");

				
    	// 2. generate arcs and as going along add in preprocessed items that are wanted
    	List<Arc> testArcs = dataGenerator.generateAllSentenceArcs(fullProcessingPath + ".sentences", null, ids, opinion, polarity, pos, ss, phraseSS, differentiatePosts, _dataDirectory);
    	
    	// 3. return arcs
    	return testArcs;
    }
    
    public static void processFile(DataGenerator dataGenerator, String file, String processingDirectory, String modelDirectory,
    		boolean subjectivity, boolean polarity, boolean ss, boolean phraseSS) {
    	
    	
    	String fullProcessingPath = processingDirectory + "/processed/processed-" + new File(file).getName() + "/" + new File(file).getName();
    	
    	new File(processingDirectory + "/processed/processed-" + new File(file).getName() + "/").mkdirs();
    	
    	if (!new File(fullProcessingPath + ".sentences").exists()) {
	    	try {
	    		XMLUtilities.addSentenceTagsToBlogs(file,fullProcessingPath + ".sentences",new EmoticonDictionary(),new Contractions(),DataGenerator.SENT_DELIMITERS);
	    	} catch (Exception e) {
	    		System.err.println("[AgreementClassifier] " + e);
	    		e.printStackTrace();
	    	}
    	}
    	if (!new File(fullProcessingPath + ".txt").exists()) {
			Document entry = XMLUtilities.getDocument(fullProcessingPath + ".sentences");
			ArrayList<String> sentences = XMLUtilities.getSentences(entry);
			XMLUtilities.writeSentencesToTxtFile(fullProcessingPath,sentences);
    	}
    	if (!new File(fullProcessingPath + ".ids").exists()) {
    		Document entry = XMLUtilities.getDocument(fullProcessingPath + ".sentences");
    		ArrayList<String> ids = XMLUtilities.getIDs(entry);
			XMLUtilities.writeSentencesToTxtFile(fullProcessingPath,".ids",ids);
    	}
    	
    	List<String> ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
		// skip... file too long
		if (ids.size() > 500) return; 
    	
    	// 1. preprocess file for pos, sentiment etc...
    	if (subjectivity && !new File(fullProcessingPath + ".txt.sbj").exists()) {
    		dataGenerator.addSentiment(fullProcessingPath + ".txt", modelDirectory);
		}
		if (polarity && !new File(fullProcessingPath + ".txt.op").exists()) {
			dataGenerator.addPolarity(fullProcessingPath + ".txt", modelDirectory);
		}
		// 2. add sentence similarity
		if (ss && !new File(fullProcessingPath + ".txt.ss").exists()) {
			dataGenerator.computeSentenceSimilarity(fullProcessingPath + ".txt");
		}
		// 3. add phrase ss
		if (phraseSS && !new File(fullProcessingPath + ".txt.phrase.emo.ss").exists()) { // || !new File(fullProcessingPath + ".txt.phrase.emo").exists()) {
			//OpinionRunner.preprocess(fullProcessingPath + ".txt",false);
			dataGenerator.computePhraseSimilarity(fullProcessingPath + ".txt.emo.phrase",fullProcessingPath + ".txt.phrase");
		}
    }

	/**
	 * Lets perform our own cross-validation on the unfiltered data
	 * @return
	 * @throws Exception
	 */
	public void crossValidation(Instances train, List<String> experiments, boolean balanced, String classifierName, double ridge) throws Exception {
		System.out.println("[SentimentRunner.CrossValidation] Starting crossvalidation at " + new Date());
		
		Hashtable<String,Evaluation> results = new Hashtable<String,Evaluation>();
		//Evaluation [] values = new Evaluation[10];
		
		//for (int run = 0; run < 10; run++) {
			Random rand = new Random(1);   // create seeded number generator
			Instances randData = new Instances(train);   // create copy of original data
			
			randData.randomize(rand);     
			randData.stratify(10);
			
			//Evaluation eval = null;
			
			for (int n = 0; n < 10; n++) {
				System.out.println("FOLD " + n);
				Instances trainCV = randData.trainCV(10, n);
				Instances testCV = randData.testCV(10, n);
				
				trainCV.setClassIndex(0);
				
				// if we want balanced training, balance the trainCV here
				if (balanced) trainCV = balance(trainCV);

				trainCV = createFilters(trainCV);
				testCV = this.createTestFilters(testCV);
				
				System.out.println(Arrays.toString(trainCV.attributeStats(trainCV.classIndex()).nominalCounts));
				System.out.println(Arrays.toString(testCV.attributeStats(testCV.classIndex()).nominalCounts));
				
				//if (eval == null) eval = new Evaluation(trainCV);
				
				Classifier classifier = null;
				
				if (classifierName.equals("logistic")) {
					classifier = new Logistic();
					if (ridge != -1) ((Logistic)classifier).setRidge(ridge);
				}
				else if (classifierName.equals("svm")) {
					classifier = new SMO();
				}
				else if (classifierName.equals("nb")) {
					classifier = new NaiveBayesMultinomial();
				}

				if (experiments != null) this.runList(experiments, results, classifier,trainCV,testCV);
				else this.runAll(results, classifier,trainCV,testCV);
				//classifier.buildClassifier(trainCV);
				//eval.evaluateModel(classifier, testCV);
			}
			
			printResults(results);
			/*System.out.println();
		    System.out.println("=== Setup run " + (run+1) + " ===");
		    System.out.println("Dataset: " + train.relationName());
		    System.out.println();
		    System.out.println(eval.toSummaryString("=== " + 10 + "-fold Cross-validation run " + (run+1) + "===", false));
			values[run] = eval;*/
		//}
		
		//Evaluation [] values = runner.PerformCrossValidation(new Logistic(),train, 10,10);
		System.out.println("[SentimentRunner.CrossValidation] Ending crossvalidation at " + new Date());
		//return values;
	}
	
	public void run(Instances train, Instances test, List<String> experiments, boolean balanced, String classifierName, double ridge) throws Exception {
		System.out.println("[SentimentRunner.run] Starting test at " + new Date());
		
		Hashtable<String,Evaluation> results = new Hashtable<String,Evaluation>();
		
		train.setClassIndex(0);
				
		// if we want balanced training, balance the train here
		if (balanced) train = balance(train);

		train = createFilters(train);
		test = this.createTestFilters(test);
		test.setClassIndex(0);		
		
		System.out.println(Arrays.toString(train.attributeStats(train.classIndex()).nominalCounts));
		System.out.println(Arrays.toString(test.attributeStats(test.classIndex()).nominalCounts));
				
		Classifier classifier = null;
		
		if (classifierName.equals("logistic")) {
			classifier = new Logistic();
			if (ridge != -1) ((Logistic)classifier).setRidge(ridge);
		}
		else if (classifierName.equals("svm")) {
			classifier = new SMO();
		}
		else if (classifierName.equals("nb")) {
			classifier = new NaiveBayesMultinomial();
		}

		if (experiments != null) this.runList(experiments, results, classifier,train,test);
		else this.runAll(results, classifier,train,test);

		printResults(results);
		System.out.println("[SentimentRunner.run] Ending test at " + new Date());
	}
	
	private void printResults(Hashtable<String,Evaluation> results) {
		if (_2way) System.out.println("Experiment F-measure None Agreement/Disagreement");
		else System.out.println("Experiment F-measure A/D_Accuracy A/D_F-measure None Agreement Disagreement");
			
		// -opW,-opN,-ngram,-pos,-sl,-question,-polW,-polN,-thread,-ss,-PssW,-PssN,

		for (String experimentName : results.keySet()) {
			System.out.println(experimentName + " " + (results.get(experimentName).correct()/results.get(experimentName).numInstances()) + " "
					+ (!_2way ? ((results.get(experimentName).numTruePositives(1) + results.get(experimentName).numTruePositives(2))/
							(results.get(experimentName).numInstances() - 
									(results.get(experimentName).numTruePositives(0) + (results.get(experimentName).numFalseNegatives(0))))) + " " : "")
					+ (!_2way ? fMeasureAD(results.get(experimentName)) : "")					
					+ " " + results.get(experimentName).fMeasure(0) + " " + 
					results.get(experimentName).fMeasure(1) + (!_2way ? " " + results.get(experimentName).fMeasure(2) : ""));
		}

	}
	
	private double fMeasureAD(Evaluation results) {
		
		double precision = (results.numTruePositives(1) + results.numTruePositives(2))/
				(results.numTruePositives(1) + results.numTruePositives(2) + results.numFalsePositives(1) + results.numFalsePositives(2));
		double recall = (results.numTruePositives(1) + results.numTruePositives(2))/
				(results.numTruePositives(1) + results.numTruePositives(2) + results.numFalseNegatives(1) + results.numFalseNegatives(2));
		
		return 2 * (precision * recall)/(precision + recall);
	}
	
	private Instances balance(Instances instances) throws Exception {
		
		Resample balance = new Resample();
		balance.setInputFormat(instances);
		int min = instances.attributeStats(instances.classIndex()).nominalCounts[0];
		
		for (int index = 1; index < instances.numClasses(); index++) {
			if (instances.attributeStats(instances.classIndex()).nominalCounts[index] < min) 
				min = instances.attributeStats(instances.classIndex()).nominalCounts[index];
		}

		double percent = min/(double)instances.numInstances();
		balance.setSampleSizePercent(Math.ceil(percent*100*instances.numClasses()));

		balance.setNoReplacement(true);
		balance.setBiasToUniformClass(1);
		return Resample.useFilter(instances, balance);
	
	}
    
	/**
	 * load data into weka instances
	 * 
	 * @param arcs
	 * @return
	 * @throws Exception
	 */
    public Instances buildInstances(List<Arc> arcs, String name) throws Exception {
    	
        Instances instances = new Instances(name, attVector, 0);

        int agreement = 0; int disagreement = 0; int none = 0;
        
        for(Arc a : arcs) {
        	if (a == null) {
        		//instances.add(new Instance(attVector.size()));
        		continue;
        	}
        	
        	if (a.getType() == Arc.AGREEMENT) agreement++;
        	else if (a.getType() == Arc.DISAGREEMENT) disagreement++;
        	else if (a.getType() == Arc.NONE) none++;
        	
        	instances.add(arcToInstance(a, instances));
        }
        
        System.out.println("[AgreementClassifier] == " + name  + " == none: " + none + ", agreeement: " + agreement + ", disagreement: " + disagreement);
        
        ArffSaver saver = new ArffSaver();
        saver.setInstances(instances);
        saver.setFile(new File("features-" + name + ".arff"));
        saver.writeBatch();
        
        instances.setClassIndex(0);
        return instances;
    }
    
    @SuppressWarnings("unchecked")
	public Instances createFilters(Instances trainInsts) throws Exception {
        Stemmer stem = new SnowballStemmer();

        StringToWordVector ngramFilt = new StringToWordVector();
        ngramFilt.setStemmer(stem);
        ngramFilt.setLowerCaseTokens(true);
        ngramFilt.setWordsToKeep(1000);
        ngramFilt.setOutputWordCounts(true);
        ngramFilt.setUseStoplist(true);
      
        List<Object> modified = null;
        
        //int size = trainInsts.numAttributes();
        
        if (_ngram) {
	        /*modified = this.applyFilter(trainInsts, ngramFilt, _ngramFiltSBJ, "-ngram-SBJ_GEN", 1,3);
	        trainInsts = (Instances)modified.get(0); _ngramFiltSBJ = (StringToWordVector)modified.get(1); _nGramsSBJ = (HashSet<String>)modified.get(2);
	        modified = this.applyFilter(trainInsts, ngramFilt, _ngramFiltTAR, "-ngram-TAR_GEN", 1,3);
	        trainInsts = (Instances)modified.get(0); _ngramFiltTAR = (StringToWordVector)modified.get(1); _nGramsTAR = (HashSet<String>)modified.get(2);    */ 

        	modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltSBJ, new int[] {1}, "-ngram-GEN_SBJ", 1, 3);
        	trainInsts = (Instances)modified.get(0); _ngramFiltSBJ = (StringToWordVector)modified.get(1);
	        modified = this.applyFilter(trainInsts, "-ngram-GEN_SBJ");
	        trainInsts = (Instances)modified.get(0); _nGramsSBJ = (HashSet<String>)modified.get(1);
        	modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltTAR, new int[] {1}, "-ngram-GEN_TAR", 1, 3);
        	trainInsts = (Instances)modified.get(0); _ngramFiltTAR = (StringToWordVector)modified.get(1);
	        modified = this.applyFilter(trainInsts, "-ngram-GEN_TAR");
	        trainInsts = (Instances)modified.get(0); _nGramsTAR = (HashSet<String>)modified.get(1);

        }
        
        if (_POS) {
        	modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltSBJPOS, new int[] {1}, "-pos-SBJ_POS", 1,1);
            trainInsts = (Instances)modified.get(0); _ngramFiltSBJPOS = (StringToWordVector)modified.get(1);
            modified = this.applyFilter(trainInsts, "-pos-SBJ_POS");
            trainInsts = (Instances)modified.get(0); _nGramsSBJPOS = (HashSet<String>)modified.get(1); 
            modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltTARPOS, new int[] {1}, "-pos-TAR_POS", 1,1); 
            trainInsts = (Instances)modified.get(0); _ngramFiltTARPOS = (StringToWordVector)modified.get(1);
            modified = this.applyFilter(trainInsts, "-pos-TAR_POS");
            trainInsts = (Instances)modified.get(0); _nGramsTARPOS = (HashSet<String>)modified.get(1); 
	        //_featureRanges.add(trainInsts.numAttributes()-size+2);
	        //_featureNames.add("-pos");
	        //size = trainInsts.numAttributes();
        }
        
        if (_opinion) {
        	
        	modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltSBJOP, new int[] {1}, "-opW-SBJ_OP", 1,1);
        	trainInsts = (Instances)modified.get(0); _ngramFiltSBJOP = (StringToWordVector)modified.get(1);
	        modified = this.applyFilter(trainInsts, "-opW-SBJ_OP");
	        trainInsts = (Instances)modified.get(0); _nGramsSBJOP = (HashSet<String>)modified.get(1); 
	        modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltTAROP, new int[] {1}, "-opW-TAR_OP", 1,1);
	        trainInsts = (Instances)modified.get(0); _ngramFiltTAROP = (StringToWordVector)modified.get(1);
	        modified = this.applyFilter(trainInsts, "-opW-TAR_OP");
	        trainInsts = (Instances)modified.get(0); _nGramsTAROP = (HashSet<String>)modified.get(1); 
	        //_featureRanges.add(trainInsts.numAttributes()-size+2);
	        //_featureNames.add("-op");
	        //size = trainInsts.numAttributes();
        }
        
        if (_polarity) {
        	
	        modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltSBJPLUS, new int[] {1}, "-polW-SBJ_PLUS", 1,1);
	        trainInsts = (Instances)modified.get(0); _ngramFiltSBJPLUS= (StringToWordVector)modified.get(1); 
	        modified = this.applyFilter(trainInsts, "-polW-SBJ_PLUS");
	        trainInsts = (Instances)modified.get(0); _nGramsSBJPLUS = (HashSet<String>)modified.get(1); 
	        modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltTARPLUS, new int[] {1}, "-polW-TAR_PLUS", 1,1);
	        trainInsts = (Instances)modified.get(0); _ngramFiltTARPLUS = (StringToWordVector)modified.get(1);
	        modified = this.applyFilter(trainInsts, "-polW-TAR_PLUS");
	        trainInsts = (Instances)modified.get(0); _nGramsTARPLUS = (HashSet<String>)modified.get(1); 

	        modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltSBJNEG, new int[] {1}, "-polW-SBJ_NEG", 1,1);
	        trainInsts = (Instances)modified.get(0); _ngramFiltSBJNEG= (StringToWordVector)modified.get(1);
	        modified = this.applyFilter(trainInsts, "-polW-SBJ_NEG");
	        trainInsts = (Instances)modified.get(0); _nGramsSBJNEG = (HashSet<String>)modified.get(1); 
	        modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltTARNEG, new int[] {1}, "-polW-TAR_NEG", 1,1);
	        trainInsts = (Instances)modified.get(0); _ngramFiltTARNEG = (StringToWordVector)modified.get(1);
	        modified = this.applyFilter(trainInsts, "-polW-TAR_NEG");
	        trainInsts = (Instances)modified.get(0); _nGramsTARNEG = (HashSet<String>)modified.get(1); 

	        //_featureRanges.add(trainInsts.numAttributes()-size+2);
	        //_featureNames.add("-op");
	        //size = trainInsts.numAttributes();
        }
        
        if (_phraseSimilarity) {
        	/*modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltPHRASE, new int[] {1}, "-PssWIntersection", 1,1);
	        trainInsts = (Instances)modified.get(0); _ngramFiltPHRASEIntersection = (StringToWordVector)modified.get(1); 
	        modified = this.applyFilter(trainInsts, "-PssWIntersection");
	        trainInsts = (Instances)modified.get(0); _nGramsPHRASEIntersection = (HashSet<String>)modified.get(1); 
        	*/
	        modified = this.adjustNames(trainInsts, ngramFilt, _ngramFiltPHRASEUnique, new int[] {1}, "-PssWUnique", 1,1);
	        trainInsts = (Instances)modified.get(0); _ngramFiltPHRASEUnique= (StringToWordVector)modified.get(1); 
	        modified = this.applyFilter(trainInsts, "-PssWUnique");
	        trainInsts = (Instances)modified.get(0); _nGramsPHRASEUnique = (HashSet<String>)modified.get(1);
        }


        trainInsts.setClassIndex(0);
        //dump(trainInsts);
        
        return trainInsts;
    }
   
    private List<Object> adjustNames(Instances trainInsts, StringToWordVector ngramFilt, StringToWordVector filter, int [] attributes, String type, int min, int max) throws Exception{
          	
        NGramTokenizer tok = new NGramTokenizer();
        tok.setNGramMinSize(min);
        tok.setNGramMaxSize(max);
        ngramFilt.setTokenizer(tok);

    	int oldNumAttributes = trainInsts.numAttributes();
        trainInsts.setClassIndex(0);
        ngramFilt.setInputFormat(trainInsts);
        ngramFilt.setAttributeIndicesArray(attributes);
        filter = (StringToWordVector)Filter.makeCopy(ngramFilt);
        trainInsts = Filter.useFilter(trainInsts, filter);

        adjustAttributeNames(type,trainInsts, oldNumAttributes - 1, trainInsts.numAttributes());
        
        List<Object> modified = new ArrayList<Object>();
        modified.add(trainInsts);
        modified.add(filter);
        return modified;
    }
    
    private List<Object> applyFilter(Instances trainInsts, String type) throws Exception{
		trainInsts = chiSquareByName(trainInsts,type + "_");
        HashSet<String> ngrams = setNgramsByName(trainInsts,type + "_");
        
        List<Object> modified = new ArrayList<Object>();
        modified.add(trainInsts);
        modified.add(ngrams);
        return modified;
    }
    
    private Instances applyTestFilter(Instances testInsts, StringToWordVector filter, HashSet<String> ngrams, String type) throws Exception {
    	int size = testInsts.numAttributes();
   	 	
   	 	testInsts = Filter.useFilter(testInsts, filter);
   	 	//testInsts = chiSquareTest(testInsts,ngrams,size-1);
   	 	adjustAttributeNames(type,testInsts, size - 1, testInsts.numAttributes());
   	 	testInsts = chiSquareTestByName(testInsts,ngrams,type + "_");
   	 	
   	 	return testInsts;
    }
        
    public Instances createTestFilters(Instances testInsts) throws Exception {
   	 	
   	 	if (_ngram) {
	   	 	testInsts = this.applyTestFilter(testInsts, _ngramFiltSBJ, _nGramsSBJ, "-ngram-GEN_SBJ");
	   	 	testInsts = this.applyTestFilter(testInsts, _ngramFiltTAR, _nGramsTAR, "-ngram-GEN_TAR");
	   	 	//testInsts = this.applyTestFilter(testInsts, _ngramFilt, _nGrams, "-ngram-GEN");
   	 	}
   	 	
   	 	if (_POS) {
   	 		testInsts = this.applyTestFilter(testInsts, _ngramFiltSBJPOS, _nGramsSBJPOS, "-pos-SBJ_POS");
   	 		testInsts = this.applyTestFilter(testInsts, _ngramFiltTARPOS, _nGramsTARPOS, "-pos-TAR_POS");
   	 	}
   	 	
   	 	if (_opinion) {
   	 		testInsts = this.applyTestFilter(testInsts, _ngramFiltSBJOP, _nGramsSBJOP, "-opW-SBJ_OP");
   	 		testInsts = this.applyTestFilter(testInsts, _ngramFiltTAROP, _nGramsTAROP, "-opW-TAR_OP");
   	 	}
   	 	
   	 	if (_polarity) {
   	 		testInsts = this.applyTestFilter(testInsts, _ngramFiltSBJPLUS, _nGramsSBJPLUS, "-polW-SBJ_PLUS");
   	 		testInsts = this.applyTestFilter(testInsts, _ngramFiltTARPLUS, _nGramsTARPLUS, "-polW-TAR_PLUS");
   	 		testInsts = this.applyTestFilter(testInsts, _ngramFiltSBJNEG, _nGramsSBJNEG, "-polW-SBJ_NEG");
   	 		testInsts = this.applyTestFilter(testInsts, _ngramFiltTARNEG, _nGramsTARNEG, "-polW-TAR_NEG");
   	 	}
        if (_phraseSimilarity) {
        	//testInsts = this.applyTestFilter(testInsts, _ngramFiltPHRASEIntersection, _nGramsPHRASEIntersection, "-PssWIntersection");
        	testInsts = this.applyTestFilter(testInsts, _ngramFiltPHRASEUnique, _nGramsPHRASEUnique, "-PssWUnique");
        }

   	 	return testInsts;
    }
    
    private void runAll(Hashtable<String,Evaluation> results, Classifier classifier, Instances trainInsts, Instances testInsts) throws Exception {
    	
		int counter = 0;
		List <String> experiments = new ArrayList<String>();
		if (_thread) { counter++; experiments.add("-thread"); }
		if (_ngram) { counter++; experiments.add("-ngram"); }
		if (_opinion) { counter+=2; experiments.add("-opN"); experiments.add("-opW"); }
		if (_polarity) { counter+=2; experiments.add("-polN"); experiments.add("-polW"); }
		if (_question) { counter++; experiments.add("-?"); }
		if (_sentenceLength) { counter++; experiments.add("-sl"); }
		if (_POS) { counter++; experiments.add("-pos"); }		
		if (_sentenceSimilarity) { counter++; experiments.add("-ss"); }
		if (_phraseSimilarity) { counter+=2; experiments.add("-PssN"); experiments.add("-PssW"); }
		
		for (int index = 0; index < Math.pow(2, counter); index++) {
			String experiment = Integer.toBinaryString(index);
			while (experiment.length() < counter) experiment = "0" + experiment;
		
			String experimentName = "agreement";
			HashSet<String> experimentList = new HashSet<String>();
			//experimentList.add("-thread");
			
			for (int i = 0; i < experiments.size(); i++) {
				if (experiment.charAt(i) == '1') {
					experimentName += experiments.get(i);
					experimentList.add(experiments.get(i));
				}
			}

			Instances train = subset(trainInsts,experimentList);
			Instances test = subset(testInsts,experimentList);
			train.setClass(train.attribute(0));
			//System.out.println(experimentName);
			//this.crossValidation(train, false, "-logistic", -1);
			classifier.buildClassifier(train);

			Evaluation eval = results.get(experimentName);
			if (eval == null) eval = new Evaluation(train);
			
			eval.evaluateModel(classifier, test);
			results.put(experimentName, eval);
		    //eval.crossValidateModel(classifier, train, 10, new Random(0));
		    //System.out.println(experimentName);
		    // none agreement disagreement
		    //System.out.println(experimentName + " " + eval.fMeasure(0) + " " + eval.fMeasure(1) +" " + eval.fMeasure(2));
		    //System.out.println(eval.toClassDetailsString());
		}
    }
    
    
    private void runList(List<String> experiments, Hashtable<String,Evaluation> results, Classifier classifier, Instances trainInsts, Instances testInsts) throws Exception {
    	
		for (int index = 0; index < experiments.size(); index++) {
			
			String experimentName = "agreement" + experiments.get(index);
			HashSet<String> experimentList = new HashSet<String>();
			//experimentList.add("-thread");
			
			String [] e = experiments.get(index).split("-");
			
			for (int i = 0; i < e.length; i++) {
					experimentList.add("-" + e[i]);
			}

			Instances train = subset(trainInsts,experimentList);
			Instances test = subset(testInsts,experimentList);
			train.setClass(train.attribute(0));
			//System.out.println(experimentName);
			//this.crossValidation(train, false, "-logistic", -1);
			classifier.buildClassifier(train);

			Evaluation eval = results.get(experimentName);
			if (eval == null) eval = new Evaluation(train);
			
			eval.evaluateModel(classifier, test);
			results.put(experimentName, eval);
		    //eval.crossValidateModel(classifier, train, 10, new Random(0));
		    //System.out.println(experimentName);
		    // none agreement disagreement
		    //System.out.println(experimentName + " " + eval.fMeasure(0) + " " + eval.fMeasure(1) +" " + eval.fMeasure(2));
		    //System.out.println(eval.toClassDetailsString());
		}
    }
    
	
	public Instances subset(Instances instances, HashSet<String> experiments) {
		
		try {
			for (String name : _featureNames) {
				if (experiments.contains(name)) continue;
				instances = remove(instances, name);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instances;
	}

	protected Instances remove(Instances instances, String name) throws Exception {
		
		String attributes = "";
		
		//System.out.println(instances.attribute(start) + " " + instances.attribute(end-1));
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name)) {
				attributes += "," + (index+1);
			}
		}
		
		Remove remove = new Remove();
        remove.setAttributeIndices(attributes);
        remove.setInputFormat(instances);
        remove.setInvertSelection(false);
        return Filter.useFilter(instances, remove);
	}
	
    // classify new entry and output
    public List<Arc> classify(HashMap<String,Arc> testArcs, Instances testInsts) throws Exception {

    	if (testInsts != null) System.out.println("[AgreementClassifier] Classifying " + testInsts.numInstances() + " arcs");
    	int none = 0; int agreement = 0; int disagreement = 0;
    	
    	List<Arc> classifiedArcs = new ArrayList<Arc>(); 
    	
        if (testInsts == null) {
        	return classifiedArcs;
        }
        
        List<Arc> arcs = new ArrayList<Arc>(testArcs.values());

        for(int i = 0; i < testInsts.numInstances(); i++) {
        	
            int cls = (int)classifier.classifyInstance(testInsts.instance(i));
            
            switch (cls) {
	            case 0: none++; break;
	            case 1: agreement++; break;
	            case 2: disagreement++; break;
            }
            
            Arc arc = arcs.get(i);
            
            if(cls != 0 && !arc.getSubject().getAuthor().equals(arc.getTarget().getAuthor())) {
            	
            	Arc a = new Arc(arc.getSubject().getId(), arc.getTarget().getId(), arc.getSubject().getBody(), arc.getTarget().getBody(), arc.getSubject().getAuthor(), arc.getTarget().getAuthor(), 
            			arc.getSubject().getDepth(), arc.getTarget().getDepth(), arc.isAncestor(), cls);	
            	classifiedArcs.add(a);
            }
        }
        System.out.println("[AgreementClassifier] == Test File == none: " + none + ", agreeement: " + agreement + ", disagreement: " + disagreement);
        return classifiedArcs;
    }
    
    public void write(List<Arc> arcs, String outputFile) throws Exception {
    	PrintWriter writer = new PrintWriter(new FileWriter(outputFile + ".agreement"));
        
        if (arcs == null) {
        	writer.print("");
        	writer.close();
        	return;
        }

        for (Arc arc : arcs) {
        		
        	// class \t subjectId \t targetId \t subjectAuthor \t targetAuthor \t subjectBody \t targetBody
            writer.print(arc.getType() == 1 ? "agreement" : "disagreement");
            writer.print("\t");
            writer.print(arc.getSubject().getId());
            writer.print("\t");
            writer.print(arc.getTarget().getId());
            writer.print("\t");
            writer.print(arc.getSubject().getAuthor());
            writer.print("\t");
            writer.print(arc.getTarget().getAuthor());
            writer.print("\t");
            writer.print(arc.getSubject().getBody());
            writer.print("\t");
            writer.print(arc.getTarget().getBody());
            writer.print("\t");

            // if this is not for SCIL output thread structure information too!
            // \t depthSbj \t depthTar \t isParent
            
            	writer.print(arc.getSubject().getDepth());
                writer.print("\t");
                writer.print(arc.getTarget().getDepth());
                writer.print("\t");
                writer.print(arc.isAncestor());
                writer.print(arc.getSentenceSimilarity() + "\t");
            
            
            writer.println();
        }
        writer.close();
    }
    
    protected Instance arcToInstance(Arc a, Instances instances) {
        Instance i = new Instance(attVector.size());
        
        if (_thread) {
	        i.setValue(parentIsRoot, a.getSubject().getDepth() == 0 ? 1 : 0);
	        //i.setValue(closestAncestorIsRoot, (!a.isParent() || (a.isParent() && a.getTarget().getDepth() == 0)) ? 1 : 0);
	        i.setValue(sameDepth, a.getSubject().getDepth() == a.getTarget().getDepth() ? 1 : 0);
	        i.setValue(isAncestor, a.isAncestor() ? 1 : 0);
	        i.setValue(isParent, a.isParent() ? 1 : 0);
	        i.setValue(sameAuthor, a.getSubject().getAuthor().equals(a.getTarget().getAuthor()) ? 1 : 0);
	        i.setValue(this.numberOfResponsesSubject, a.getSubject().getNumResponses());
	        i.setValue(this.numberOfResponsesTarget, a.getTarget().getNumResponses());
	        i.setValue(this.sentencePositionSubject,a.getSubject().getApproxSentencePosition());
	        i.setValue(this.sentencePositionTarget,a.getTarget().getApproxSentencePosition());
	        //i.setValue(overlap, a.getOverlap());
        }
        
        if (_question) {
        	i.setValue(questionSBJ, a.getSubject().getBody().endsWith("?") ? 1 : 0);
        	i.setValue(questionTAR, a.getTarget().getBody().endsWith("?") ? 1 : 0);
        }
        
        if (_sentenceLength) {
        	i.setValue(sentenceLengthSBJ, a.getSubject().getBody().split("\\s+").length);
        	i.setValue(sentenceLengthTAR, a.getTarget().getBody().split("\\s+").length);
        }
        
        if (_sentenceSimilarity) {
        	i.setValue(sentenceSimilarity, a.getMaxSentenceSimilarity(.65));
        }
        
        if (_phraseSimilarity) {
        	// check whether polarity/opinion of most similar phrases match as long as it beats threshold
        	i.setValue(phraseSimilarityTypes,a.getPhraseSimilaritySentiment(.6,false));	        
	        //i.setValue(phraseSimilarityBodyIntersection, a.getPhraseSimilarityWords(.6));
	        i.setValue(phraseSimilarityBodyUnique, a.getPhraseSimilarityUniqueWords(.6,false));
	        int [] counts = a.getPhraseSimilarity(.6,false);
	        i.setValue(phraseSimilarityCountSame, a.getNumPhrases() > 0 ? counts[0]/a.getNumPhrases() : 0); // how many similar phrases there are with same polarity
	        i.setValue(phraseSimilarityCountDiff, a.getNumPhrases() > 0 ? counts[1]/a.getNumPhrases() : 0); // how many similar phrases there are with different polarity
	        i.setValue(phraseSimilarityCountNA, a.getNumPhrases() > 0 ? counts[2]/a.getNumPhrases() : 0); // how many similar phrases there are with no polarity
        }
        if (_opinion) {
        	boolean _normalized = true;
        	
	        // for subject and target
	        i.setValue(hasSentimentSBJ,a.getSubject().hasSentiment() ? 1 : 0);
	        i.setValue(sentimentCountSBJ,a.getSubject().getSentimentCount(_normalized));
	        i.setValue(hasSentimentTAR,a.getTarget().hasSentiment() ? 1 : 0);
	        i.setValue(sentimentCountTAR,a.getTarget().getSentimentCount(_normalized));
	        
	        i.setValue(subjectBodyOpinion, Arrays.toString(a.getSubject().getSubjectiveWords(false).toArray()));
	        i.setValue(targetBodyOpinion, Arrays.toString(a.getTarget().getSubjectiveWords(false).toArray()));
	    }
        
        if (_polarity) {
        	boolean _normalized = true;
        	
	        // for subject and target
	        i.setValue(hasPositiveSBJ,a.getSubject().hasPolarity(true) ? 1 : 0);
	        i.setValue(positiveCountSBJ,a.getSubject().getPolarityCount(_normalized, true, false));
	        i.setValue(hasPositiveTAR,a.getTarget().hasPolarity(true) ? 1 : 0);
	        i.setValue(positiveCountTAR,a.getTarget().getPolarityCount(_normalized, true, false));

	        i.setValue(hasNegativeSBJ,a.getSubject().hasPolarity(false) ? 1 : 0);
	        i.setValue(negativeCountSBJ,a.getSubject().getPolarityCount(_normalized, false, false));
	        i.setValue(hasNegativeTAR,a.getTarget().hasPolarity(false) ? 1 : 0);
	        i.setValue(negativeCountTAR,a.getTarget().getPolarityCount(_normalized, false, false));

	        
	        i.setValue(subjectBodyPositive, Arrays.toString(a.getSubject().getPolarityWords(true,false).toArray()));
	        i.setValue(targetBodyPositive, Arrays.toString(a.getSubject().getPolarityWords(true,false).toArray()));
	        i.setValue(subjectBodyNegative, Arrays.toString(a.getTarget().getPolarityWords(false,false).toArray()));
	        i.setValue(targetBodyNegative, Arrays.toString(a.getTarget().getPolarityWords(false,false).toArray()));
	    }

        
        if (_ngram) {
	        i.setValue(subjectBody, a.getSubject().getBody());
	        i.setValue(targetBody, a.getTarget().getBody());
        }
        
        if (_POS) {
	        i.setValue(subjectBodyPOS, a.getSubject().getPOS(false));
	        i.setValue(targetBodyPOS, a.getTarget().getPOS(false));
        }
        
        //i.setValue(docId, a.getDocId());
        
        switch(a.getType()) {
            case Arc.AGREEMENT:
                i.setValue(type, types.indexOf("agreement"));
                break;
            case Arc.DISAGREEMENT:
                i.setValue(type, _2way ? types.indexOf("agreement") : types.indexOf("disagreement"));
                break;
            case Arc.NONE:
                i.setValue(type, types.indexOf("none"));
                break;
        }
        return i;
    }
    
	protected Instances adjustAttributeNames(String name, Instances instances, int start, int count) {
		for (int index = start; index < count; index++) {
			instances.renameAttribute(index, name + "_" + instances.attribute(index).name());
		}
		return instances;
	}

    
    protected Instances chiSquare(Instances instances, int start, int count) throws Exception {
		// 1. Copy instances in list
		Remove chiSquareFilt = new Remove();
        
        // set class to be included
        chiSquareFilt.setAttributeIndices("first," + start + "-" + count);
        chiSquareFilt.setInvertSelection(true);
        chiSquareFilt.setInputFormat(instances);
        Instances subset = Filter.useFilter(instances, chiSquareFilt);
        subset.setClassIndex(0);
		// 2. Perform Chi Square Filter
        ChiSquaredAttributeEval eval = new ChiSquaredAttributeEval();
		eval.buildEvaluator(subset);
		
		// 3. remove attributes from original instances that do not pass threshold
		int deleted = 0;
		int kept = 0;
		
		String delete = "";
		
		for (int index = 1; index < subset.numAttributes(); index++) {
			double score = eval.evaluateAttribute(index);
			
			if (score == 0.0) {
			  //System.out.println(subset.attribute(index) + ": " + eval.evaluateAttribute(index));
				//subset.deleteAttributeAt(index);
				delete += "," + (index+1);
				//index--;
				deleted++;
			}
			else {
				kept++;
				//keep += "," + index;
				System.out.print(subset.attribute(index).name() + "(" + score + "),");
			}
		}
		
		Remove remove = new Remove();
        remove.setAttributeIndices(start + "-" + count);
        remove.setInputFormat(instances);
        remove.setInvertSelection(true);
        instances = Filter.useFilter(instances, remove);
        
        remove = new Remove();
        remove.setAttributeIndices("first," + delete.substring(1));
        remove.setInputFormat(subset);
        //remove.setInvertSelection(true);
        subset = Filter.useFilter(subset, remove);
        
        instances = Instances.mergeInstances(instances, subset);
		
		System.out.println("\n[WekaBuilder.chiSquare] removed " + deleted + " attributes. Kept " + kept + " attributes. "+ instances.numAttributes() + " left.");
		
		return instances;
	}
    
    protected Instances chiSquareTest(Instances instances, HashSet<String> grams, int start) throws Exception {

		String attributes = "";
		
		for (int index = start; index < instances.numAttributes(); index++) {
			if (!grams.contains(instances.attribute(index).name())) {
				attributes += (instances.attribute(index).index() + 1) + ",";
			}
		}
		
		Remove chiSquareFilt = new Remove();
        chiSquareFilt.setAttributeIndices(attributes);
        chiSquareFilt.setInputFormat(instances);
        chiSquareFilt.setInvertSelection(false);
        return Filter.useFilter(instances, chiSquareFilt);
		//return instances;
	}
    
	protected HashSet<String> setNgrams(Instances instances, int start, int end) {
		HashSet<String> grams = new HashSet<String>();
		
		for (int index = start; index < end; index++) {
			grams.add(instances.attribute(index).name());
		}
		return grams;
	}
	
	protected Instances chiSquareByName(Instances instances, String name) throws Exception {

		//System.err.println(name);
		
		// 1. Copy instances in list
		Remove chiSquareFilt = new Remove();
        
		String indices = "first";
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name)) {
				indices += "," + (index+1);
			}
		}
		
        // set class to be included
        chiSquareFilt.setAttributeIndices(indices);
        chiSquareFilt.setInvertSelection(true);
        chiSquareFilt.setInputFormat(instances);
        Instances subset = Filter.useFilter(instances, chiSquareFilt);
        subset.setClassIndex(0);
		// 2. Perform Chi Square Filter
        ChiSquaredAttributeEval eval = new ChiSquaredAttributeEval();
        eval.buildEvaluator(subset);
		
		// 3. remove attributes from original instances that do not pass threshold
		int deleted = 0;
		int kept = 0;
		for (int index = 1; index < subset.numAttributes(); index++) {
			if (eval.evaluateAttribute(index) == 0.0) {
			    //System.err.print(subset.attribute(index) + ": " + eval.evaluateAttribute(index) + ", ");
				instances.deleteAttributeAt(instances.attribute(subset.attribute(index).name()).index());
				deleted++;
			}
			else {
				System.err.print(subset.attribute(index) + ": " + eval.evaluateAttribute(index) + ", ");
				kept++;
			}
		}
		System.err.println();
		System.out.println("[AgreementClassifier.chiSquare] " + name + ": removed " + deleted + " attributes. Kept " + kept + " attributes. "+ instances.numAttributes() + " left.");
		return instances;
	}
	
	protected Instances chiSquareTestByName(Instances instances, HashSet<String> grams, String name) throws Exception {
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name) && !grams.contains(instances.attribute(index).name())) {
				instances.deleteAttributeAt(index);
				index--;
			}
		}
		return instances;
	}
	
	protected HashSet<String> setNgramsByName(Instances instances, String name) {
		HashSet<String> grams = new HashSet<String>();
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name)) {
				grams.add(instances.attribute(index).name());
			}
		}
		return grams;
	}
}
