package agreement.classifier;

import agreement.Arc;
import agreement.Node;
import agreement.Utilities;
import agreement.crf.ResponseTemplate;
import agreement.io.ProcessBlog;
import bsh.EvalError;
import cc.mallet.classify.*;
import cc.mallet.grmm.inference.Inferencer;
import cc.mallet.grmm.learning.ACRF;
import cc.mallet.grmm.learning.ACRF.Template;
//import cc.mallet.grmm.learning.ACRFEvaluator;
import cc.mallet.grmm.util.LabelsAssignment;
import cc.mallet.grmm.learning.*;
import cc.mallet.grmm.learning.DefaultAcrfTrainer.LogEvaluator;
import cc.mallet.grmm.learning.DefaultAcrfTrainer.TestResults;
import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.*;
import cc.mallet.types.*;
import cc.mallet.util.BshInterpreter;
import cc.mallet.util.FileUtils;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

import processing.GeneralUtils;

import edu.columbia.opinion.process.*;

public class MalletClassifier extends AgreementClassifier {
	
	boolean _mallet;
	DAL _DAL;
	SentiWordNet _SentiWordNet;
	HashSet<String> _stopWords;
	int _size = 0;
	boolean _overwrite = false;
    int _numIterations = 1000;	
    boolean _first = true;
	
	public MalletClassifier(String dataDirectory, boolean thread,
			boolean usePOS, boolean balanced, boolean opinion,
			boolean polarity, boolean question, boolean sentenceLength,
			boolean ngram, boolean sentenceSimilarity,
			boolean phraseSimilarity, boolean twoWay, boolean mallet, int size, boolean overwrite, boolean liwc, boolean sm, boolean entrainment, boolean first)
	throws Exception {
		super(dataDirectory, thread, usePOS, balanced, opinion, polarity, question,
				sentenceLength, ngram, sentenceSimilarity, phraseSimilarity, twoWay, liwc, sm, entrainment);
		
		String stopwordDir = null;
		String sentiWordNetDir = null;
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			stopwordDir = prop.getProperty("stopwords");
			sentiWordNetDir = prop.getProperty("sentiwordnet");
		} catch (Exception e) {
			e.printStackTrace();
		}

		_mallet = mallet;
		stopwords(stopwordDir);
		_DAL = DAL.getInstance();
		_SentiWordNet = new SentiWordNet(sentiWordNetDir); 
		_size = size;
		_overwrite = overwrite;
		_first = first;
	}
	
	private void stopwords(String file) {
		
		_stopWords = new HashSet<String>();
		
		try {
		 BufferedReader in = new BufferedReader(new FileReader(file));
	     String line = "";
	     while ((line = in.readLine()) != null) {
            _stopWords.add(line.trim());
	     }
	     in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception {
		
        boolean newClassifier = false;
        String trainingDirectory = null;
        String [] testDirectory = null;
        boolean thread = false;
        boolean pos = false;
        boolean balanced = false;
        boolean opinion = false;
        boolean polarity = false;
        boolean question = false;
        boolean sentenceLength = false;
        boolean ngram = false;
        boolean sentenceSimilarity = false;
        boolean phraseSimilarity = false;
        boolean liwc = false;
        boolean sm = false;
        boolean entrainment = false;
        String nameTrain = "train";
        String nameTest = "test";
        boolean twoWay = false;
        String dataDirectory = "";
        String outputDirectory = "";
        String processingDirectory = "";
        String processingDirectoryTest = null;
        String acrfType = "maxent";
        int size = -1;
        int sizeTest = -1;
        boolean overwrite = false;
        String experiment = "";
        String annotationDirectory = null;
        String annotationOutputDirectory = null;
        boolean first = true;
        boolean balTest = false;
        
        for (int i = 0; i < args.length; i++) {
        	
        	if (args[i].equals("-overwrite")) {
        		overwrite = true;
        	}
        	if (args[i].equals("-retrain")) {
        		newClassifier = true;
        		overwrite = true;
        	}
        	else if (args[i].equals("-train")) {
        		trainingDirectory = args[++i];
        	}
        	else if (args[i].equals("-test")) {
        		testDirectory = args[++i].split(",");
        	}
        	else if (args[i].equals("-annotations")) {
        		annotationDirectory = args[++i];
        	}
        	else if (args[i].equals("-annotationOutput")) {
        		annotationOutputDirectory = args[++i];
        	}
        	else if (args[i].equals("-name")) {
        		nameTrain = args[++i];
        		nameTest = args[++i];
        		experiment += "-" + nameTrain + ":" + nameTest;
        	}
        	else if (args[i].equals("-pos")) {
        		pos = true;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-balanced")) {
        		balanced = true;
        		experiment += "-bal";
        	}
        	else if (args[i].equals("-balancedTest")) {
        		balTest = true;
        		experiment += "-balTest";
        	}
        	else if (args[i].equals("-nofirst")) {
        		first = false;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-opinion")) {
        		opinion = true;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-polarity")) {
        		polarity = true;
        		experiment += args[i];
        	}

        	else if (args[i].equals("-question")) {
        		question = true;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-ngram")) {
        		ngram = true;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-ss")) {
        		sentenceSimilarity = true;
        		phraseSimilarity = true;
        		experiment += args[i];
        	}
        	/*else if (args[i].equals("-phraseSS")) {
        		phraseSimilarity = true;
        		experiment += args[i];
        	}*/
        	else if (args[i].equals("-thread")) {
        		thread = true;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-liwc")) {
        		liwc = true;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-sm")) {
        		sm = true;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-entrainment")) {
        		entrainment = true;
        		experiment += args[i];
        	}
        	else if (args[i].equals("-d")) {
        		dataDirectory = args[++i];
        	}
        	else if (args[i].equals("-o")) {
        		outputDirectory = args[++i];
        	}
        	else if (args[i].equals("-p")) {
        		processingDirectory = args[++i];
        	}
        	else if (args[i].equals("-pTest")) {
        		processingDirectoryTest = args[++i];
        	}
        	else if (args[i].equals("-acrf"))
        	{
        		acrfType = args[++i];
        		experiment = acrfType + experiment;
        	}
        	else if (args[i].equals("-size")) {
        		size = Integer.parseInt(args[++i]);
        		sizeTest = Integer.parseInt(args[++i]);
        	}
        	else if (args[i].equals("-2way")) {
        		twoWay = true;
        		experiment += "-2way";
        	}
        }
        
        System.out.println("Running Experiment: " + experiment);
        
        if (processingDirectoryTest == null) processingDirectoryTest = processingDirectory;
        
        MalletClassifier classifier = new MalletClassifier(dataDirectory, thread, pos, balanced, opinion, polarity, question, 
        		sentenceLength, ngram, sentenceSimilarity, phraseSimilarity, false, 
        		acrfType.equals("acrf") ? false : true, size, overwrite, liwc, sm, entrainment, first); 
       
        Pipe pipe = classifier.createPipe(acrfType.equals("acrf") ? false : true);
        
        InstanceList train = null;
        InstanceList test = null;
       
    	if (!nameTest.equals("unannotated") || !new File(outputDirectory + "/models/" + experiment + "-" + size + ".ser.gz").exists()) {
    		System.out.println("[AgreementClassifier] LOADING TRAINING");
    		train = classifier.loadChain("train-" + nameTrain, newClassifier, trainingDirectory, 
        			processingDirectory, outputDirectory, "/proj/nlp/users/sara/influence/models/opinion/", 
        			annotationDirectory, annotationOutputDirectory, pipe, size, twoWay, balanced, true, nameTrain, outputDirectory, false);
    	}
    	
	    System.out.println("[AgreementClassifier] LOADING TESTING");
		boolean permanent = true;
	    
		for (String tD : testDirectory) {
	    	System.out.println("Loading Test Directory: " + tD);
	    	
	    	String name = "";
	    	String output = outputDirectory;
	    	File f = null;
	    	
	    	if (!permanent) {
	    		name = "";
		    	output = outputDirectory;
		    	f = File.createTempFile("agree", "dir");
		    	f.delete();
	    		f.mkdir();
	    		output = f.toString();
	    	}
	    	
	    	if (nameTest.equals("unannotated")) {
	    		name = "";
	    	}
	    	else if (tD.indexOf("development") > 0) name = "dev-" + nameTest;
	    	else name = "test-" + nameTest;
	    	
	    	if (new File(outputDirectory + "/models/" + experiment + "-" + size + ".ser.gz").exists()) {
				// Load classifier here 
				ObjectInputStream ois =
		            new ObjectInputStream (new FileInputStream (outputDirectory + "/models/" + experiment + "-" + size + ".ser.gz"));
		        pipe = ((Classifier) ois.readObject()).getInstancePipe();
		        ois.close();
	    	}
	    		    	
	    	InstanceList list = classifier.loadChain(name, newClassifier, tD, 
        		processingDirectoryTest, output, "/proj/nlp/users/sara/influence/models/opinion/", annotationDirectory, annotationOutputDirectory, pipe, sizeTest, twoWay, balTest, false, nameTrain, outputDirectory, nameTest.equals("unannotated") ? true : false);
	    	if (f != null) classifier.delete(f);

	        if (nameTest.equals("unannotated")) {
	        	if (!new File(outputDirectory + "/models/" + experiment + "-" + size + ".ser.gz").exists())
	        		classifier.loadModel(list, acrfType, experiment, outputDirectory);
				classifier.printAnnotations(outputDirectory + "/models/" + experiment + "-" + size + ".ser.gz", list,tD,outputDirectory + "/output/");
	        }
	        
	    	if (test == null) test = list;
	    	else test.addAll(list);
	    }
		
        if (acrfType.equals("acrf")) {
            LogEvaluator eval = classifier.classify("general", train, test, experiment, outputDirectory, nameTrain + ":" + nameTest);

        	// experiment with annotations
        	if (annotationDirectory != null || dataDirectory.indexOf("create_debate") >= 0) 
        		classifier.printResults(outputDirectory, eval, experiment, nameTrain + "-" + size + ":" + nameTest + "-" + sizeTest);
        	// write out the annotations to a file
        	else  {
        		for (String tD : testDirectory) {
        			classifier.printAnnotations(tD, processingDirectoryTest, eval);
        		}
        	}
        		
        }        	
        Classifier c = null;
        
        if (!new File(outputDirectory + "/models/" + experiment + "-" + size + ".ser.gz").exists()) {
        	c = classifier.loadModel(train, acrfType, experiment, outputDirectory);
        }
        else {
        	ObjectInputStream ois =
    	            new ObjectInputStream (new FileInputStream (outputDirectory + "/models/" + experiment + "-" + size + ".ser.gz"));
    	        c = (Classifier) ois.readObject();
    	        ois.close();	
        }
        classifier.classifyMallet(test, c, outputDirectory, experiment, nameTrain, nameTest, twoWay);
    }
	
	public InstanceList loadChain(String name, boolean newClassifier, String directory, String processingDirectory, String outputDirectory,
			String modelDirectory, String annotationDirectory, String annotationOutputDirectory, Pipe pipe, 
			int size, boolean twoWay, boolean balanced, boolean  training, String trainingName, String trainingDirectory, boolean unannotated) throws Exception {
		if (newClassifier || !new File(outputDirectory + "/" + name + "/").exists() || 
				!new File(outputDirectory + "/" + name + "/ss" + trainingName + ".txt").exists()) {
			
			String [] files = new String[1];
			
			File f = new File(directory);
			if (f.isDirectory()) files = f.list();
			else {
				files[0] = f.getName();
				directory = f.getParent();
			}
			
			List<agreement.Thread> threads = loadDirectChain(newClassifier, directory, 
        		files, processingDirectory, outputDirectory, modelDirectory, size,
        				_opinion, _polarity, _POS, _sentenceSimilarity, _phraseSimilarity, annotationDirectory, annotationOutputDirectory, unannotated);
			generatePerNodeFeatures(outputDirectory + "/" + name + "/", threads, training, trainingDirectory + "/train-" + trainingName + "/", trainingName, unannotated);
		}
    
        return load(outputDirectory + "/" + name + "/",pipe, size, balanced, twoWay, trainingName);
	}
	
	/**
	 * Return list of answers per post
	 * @return
	 */
	public LogEvaluator classify(String acrfType, InstanceList train, InstanceList test, String experiment, String outputDirectory, String name) throws Exception {

        Template [] templates = new Template[1];
        // sequence template
        if (acrfType.equals("bigram"))  templates[0] = new ACRF.BigramTemplate(0); // ordered by date
        else if (acrfType.equals("general")) templates[0] = new ResponseTemplate(0); // add link for thread neighbors
        else templates[0] = new ACRF.UnigramTemplate(0);
        
        // General CRF
        ACRF acrf = null; 
        LogEvaluator eval = (LogEvaluator)GenericAcrfTui.createEvaluator("LOG"); //evalOption.value);
        
        
        
        if (new File (outputDirectory + "/models/acrf-" + experiment + "-" + _size + ".ser.gz").exists() && !_overwrite) {
        	System.out.println("Loading Model: acrf-" + experiment + "-" + _size + ".ser.gz");
        	acrf = loadClassifier(new File (outputDirectory + "/models/acrf-" + experiment + "-" + _size + ".ser.gz"));
        }
        else {
        	System.out.println("Generating Model: acrf-" + experiment + "-" + _size + ".ser.gz");
        	acrf = new ACRF(train.getPipe(), templates);
	        
	        acrf.setInferencer(createInferencer("LoopyBP"));
	        acrf.setViterbiInferencer(createInferencer("LoopyBP.createForMaxProduct()"));
        	ACRFTrainer trainer = new DefaultAcrfTrainer ();
        	trainer.train (acrf, train, null, test, eval, _numIterations); //9999
        	System.out.println("Serializing Model");
        	FileUtils.writeGzippedObject (new File (outputDirectory + "/models/acrf-" + experiment + "-" + _size + ".ser.gz"), acrf);
        	System.out.println("Testing Model");
        }
        
        eval.test(acrf, test, name);
        return eval;
	}
	
	@SuppressWarnings("rawtypes")
	public Classifier loadModel(InstanceList train, String classifierName, String experiment, String outputDirectory) throws Exception{
		
		ClassifierTrainer trainer = null;
		
		if (classifierName.equals("nb"))
			trainer = new NaiveBayesTrainer();
		else if (classifierName.equals("dt"))
			trainer = new C45Trainer();
		else 
			trainer = new MaxEntTrainer();
        
		Classifier classifier = null;
        
		new File(outputDirectory + "/models/").mkdirs();
    	classifier = trainer.train(train);
    	ObjectOutputStream oos =
                new ObjectOutputStream(new FileOutputStream (outputDirectory + "/models/" + experiment + "-" + _size + ".ser.gz"));
            oos.writeObject (classifier);
            oos.close();
        return classifier;
	}
	
	public void classifyMallet(InstanceList test, Classifier classifier, String outputDirectory, String experiment, String nameTrain, String name, boolean twoWay) throws Exception {
		
		
    	Trial trial = new Trial(classifier, test);

        System.out.println("Accuracy: " + trial.getAccuracy());
        System.out.println("F1 for class 'agreement': " + trial.getF1("agreement"));
        System.out.println("F1 for class 'disagreement': " + trial.getF1("disagreement"));
        if (!twoWay) System.out.println("F1 for class 'none': " + trial.getF1("none"));
        
        if (!new File(outputDirectory + "/answers-" + "-" + (twoWay ? "2way-" : "3way-") + nameTrain + "-" + name + ".txt").exists()) {
        	BufferedWriter ganswers = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/answers-" + "-" + (twoWay ? "2way-" : "3way-") + nameTrain + "-" + name + ".txt",true),"UTF-8"));
			ganswers.write("gold_annotation_" + name + " ");
			Iterator<Classification> glabels = trial.iterator();
			 
			while (glabels.hasNext()) {
				Classification label = glabels.next();
				ganswers.write(label.getInstance().getLabeling().getBestLabel() + " ");
			}
			ganswers.write("\n");
			ganswers.close();
			BufferedWriter gresults = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/results-" + "-" + (twoWay ? "2way-" : "3way-") + nameTrain + "-" + name + ".txt",true),"UTF-8"));
			if (twoWay) gresults.write("Experiment,N Train, N Test, F1 Agreement,F1 Disagreement, Accuracy, Avg F1\n");
			else gresults.write("Experiment,N Train, N Test,F1 None,F1 Agreement,F1 Disagreement, Accuracy, Avg F1 Agreement+Disagreement, Avg F1\n");
			gresults.close();
		}
		
        // overall results
        BufferedWriter results = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/results-" + "-" + (twoWay ? "2way-" : "3way-") + nameTrain + "-" + name + ".txt",true),"UTF-8"));
		
		results.write(experiment + "-" + _size + "," + _size + "," + trial.size() + ","+ 
		(!twoWay ? trial.getF1("none") + "," : "") + 
		trial.getF1("agreement") +"," + trial.getF1("disagreement") + "," + trial.getAccuracy() + "," + 
				((trial.getF1("agreement") + trial.getF1("disagreement"))  / 2) + "," + 
				(twoWay ? "" : ((trial.getF1("agreement") + trial.getF1("disagreement") + trial.getF1("none")) / 3)) + "\n");
		results.close();
		
		// for stat significance
		BufferedWriter answers = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/answers-" + "-" + (twoWay ? "2way-" : "3way-") + nameTrain + "-" + name + ".txt",true),"UTF-8"));
        
		answers.write(experiment + "-" + _size + " ");
        
        Iterator<Classification> labels = trial.iterator();
			 
		while (labels.hasNext()) {
			Classification label = labels.next();
			answers.write(label.getLabeling().getBestLabel() + " ");
		}	        
        answers.write("\n");
        answers.close();
	}
	
	public void printResults(String outputDirectory, LogEvaluator eval, String experiment, String name) throws Exception {

		if (!new File(outputDirectory + "/answers.txt").exists()) {
			BufferedWriter answers = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/answers.txt",true),"UTF-8"));
			answers.write("gold_annotation" + name + " ");
			 for (int i = 0; i < eval.getTargetLables().size(); i++) {
	        	LabelsSequence target = ((LabelsAssignment)eval.getTargetLables().get(i).getTarget()).getLabelsSequence();
	        	for (int j = 0; j < target.size(); j++) {
	        		answers.write(target.getLabels(j) + " ");
	        	}
	        }	 
        	answers.write("\n");
        	answers.close();
			BufferedWriter results = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/results.txt",true),"UTF-8"));
			results.write("Experiment,N,F1 None,F1 Agreement,F1 Disagreement,Micro F1 Agreement+Disagreement,Macro F1 Agreement+Disagreement,Accuracy,Agreement+Disagreement Accuracy," +
					"N/N,N/D,N/A," +
					"D/N,D/D,D/A," +
					"A/N,A/D,A/A\n");
			results.close();
		}
		
		BufferedWriter ganswers = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/answers.txt",true),"UTF-8"));
		ganswers.write("gold_annotation_" + name + " ");
		
		/*for (int i = 0; i < eval.getTargetLables().size(); i++) {
			 LabelsSequence target = ((LabelsAssignment)eval.getTargetLables().get(i).getTarget()).getLabelsSequence();
			 for (int j = 0; j < target.size(); j++) {
				 ganswers.write(target.getLabels(j) + " ");
			 }
		}	
		ganswers.write("\n");
		ganswers.close();*/
        
        // overall results
		BufferedWriter results = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/results.txt",true),"UTF-8"));
		TestResults r = eval.getTestResults();
		

		int agreement = 0; int disagreement = 0; int none = 0;
		
		Object [] classes = r.alphabet.toArray();
		if (classes[1].equals("agreement")) agreement = 1;
		else if (classes[1].equals("disagreement")) disagreement = 1;
		else if (classes[1].equals("none")) none = 1;
		
		if (classes[2].equals("agreement")) agreement = 2;
		else if (classes[2].equals("disagreement")) disagreement = 2;
		else if (classes[2].equals("none")) none = 2;
		
		double tp = -1;
		double microf1 = -1;
		double macrof1 = -1;
		
		// agreement + disagreement
		if (r.numClasses == 3) {
			 tp = r.confusion[disagreement][disagreement]+r.confusion[agreement][agreement];
			// none marked as a/d, a marked as d, d marked as a
			double fp = r.confusion[none][agreement]+r.confusion[none][disagreement]+r.confusion[disagreement][agreement]+r.confusion[agreement][disagreement];
			// a marked as n/d, d marked as n/a
			double fn = r.confusion[disagreement][agreement]+r.confusion[disagreement][none]+r.confusion[agreement][disagreement]+r.confusion[agreement][none];
			
			double precision = tp / (tp + fp);
			double recall = tp / (tp + fn);
			 microf1 = 2 * ((precision*recall)/(precision+recall));
			double macroP = (r.precision[agreement]+r.precision[disagreement])/2;
			double macroR = (r.recall[agreement]+r.recall[disagreement])/2;
			 macrof1 = 2 * ((macroP*macroR)/(macroP+macroR));
		}
		
		results.write(experiment + "-" + name + "," + r.maxT + "," + r.f1[none] +" ," + r.f1[agreement] +
				"," + r.f1[disagreement] + "," + microf1 + "," + macrof1 + "," + r.getJointAccuracy() + "," + 
		(tp / (r.trueCounts[agreement] + r.trueCounts[disagreement]) +  ","));
		results.write(r.confusion[none][none] + "," + r.confusion[none][agreement] + "," + r.confusion[none][disagreement] + ",");
		results.write(r.confusion[agreement][none] + "," + r.confusion[agreement][agreement] + "," + r.confusion[agreement][disagreement] + ",");
		results.write(r.confusion[disagreement][none] + "," + r.confusion[disagreement][agreement] + "," + r.confusion[disagreement][disagreement] + "\n");
		results.close();
		
		
		// for stat significance
		BufferedWriter answers = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputDirectory + "/answers.txt",true),"UTF-8"));
        
		answers.write(experiment + "-" + name + " ");
        for (int i = 0; i < eval.getTargetLables().size(); i++) {
        	LabelsSequence target = ((LabelsAssignment)eval.getTargetLables().get(i).getTarget()).getLabelsSequence();
        	LabelsSequence returned = (LabelsSequence)eval.getReturnedLabels().get(i);
        	
        	for (int j = 0; j < target.size(); j++) {
        		//System.out.println(target.getLabels(j)); // + " " + returned.getLabels(j));
        		answers.write(returned.getLabels(j) + " ");
        	}
        }	        
        answers.write("\n");
        answers.close();
	}
	
	public void printAnnotations(String directory, String processingDirectory, LogEvaluator eval) throws Exception {
		
		String [] files = new String[1];
		
		File f = new File(directory);
		if (f.isDirectory()) files = f.list();
		else files[0] = directory;

		for (int i = 0; i < eval.getTargetLables().size(); i++) {
			ArrayList<String> annotations = new ArrayList<String>();
			
        	LabelsSequence returned = (LabelsSequence)eval.getReturnedLabels().get(i);
        	
        	for (int j = 0; j < returned.size(); j++) {
        		annotations.add(returned.getLabels(j).toString());
        	}
        	String file = ProcessBlog.writeBlog(files[i], processingDirectory, annotations);
        	System.out.println(Arrays.toString(ProcessBlog.getAnnotations(file).values().toArray()));
        }	        
       
	}
	
	public void printAnnotations(String model, InstanceList test, String file, String output) throws Exception {
		
		new File(output).mkdirs();

		// Load classifier here 
			ObjectInputStream ois =
	            new ObjectInputStream (new FileInputStream (model));
	        Classifier classifier = (Classifier) ois.readObject();
	        ois.close();

		
		ArrayList<String> annotations = new ArrayList<String>();

		Trial trial = new Trial(classifier, test);
	    
		for (Classification classification : trial) {
			annotations.add(classification.getLabeling().getBestLabel().toString());
		}
        String fileO = ProcessBlog.writeBlog(file, output, annotations);
        System.out.println(Arrays.toString(ProcessBlog.getAnnotations(fileO).values().toArray()));
	}
	
	public ACRF loadClassifier(File serializedFile)
	        throws FileNotFoundException, IOException, ClassNotFoundException {
	        ACRF classifier = (ACRF)FileUtils.readGzippedObject(serializedFile);
	        return classifier;
	}
	
	private static BshInterpreter interpreter = setupInterpreter ();
	
	private static BshInterpreter setupInterpreter () {
        BshInterpreter interpreter = new BshInterpreter (); //CommandOption.getInterpreter();
        try {
          interpreter.eval ("import cc.mallet.base.extract.*");
          interpreter.eval ("import cc.mallet.grmm.inference.*");
          interpreter.eval ("import cc.mallet.grmm.learning.*");
          interpreter.eval ("import cc.mallet.grmm.learning.templates.*");
        } catch (EvalError e) {
          throw new RuntimeException (e);
        }
    
        return interpreter;
	}
	
	private static Inferencer createInferencer (String spec) throws EvalError {
        String cmd;
        if (spec.indexOf ('(') >= 0) {
          // assume it's Java code, and don't screw with it.
          cmd = spec;
        } else {
          cmd = "new "+spec+"()";
        }
    
        // Return whatever the Java code says to
        Object inf = interpreter.eval (cmd);
    
        if (inf instanceof Inferencer)
          return (Inferencer) inf;
    
        else throw new RuntimeException ("Don't know what to do with inferencer "+inf);
      }
	
	public void writeFeatureList(HashSet<String> features, String outputFile) {
		try {
			new File(outputFile).getParentFile().mkdir();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile),"UTF-8"));
			
			for (String feature : features) {
				writer.write(feature + "\n");
			}
			writer.close();
		}	catch (Exception e) {
			e.printStackTrace();
		}
	}

	public HashSet<String> readFeatureList(String inputFile) {
		HashSet<String> features = new HashSet<String>();
		
		try {
			List<String> lines = Utils.readLines(inputFile);
			
			for (String feature : lines) {
				features.add(feature);
			}

		}	catch (Exception e) {
			e.printStackTrace();
		}
		return features;
	}

	
	public void generatePerNodeFeatures(String file, List<agreement.Thread> threads, boolean training, String trainDirectory, String trainingName, boolean unannotated) {
		HashSet<String> pos = null;
		HashSet<String> ngrams = null;
		
		if (training) {
			ngrams = Utilities.ngrams(threads, 5000, 1, 3, _2way);
			writeFeatureList(ngrams,trainDirectory + "/ngram-features.txt");
		
			if (_POS) {
				pos = Utilities.pos(threads, 500, 1, 4, _2way);
				writeFeatureList(pos,trainDirectory + "/pos-features.txt");
			}
		}
		else {
			ngrams = readFeatureList(trainDirectory + "/ngram-features.txt");
			pos = readFeatureList(trainDirectory + "/pos-features.txt");
		}
		for (agreement.Thread thread : threads) {
			generatePerNodeFeatures(file, thread, ngrams, pos, training, true, trainingName, unannotated);
		}
	}
	
	public void generatePerNodeFeatures(String file, agreement.Thread threadT, 
			HashSet<String> ngramList, HashSet<String> posList,
			boolean training, boolean append, String trainingName, boolean unannotated) {
		
		try {

			new File(file).mkdirs();
			BufferedWriter ngram = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/ngram" + trainingName + ".txt", append),"UTF-8"));
			BufferedWriter thread = _thread ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/thread.txt", append),"UTF-8")) : null;
			BufferedWriter sentenceSimilarity =  _sentenceSimilarity ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/ss" + trainingName + ".txt", append),"UTF-8")) : null;
			BufferedWriter question = _question ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/question.txt", append),"UTF-8")) : null;
			//BufferedWriter phraseSimilarity = _phraseSimilarity ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/phraseSS.txt", append),"UTF-8")) : null;
			//BufferedWriter opinion = _opinion ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/opinion.txt", append),"UTF-8")) : null;
			BufferedWriter polarity = _polarity ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/polarity.txt", append),"UTF-8")) : null;
			BufferedWriter pos = _POS ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/POS" + trainingName + ".txt", append),"UTF-8")) : null;
			BufferedWriter entrainment = _entrainment ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/entrainment" + trainingName + ".txt", append),"UTF-8")) : null;
			BufferedWriter liwc = _liwc != null ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/liwc.txt", append),"UTF-8")) : null;
			BufferedWriter sm = _sm != null ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/sm.txt", append),"UTF-8")) : null;
			BufferedWriter label = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/label.txt", append),"UTF-8"));
			BufferedWriter ids = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file + "/ids.txt", append),"UTF-8"));
			
			//HashMap<String,HashSet<String>> uniqueTerms = DataGenerator.featureSelection(threads);
			
			
				List<Arc> arcs = threadT.getArcs();
				
				for (Arc arc : arcs) {

					String l = "none";
					if (arc.getType() == Arc.AGREEMENT) l = "agreement";
					else if (arc.getType() == Arc.DISAGREEMENT) l = "disagreement";
					else if (arc.getType() == Arc.NONE) l = "none";
					else if (arc.getType() == Arc.UNKNOWN && !unannotated) continue;
					
					//if (twoWay && l.equals("none")) continue;
					
					label.write(l);
					
					// a feature id
					ids.write(threadT.getDocID() + ";" + (arc.getSubject() == null ? "null" : arc.getSubject().getId()) + ";" + arc.getTarget().getId() + " ");
					
					//ngram.write(removeStopWords(arc.getTarget().getBody(),false) + " ");
					String tarNGram = arc.getTarget().getBody().replaceAll("\\p{Punct}+", "").toLowerCase();
					String tarNGramF = arc.getTarget().getSentences().size() > 0 ?
							arc.getTarget().getSentences().get(0).replaceAll("\\p{Punct}+", "").toLowerCase() : "";
		        	
					if (!tarNGram.trim().isEmpty()) {
			        	for (String n : ngramList) {
			        		if (tarNGram.matches(".*( |^)" + n.replaceAll("_", " ") + "( |$).*")) ngram.write(n + "_NGRAM ");
			        		if (tarNGramF.matches(".*( |^)" + n.replaceAll("_", " ") + "( |$).*")) ngram.write(n + "_NGRAM_FIRST ");
			        	}
					}		        		
					if (_thread) {
				        if (arc.getTarget().isRoot()) thread.write("ROOT ");
				        if (arc.getSubject() != null && arc.getTarget().getAuthor().equals(arc.getSubject().getAuthor())) thread.write("SAME_AUTHOR ");
				        thread.write("DEPTH_" + arc.getTarget().getDepthBinned() + " ");
				        thread.write("RESPONSE_COUNT_" + arc.getTarget().getNumResponsesBinned() + " ");
					}
					if (_question) {
			        	if (arc.getTarget().hasQuestion(true)) question.write("QUESTION_FIRST ");
			        	if (arc.getTarget().hasNegation(true)) question.write("NEGATION_FIRST ");
			        	if (arc.getTarget().hasAgreement(true)) question.write("AGREE_FIRST ");
			        	if (arc.getTarget().hasDisagreement(true)) question.write("DISAGREE_FIRST ");
			        	if (arc.getTarget().hasQuestion(false)) question.write("QUESTION ");
			        	if (arc.getTarget().hasNegation(false)) question.write("NEGATION ");
			        	if (arc.getTarget().hasAgreement(false)) question.write("AGREE ");
			        	if (arc.getTarget().hasDisagreement(false)) question.write("DISAGREE ");
			        	question.write("SENTENCE_COUNT_" + arc.getTarget().getNumSentencesBinned() + " ");
			        	question.write("CHARACTER_COUNT_" + arc.getTarget().getNumCharsBinned() + " ");
					}
					
					if (_liwc != null) {
						HashMap<String,Double> Qcounts = (_entrainment && arc.getSubject() != null) ?
								_liwc.generateFeatureCounts(arc.getSubject().getBody()) : null;
		        		HashMap<String,Double> Rcounts = _liwc.generateFeatureCounts(arc.getTarget().getBody());
		        		HashMap<String,Double> QcountsF = (_entrainment && arc.getSubject() != null && arc.getSubject().getSentences().size() > 0) ?
		        				_liwc.generateFeatureCounts(arc.getSubject().getSentences().get(0)) : null;
		        		HashMap<String,Double> RcountsF = (arc.getTarget().getSentences() != null && arc.getTarget().getSentences().size() > 0) ? 
		        				_liwc.generateFeatureCounts(arc.getTarget().getSentences().get(0)) : null;
						
						for (String feature : Rcounts.keySet()) {
							if (Rcounts.get(feature) > 0.0) liwc.write("LIWC_" + feature.replaceAll("\\s+", "") + " ");
							if (_entrainment && Qcounts != null) 
								if (Rcounts.get(feature) > 0 && Qcounts.get(feature) > 0) entrainment.write("LIWC_ENTRAINMENT_" + feature.replaceAll("\\s+", "") + " ");
						}
						
						if (RcountsF != null) {
							for (String feature : RcountsF.keySet()) {
								if (RcountsF.get(feature) > 0) liwc.write("LIWC_FIRST_" + feature.replaceAll("\\s+", "") + " ");
								if (_entrainment && QcountsF != null) 
									if (RcountsF.get(feature) > 0 && QcountsF.get(feature) > 0) entrainment.write("LIWC_ENTRAINMENT_FIRST_" + feature.replaceAll("\\s+", "") + " ");
							}
						}
					}
					
					if (_sm != null) {
						Hashtable<String,Double> Rsmcounts = _sm.compute(arc.getTarget().getBody().split("\\s+"));
						Hashtable<String,Double> Qsmcounts = _entrainment && arc.getSubject() != null ? _sm.compute(arc.getSubject().getBody().split("\\s+")) : null;
						Hashtable<String,Double> RsmcountsF = arc.getTarget().getSentences().size() > 0 ?
								_sm.compute(arc.getTarget().getSentences().get(0).split("\\s+")) : null;
						Hashtable<String,Double> QsmcountsF = _entrainment && arc.getSubject() != null && arc.getSubject().getSentences().size() > 0 ? _sm.compute(arc.getSubject().getSentences().get(0).split("\\s+")) : null;
						
						for (String feature : Rsmcounts.keySet()) {
							if (feature.equals("punctuation_count") || feature.equals("word_count") || feature.equals("average_word_length")) continue;
							if (Rsmcounts.get(feature) > 0) sm.write("SM_" + feature.replaceAll("\\s+", "") + " ");
							if (_entrainment && Qsmcounts != null && Rsmcounts.get(feature) > 0 && Qsmcounts.get(feature) > 0) entrainment.write("SM_ENTRAINMENT_" + feature.replaceAll("\\s+", "") + " ");
						}
						
						// first sentence
						if (arc.getTarget().getSentences().size() > 0) {
							
							for (String feature : RsmcountsF.keySet()) {
								if (feature.equals("punctuation_count") || feature.equals("word_count") || feature.equals("average_word_length")) continue;
								if (RsmcountsF.get(feature) > 0) sm.write("SM_FIRST_" + feature.replaceAll("\\s+", "") + " ");
								if (_entrainment && QsmcountsF != null && RsmcountsF.get(feature) > 0 && QsmcountsF.get(feature) > 0) entrainment.write("SM_ENTRAINMENT_FIRST_" + feature.replaceAll("\\s+", "") + " ");
							}
						}
					}
					
					if (_sentenceSimilarity) {
						sentenceSimilarity.write(arc.getFirstSimilar(.6) == 1 ? "SIMILAR_FIRST " : "");
						sentenceSimilarity.write(arc.getMaxSentenceSimilarity(.6) == 1 ? "SIMILAR " : "");
						sentenceSimilarity.write(arc.getMeanSentenceSimilarity(.6) == 1 ? "HIGHLY_SIMILAR " : "");
					
		        		String uniqueWords = arc.getPhraseSimilarityUniqueWords(.6, false);
		        		String uniqueWordsF = arc.getPhraseSimilarityUniqueWords(.6,true);
		        		
		        		if (!uniqueWords.isEmpty()) {
			        		/*HashMap<String,Integer> liwcCounts = _liwc.generateFeatureCounts(uniqueWords);
					        
			        		for (String feature : liwcCounts.keySet()) {
								if (liwcCounts.get(feature) > 0) phraseSimilarity.write("LIWC_UNIQUE" + feature.replaceAll("\\s+", "") + " ");
							}*/
		        			boolean hasNegation = false;
		        			boolean hasNegationFirst = false;
		        			
		        			for (String n : ngramList) {
					        	if (uniqueWords.matches(".*(^| |_)" + n + "($| |_).*")) {
					        		sentenceSimilarity.write(n + "_UNIQUE ");
					        	}
					        	else if (uniqueWords.matches(".*(^| |_)" + Node.NEGATION + "($| |_).*")) {
					        		hasNegation = true;
					        	}
					        	
					        	if (!uniqueWordsF.isEmpty()) {
						        	if (uniqueWordsF.matches(".*(^| |_)" + n + "($| |_).*")) {
						        		sentenceSimilarity.write(n + "_UNIQUE_FIRST ");
						        	}
						        	else if (uniqueWordsF.matches(".*(^| |_)" + Node.NEGATION + "($| |_).*")) {
						        		hasNegationFirst = true;
						        	}
					        	}
				        	}
			        		
		        			if (hasNegation) sentenceSimilarity.write("NEGATION_PHRASE_SS ");
		        			if (hasNegationFirst) sentenceSimilarity.write("NEGATION_PHRASE_SS_FIRST ");
			        		//phraseSimilarity.write(uniqueWords.replaceAll("\\s+", "_UNIQUE ") + (uniqueWords.length() > 0 ? "_UNIQUE " : " "));
		        		}
		        		
		        		
				        int [] counts = arc.getPhraseSimilarity(.6, false);
				        
				        if (counts[0] > 0 || counts[1] > 0 || counts[2] > 0) //phraseSimilarity.write("NO_SIMILAR_PHRASE ");
				        	sentenceSimilarity.write("SIMILAR_PHRASE ");
				        if (counts[0] > 0) sentenceSimilarity.write("PHRASE_SAME_" + (counts[0] == 1 ? 1 : 2) + " ");
				        if (counts[1] > 0) sentenceSimilarity.write("PHRASE_DIFF_" + (counts[1] == 1 ? 1 : 2) + " ");
				       //  if (counts[2] > 0) phraseSimilarity.write("PHRASE_NOPOLARITY " + counts[2] + " ");
				        if (counts[0] > counts[1] && counts[0] > counts[2]) sentenceSimilarity.write("PHRASE_SAME ");
				        else if (counts[1] > counts[0] && counts[1] > counts[2]) sentenceSimilarity.write("PHRASE_DIFF ");
				       // else if (counts[2] > counts[0] && counts[2] > counts[1]) phraseSimilarity.write("PHRASE_MISSING_POLARITY ");
				        
				        int [] fCounts = arc.getPhraseSimilarity(.6, true);
				        
				        if (fCounts[0] > 0 || fCounts[1] > 0 || fCounts[2] > 0) //phraseSimilarity.write("NO_SIMILAR_PHRASE_FIRST ");
				        	sentenceSimilarity.write("SIMILAR_PHRASE_FIRST ");
				        if (fCounts[0] > 0) 
				        	sentenceSimilarity.write("PHRASE_SAME_FIRST" + (fCounts[0] == 1 ? 1 : 2) + " ");
				        if (fCounts[1] > 0) 
				        	sentenceSimilarity.write("PHRASE_DIFF_FIRST" + (fCounts[0] == 1 ? 1 : 2) + " ");
				       //  if (counts[2] > 0) phraseSimilarity.write("PHRASE_NOPOLARITY " + counts[2] + " ");
				        if (fCounts[0] > fCounts[1] && fCounts[0] > fCounts[2]) sentenceSimilarity.write("PHRASE_SAME_FIRST ");
				        else if (fCounts[1] > fCounts[0] && fCounts[1] > fCounts[2]) sentenceSimilarity.write("PHRASE_DIFF_FIRST ");
				       // else if (counts[2] > counts[0] && counts[2] > counts[1]) phraseSimilarity.write("PHRASE_MISSING_POLARITY ");
		        	}
		        	if (_polarity) {
			        	double positive = arc.getTarget().getPolarityCount(false, true, false);
			        	double negative = arc.getTarget().getPolarityCount(false, false, false);
			        	if (positive > 0 && negative > 0 && positive > negative) polarity.write("POSITIVE ");
			        	else if (positive > 0 && negative > 0 && negative > positive) polarity.write("NEGATIVE ");
			        	else if (arc.getTarget().hasSentiment()) polarity.write("OPINION ");
			        	else polarity.write("NO_OPINION ");
			        	
			        	positive = arc.getTarget().getPolarityCount(false, true, true);
			        	negative = arc.getTarget().getPolarityCount(false, false, true);
			        	if (positive > 0 && negative > 0 && positive > negative) polarity.write("POSITIVE_FIRST ");
			        	else if (positive > 0 && negative > 0 && negative > positive) polarity.write("NEGATIVE_FIRST ");
			        	else if (arc.getTarget().hasSentiment()) polarity.write("OPINION_FIRST ");
			        	else polarity.write("NO_OPINION_FIRST ");
			        	
			        	//List<String> tarSentiment = arc.getTarget().getSubjectiveWords(false);
				        //List<String> tarSentimentF = arc.getTarget().getSubjectiveWords(true);
			        	List<String> posTarPos = arc.getTarget().getPolarityWords(true,false);
			        	List<String> posTarNeg = arc.getTarget().getPolarityWords(false,false);
			        	List<String> posTarPosF = arc.getTarget().getPolarityWords(true,true);
			        	List<String> posTarNegF = arc.getTarget().getPolarityWords(false,true);
			        	
			        	HashSet<String> phrases = new HashSet<String>();
			        	
			        	String polarPhrase = null;
			        	
			        	for (String phrase : posTarPos)
			        		if (!(polarPhrase = polarityMatch(phrase,true)).isEmpty()) phrases.add(polarPhrase);
			        	for (String phrase : posTarNeg)
			        		if (!(polarPhrase = polarityMatch(phrase,false)).isEmpty()) phrases.add(polarPhrase);
			        	for (String phrase : posTarPosF) {
			        		if (!(polarPhrase = polarityMatch(phrase,true)).isEmpty()) {
			        			phrases.add(polarPhrase.replaceAll("_", "_FIRST"));
			        		}
			        	}
			        	for (String phrase : posTarNegF) {
			        		if (!(polarPhrase = polarityMatch(phrase,false)).isEmpty()) {
			        			phrases.add(polarPhrase.replaceAll("_", "_FIRST"));
			        		}
			        	}
			        	
			        	for (Object phrase : phrases.toArray())
			        		polarity.write(phrase + " ");
			        }
		        	
		        	if (_POS) {
			        	String rPos = arc.getTarget().getPOS(false).replaceAll("\\p{Punct}+","").replaceAll("\\s+", "_").toLowerCase();
			        	String rPosF = arc.getTarget().getPOS(true).replaceAll("\\p{Punct}+","").replaceAll("\\s+", "_").toLowerCase();
			        	String qPos = _entrainment && arc.getSubject() != null ? 
			        			arc.getSubject().getPOS(false).replaceAll("\\p{Punct}+","").replaceAll("\\s+", "_").toLowerCase() : "";
					    String qPosF = _entrainment && arc.getSubject() != null ? 
					        			arc.getSubject().getPOS(true).replaceAll("\\p{Punct}+","").replaceAll("\\s+", "_").toLowerCase() : "";
					        			
			        	for (String p : posList) {
			        		if (rPos.indexOf(p) >= 0) pos.write(p + "_POS ");
			        		if (rPosF.indexOf(p) >= 0) pos.write(p + "_POS_FIRST ");
			        		if (qPos.indexOf(p) >= 0 && rPos.indexOf(p) >= 0) entrainment.write(p + "_POS_ENTRAINMENT ");
			        		if (qPosF.indexOf(p) >= 0 && rPosF.indexOf(p) >= 0) entrainment.write(p + "_POS_ENTRAINMENT_FIRST ");
			        	}
			        	pos.write(" ");
		        	}
		        	
		        	if (_entrainment && arc.getSubject() != null) {
		        		
		        		// 1. POS patterns (done above)
		        		/*String qPos = arc.getTarget().getPOS(false).replaceAll("\\p{Punct}+","").replaceAll("\\s+", "_").toLowerCase();
		        		String rPos = arc.getSubject().getPOS(false) == null ? "" : arc.getSubject().getPOS(false).replaceAll("\\p{Punct}+","").replaceAll("\\s+", "_").toLowerCase();
		        		String qPosF = arc.getTarget().getPOS(true).replaceAll("\\p{Punct}+","").replaceAll("\\s+", "_").toLowerCase();
		        		String rPosF = arc.getSubject().getPOS(true) == null ? "" : arc.getSubject().getPOS(false).replaceAll("\\p{Punct}+","").replaceAll("\\s+", "_").toLowerCase();

			        	
			        	for (String p : posList) {
			        		if (qPos.indexOf(p) >= 0 && rPos.indexOf(p) >= 0) entrainment.write(p + "_POS_ENTRAINMENT ");
			        		if (qPosF.indexOf(p) >= 0 && rPosF.indexOf(p) >= 0) entrainment.write(p + "_POS_ENTRAINMENT_FIRST ");
			        	}*/
			        	
		        		// 2. LIWC (between the two - crf?)
		        		/*HashMap<String,Integer> Rcounts = _liwc.generateFeatureCounts(arc.getTarget().getBody());
		        		HashMap<String,Integer> Qcounts = _liwc.generateFeatureCounts(arc.getSubject().getBody());
		        		HashMap<String,Integer> RcountsF = _liwc.generateFeatureCounts(arc.getTarget().getSentences().get(0));
		        		HashMap<String,Integer> QcountsF = _liwc.generateFeatureCounts(arc.getSubject().getSentences().get(0));
						
						for (String feature : Rcounts.keySet()) {
							if (Rcounts.get(feature) > 0 && Qcounts.get(feature) > 0) entrainment.write("LIWC_ENTRAINMENT_" + feature.replaceAll("\\s+", "") + " ");
						}
						
						for (String feature : RcountsF.keySet()) {
							if (RcountsF.get(feature) > 0 && QcountsF.get(feature) > 0) entrainment.write("LIWC_ENTRAINMENT_FIRST_" + feature.replaceAll("\\s+", "") + " ");
						}*/
		        		
		        		// 3. SM (between the two - crf?)
						/*Hashtable<String,Double> Rsmcounts = _sm.compute(arc.getTarget().getBody().split("\\s+"));
						Hashtable<String,Double> Qsmcounts = _sm.compute(arc.getSubject().getBody().split("\\s+"));
						
						for (String feature : Rsmcounts.keySet()) {
							if (feature.equals("punctuation_count") || feature.equals("word_count") || feature.equals("average_word_length")) continue;
							if (Rsmcounts.get(feature) > 0 && Qsmcounts.get(feature) > 0) entrainment.write("SM_ENTRAINMENT_" + feature.replaceAll("\\s+", "") + " ");
						}
						
						Hashtable<String,Double> RsmcountsF = _sm.compute(arc.getTarget().getSentences().get(0).split("\\s+"));
						Hashtable<String,Double> QsmcountsF = _sm.compute(arc.getSubject().getSentences().get(0).split("\\s+"));
						
						for (String feature : RsmcountsF.keySet()) {
							if (feature.equals("punctuation_count") || feature.equals("word_count") || feature.equals("average_word_length")) continue;
							if (RsmcountsF.get(feature) > 0 && QsmcountsF.get(feature) > 0) entrainment.write("SM_ENTRAINMENT_FIRST_" + feature.replaceAll("\\s+", "") + " ");
						}*/
						
		        		// 4. SS (done above)
		        	}
		        	
					ngram.write("\n");
					if (_thread) thread.write("\n");
					if (_sentenceSimilarity) sentenceSimilarity.write("\n");
					if (_question) question.write("\n");
					//if (_phraseSimilarity) phraseSimilarity.write("\n");
					//if (_opinion) opinion.write("\n");
					if (_polarity) polarity.write("\n");
					if (_POS) pos.write("\n");
					if (_entrainment) entrainment.write("\n");
					label.write("\n");
					ids.write("\n");
					if (_liwc != null) liwc.write("\n");
					if (_sm != null) sm.write("\n");
				}
				
			// end of chain
			ngram.write("\n");
			if (_thread) thread.write("\n");
			if (_sentenceSimilarity) sentenceSimilarity.write("\n");
			if (_question) question.write("\n");
			//if (_phraseSimilarity) phraseSimilarity.write("\n");
			//if (_opinion) opinion.write("\n");
			if (_polarity) polarity.write("\n");
			if (_POS) pos.write("\n");
			if (_entrainment) entrainment.write("\n");
			label.write("\n");
			ids.write("\n");
			if (_liwc != null) liwc.write("\n");
			if (_sm != null) sm.write("\n");
				
			
			ngram.close();
			if (_thread) thread.close();
			if (_sentenceSimilarity) sentenceSimilarity.close();
			if (_question) question.close();
			//if (_phraseSimilarity) phraseSimilarity.close();
			//if (_opinion) opinion.close();
			if (_polarity) polarity.close();
			if (_POS) pos.close();
			if (_liwc != null) liwc.close();
			if (_sm != null) sm.close();
			if (_entrainment) entrainment.close();
			label.close();
			ids.close();
		} catch (Exception e) {
			System.err.println(file);
			e.printStackTrace();
		}
		//return load(file + "/" + name + ".txt", pipe);
	}
		
	public String polarityMatch(String phrase, boolean positive) {
		
		String [] words = phrase.toLowerCase().replaceAll("\\p{Punct}+", "").split("\\s+");
		
		double score = 0;
		boolean noScore = true;
		
		String polarWords = "";
		
		for (String word : words) {
			if (_DAL.hasWord(word)) {
				score += _DAL.get(word).getPleasantness();
				polarWords += word + " ";
				noScore = false;
			}
		}
		
		score = score / words.length;
		
		if (noScore) return "";
		
		if (positive && score > (1.85 + .7)) return polarWords.replaceAll(" ", "_+ ");
		if (!positive && score < (1.85 - .7)) return polarWords.replaceAll(" ", "_- ");
		return "";
	}
	/**
	 * Remove stopwords and single instances of punctuation
	 * @param sentence
	 * @return
	 */
	/*public String removeStopWords(String sentence, boolean opinion) {
		String newSentence = "";
		String [] words = sentence.split("\\s+");
		
		for (String word : words) {
						
			if (opinion && _DAL.hasWord(word.toLowerCase()) && 
					(_DAL.get(word.toLowerCase()).getAEscore() > SentenceBuilder.NEUTRAL_THRESHOLD
					|| _DAL.get(word.toLowerCase()).getAEscore() < -SentenceBuilder.NEUTRAL_THRESHOLD)) 
			{}
			else if (_stopWords.contains(word)) continue;
			else if (_stopWords.contains(word.toLowerCase())) continue;
			else if (word.matches("\\p{Punct}")) continue;
			else if (word.matches("['\"`]+")) continue;
			newSentence += word + " ";
		}
		return newSentence.trim();
	}*/
	
	/*private String getNGrams(List<String> phrases) {
		
		String ngrams = "";
		
		for (String phrase : phrases) {
			ngrams += phrase.replaceAll("\\p{Punct}+", "").replaceAll("\\s+", "_") + " ";
		}
		return ngrams;
	}*/
	
	public String combineFeatures(String file, int size, boolean balanced, boolean twoWay, String trainingName) {
		
		File temp = null;
		
		try {

			List<String> ngram = _ngram ? Utils.readLines(file + "/ngram" + trainingName + ".txt") : null;
			List<String> thread = _thread ? Utils.readLines(file + "/thread.txt") : null;
			List<String> sentenceSimilarity = _sentenceSimilarity ? Utils.readLines(file + "/ss" + trainingName + ".txt") : null;
			List<String> question = _question ? Utils.readLines(file + "/question.txt") : null;
			//List<String> phraseSimilarity = Utils.readLines(file + "/phraseSS.txt");
			//List<String> opinion = Utils.readLines(file + "/opinion.txt");
			List<String> polarity = _polarity ? Utils.readLines(file + "/polarity.txt") : null;
			List<String> pos = _POS ? Utils.readLines(file + "/POS" + trainingName + ".txt") : null;
			List<String> liwc = _liwc != null ? Utils.readLines(file + "/liwc.txt") : null;
			List<String> sm = _sm != null ? Utils.readLines(file + "/sm.txt") : null;
			List<String> label = Utils.readLines(file + "/label.txt");
			List<String> ids = Utils.readLines(file + "/ids.txt");
			List<String> entrainment = _entrainment ? Utils.readLines(file + "/entrainment" + trainingName + ".txt") : null;
			
			temp = File.createTempFile(file + "tmp-", ".tmp");
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(temp),"UTF-8"));
			
			int none = 0;
			int agreement = 0;
			int disagreement = 0;
			double avgDepth = 0;
			int maxDepth = 1;			
			int count = 0;
			
			int min = size == -1 ? label.size() : size;
			
			if (balanced) {
				min = getMin(label, size == -1 ? label.size() : size, twoWay);
				System.out.println("Balancing to " + min + " posts per class.");
			}
			//List<Integer> random = this.random(ids.size());
			
			for (int i = 0; i < ids.size(); i++) {
				
				int index = i; //random.get(i);
				
				if (balanced) {
					if (!twoWay && label.get(index).equals("none") && none >= min) continue;
					if (label.get(index).equals("agreement") && agreement >= min) continue;
					if (label.get(index).equals("disagreement") && disagreement >= min) continue;
				}
				
				// end of chain
				if (!_mallet && ids.get(index).isEmpty()) {
					avgDepth += maxDepth;
					maxDepth = 1;
					count++;
					out.write("\n");
					continue;
				}
				else if (_mallet) {
					if (!twoWay && none >= min && agreement >= min && disagreement >= min) break;
					if (twoWay && agreement >= min && disagreement >= min) break;
					
					if (ids.get(index).isEmpty()) {
						count++;
						continue;
					}
				}
				
				if (label.get(index).equals("none") && twoWay) continue;
				out.write(label.get(index) + " ");
				
				if (!_mallet) {
					 out.write("---- ");
				}

				if (label.get(index).equals("agreement")) agreement++;
				else if (label.get(index).equals("disagreement")) disagreement++;
				else if (label.get(index).equals("none")) none++;
				
				
				// a feature id -- only needed for CRF #NAME
				//if (!_mallet)
					out.write(ids.get(index));
				
				if (_ngram) {
					out.write(_first ? ngram.get(index) : ngram.get(index).replaceAll("\\p{Graph}+_FIRST\\p{Graph}*( |$)", ""));
					//out.write(ngram.get(index));
				}
				if (_thread) {
					out.write(thread.get(index));
				}
				
				if (!_mallet) {
					int depth = Integer.valueOf(thread.get(index).substring(thread.get(index).indexOf("DEPTH_")+6, thread.get(index).indexOf("DEPTH_")+7));
					if (depth > maxDepth) maxDepth = depth;
				}
				
				if (_sentenceSimilarity) {
					out.write(_first ? sentenceSimilarity.get(index) : sentenceSimilarity.get(index).replaceAll("\\p{Graph}+_FIRST\\p{Graph}*( |$)",""));
				}
		        if (_question) {
		        	//out.write(_first ? question.get(index) : question.get(index).replaceAll("\\p{Graph}+_FIRST\\p{Graph}*( |$)",""));
		        	out.write(question.get(index));
		        }

		        /*if (_phraseSimilarity) {
		        	out.write(phraseSimilarity.get(index));
		        }
		        /*if (_opinion) {
		        	out.write(opinion.get(index));
			    }*/
		        if (_polarity) {
		        	//out.write(_first ? polarity.get(index) : polarity.get(index).replaceAll("\\p{Graph}+_FIRST\\p{Graph}*( |$)",""));
		        	out.write(polarity.get(index));
			    }
		        
		        if (_POS) {
		        	out.write(_first ? pos.get(index): pos.get(index).replaceAll("\\p{Graph}+_FIRST\\p{Graph}*( |$)",""));
		        }
		        
		        if (_liwc != null) {
		        	out.write(_first ? liwc.get(index) : liwc.get(index).replaceAll("\\p{Graph}+_FIRST\\p{Graph}*( |$)",""));
		        }
		        if (_sm != null) {
		        	out.write(_first ? sm.get(index) : sm.get(index).replaceAll("\\p{Graph}+_FIRST\\p{Graph}*( |$)",""));
		        }
		        
		        if (_entrainment) {
		        	out.write(_first ? entrainment.get(index) : entrainment.get(index).replaceAll("\\p{Graph}+_FIRST\\p{Graph}*( |$)",""));
		        }
				
		        /*if (_mallet) {
		        	out.write(label.get(index) + "\n");
		        	if (label.get(index).equals("agreement")) agreement++;
					else if (label.get(index).equals("disagreement")) disagreement++;
					else if (label.get(index).equals("none")) none++;
		        }*/
				out.write("\n");
			}
			out.close();
			System.out.println("STATS:");
			System.out.println("#, Agreement, Disagreement, None" + (!_mallet ? ", Average Depth" : ""));
			System.out.println(count + " " + agreement + " " + disagreement + " " + none + (!_mallet ? " " + avgDepth/count : ""));
			
		} catch (Exception e) {
			System.err.println(file);
			e.printStackTrace();
		}
		return temp.toString();
	}
	
	/*private int getMin(List<String> labels, int size, boolean twoWay) {
		int agreement = 0;
		int disagreement = 0;
		int none = 0;
		
		for (int i = 0; i < labels.size() && i < size; i++) {
			if (labels.get(i).equals("agreement")) agreement++;
			else if (labels.get(i).equals("disagreement")) disagreement++;
			else if (!twoWay && labels.get(i).equals("none")) none++;
		}
		
		int min = agreement;
		if (!twoWay && none < agreement) min = none;
		if (disagreement < agreement) min = disagreement;
		return min;
	}*/
	
	private int getMin(List<String> labels, int size, boolean twoWay) {
		int agreement = 0;
		int disagreement = 0;
		int none = 0;
		
		for (int i = 0; i < labels.size(); i++) {
			if (labels.get(i).equals("agreement")) agreement++;
			else if (labels.get(i).equals("disagreement")) disagreement++;
			else if (!twoWay && labels.get(i).equals("none")) none++;
		}
		
		int min = agreement;
		if (!twoWay && none < agreement) min = none;
		if (disagreement < agreement) min = disagreement;
		if (size < 0) return min;
		if (min > size) return size;
		return min;
	}
	
	public Pipe createPipe(boolean crf) {
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

        // Read data from File objects
        //pipeList.add(new Input2CharSequence("UTF-8"));
        
        //Pattern tokenPattern = Pattern.compile("\\S+");
        
        // Tokenize raw strings
        //pipeList.add(new CharSequence2TokenSequence(tokenPattern));

       // pipeList.add(new TokenSequenceRemoveStopwords(false, false));

        // Rather than storing tokens as strings, convert 
        //  them to integers by looking them up in an alphabet.
        if (crf) {
        	
        	//SimpleTagger.SimpleTaggerSentence2FeatureVectorSequence pipe = new SimpleTagger.SimpleTaggerSentence2FeatureVectorSequence();
            pipeList.add(new Input2CharSequence("UTF-8"));
            Pattern tokenPattern = Pattern.compile("[\\p{L}\\p{N}_]+");
            pipeList.add(new CharSequence2TokenSequence(tokenPattern));
            
         // Rather than storing tokens as strings, convert 
            //  them to integers by looking them up in an alphabet.
            pipeList.add(new TokenSequence2FeatureSequence());

            // Do the same thing for the "target" field: 
            //  convert a class label string to a Label object,
            //  which has an index in a Label alphabet.
            pipeList.add(new Target2Label());

            // Now convert the sequence of features to a sparse vector,
            //  mapping feature IDs to counts.
            pipeList.add(new FeatureSequence2FeatureVector());
            //pipeList.add(new Target2Label());
        	//SimpleTaggerSentence2TokenSequence st = new SimpleTaggerSentence2TokenSequence();
        	//st.setTargetProcessing(true);
        	//pipeList.add(st);
            
            // Print out the features and the label
            // pipeList.add(new PrintInputAndTarget());
        	//pipeList.add(pipe);
        }
        else {
        	GenericAcrfData2TokenSequence pipe = new GenericAcrfData2TokenSequence();
        	//pipe.setFeaturesIncludeToken(true);
            //pipe.setIncludeTokenText(true);
        	pipeList.add(pipe);
        	pipeList.add(new TokenSequence2FeatureVectorSequence (true, true));
        }
        
     // Do the same thing for the "target" field: 
        //  convert a class label string to a Label object,
        //  which has an index in a Label alphabet.
       // pipeList.add(new Target2Label());
        
     // Print out the features and the label
     //   pipeList.add(new PrintInputAndTarget());
		
		Pipe pipe = new SerialPipes(pipeList);
		return pipe;
	}
	
	public InstanceList load(String file, Pipe pipe, int size, boolean balanced, boolean twoWay, String trainingName) {

		file = combineFeatures(file, size, balanced, twoWay, trainingName);
		//FileIterator iterator =
	     //       new FileIterator(file);
		
		InstanceList instancesList = new InstanceList(pipe);

        // Now process each instance provided by the iterator.
		try {
			if (_mallet)
				instancesList.addThruPipe(new CsvIterator(new FileReader(file),
                        "(\\S+)\\s+(\\S+)\\s+(.*)",
                        3, 1, 2)  // (data, target, name) field indices
				);
			else	
				instancesList.addThruPipe(new LineGroupIterator(new FileReader(file),Pattern.compile("^\\s*$"), true));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// remove temp file!
		new File(file).delete();
		//if (size == -1) return instancesList;
		return instancesList; //.subList(0,size);
	}
	
	public void generateNGrams(List<String> text, List<String> label, int size) {
		
		HashMap<String,HashMap<String,Integer>> ngrams = new HashMap<String,HashMap<String,Integer>>();
		ngrams.put("agreement",new HashMap<String,Integer>());
		ngrams.put("disagreement",new HashMap<String,Integer>());
		ngrams.put("none",new HashMap<String,Integer>());
		
		for (int i = 0; i < text.size(); i++) {
			
			for (String word : text.get(i).split("\\s+")) {
				
				if (ngrams.get(label.get(i)).containsKey(word)) ngrams.get(label.get(i)).put(word, ngrams.get(label.get(i)).get(word));
				else ngrams.get(label.get(i)).put(word,1);
			}
		}
	}
	
	public void delete(File f) throws IOException {
		  if (f.isDirectory()) {
		    for (File c : f.listFiles())
		      delete(c);
		  }
		  if (!f.delete())
		    throw new FileNotFoundException("Failed to delete file: " + f);
	}
	
	/*private List<Integer> random(int size) {
		List<Integer> numbers = new ArrayList<Integer>();
		
		for (int index = 0; index < size; index++) {
			numbers.add(index);
		}
		Collections.shuffle(numbers);
		return numbers;
	}*/
	
	/* Regular CRF - Not Used
	 * if (_crf) {
	        CRF crf = new CRF(train.getPipe(), null);
	        crf.addFullyConnectedStatesForLabels();
	        //crf.addStatesForLabelsConnectedAsIn(train);
	        //crf.addStatesForThreeQuarterLabelsConnectedAsIn(trainingInstances);
	        //crf.addStartState();
	        
	        CRFOptimizableByLabelLikelihood optLabel =
	                new CRFOptimizableByLabelLikelihood(crf, train);
	        optLabel.setGaussianPriorVariance(10.0);
	        
	        CRFTrainerByLabelLikelihood trainer = 
	                new CRFTrainerByLabelLikelihood(crf);
	        trainer.setGaussianPriorVariance(10.0);
	
	        trainer.train(train,numIterations);
	        PerClassAccuracyEvaluator eval = new PerClassAccuracyEvaluator(test, name);
	        
	        trainer.addEvaluator(eval);
	        eval.evaluate(trainer);
	        //classifier.classify(crf, test);
		}
	 */
}