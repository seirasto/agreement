package agreement.classifier;

import java.io.*;
import java.util.*;

//import edu.columbia.opinion.io.*;
//import edu.columbia.opinion.module.OpinionRunner;
import edu.columbia.utilities.liwc.LIWC;

import org.w3c.dom.*;

//import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import processing.*;
import social_media.Features;
import agreement.Arc;
import agreement.Node;
import agreement.Thread;
import agreement.io.ProcessBlog;
import blog.XMLUtilities;

public class AgreementClassifier {

    /*public static final String RES_ENTRIES = 
        "res/entries.txt";
    public static final String RES_TRAINING_FILE = 
        "res/Spencer.csv"; //train2.csv";*/
    public static final String RES_INSTANCES =
        "res/instances.ser";
    public static final String RES_CLASSIFIER =
        "res/classifier.ser";

    protected String _dataDirectory;
    protected boolean _livejournal = false;
    protected boolean _wikipedia = true;
    
    protected boolean _thread = false;
    protected boolean _POS = false;
    //protected boolean _balanced = false;
    protected boolean _opinion = false;
    protected boolean _polarity = false;
    protected boolean _question = false;
    protected boolean _sentenceLength = false;
    protected boolean _ngram = false;
    protected boolean _sentenceSimilarity = false;
    protected boolean _phraseSimilarity = false;
    protected boolean _2way = false;
    protected Features _sm = null;
    protected LIWC _liwc = null;
    protected boolean _entrainment = false;
    
    public List<Thread> loadDirectChain(boolean newClassifier, String dataDirectory, String [] files, String processingDirectory, String outputDirectory, String modelDirectory, int size,
    		boolean runOpinion, boolean runPolarity, boolean runPOS, boolean runSS, boolean runPhraseSS, String annotationDirectory, String annotationOutputDirectory, boolean unannotated) throws Exception {
        
		System.out.println("[AgreementClassifier] GENERATE CHAINS");
    	DataGenerator dataGenerator = new DataGenerator(dataDirectory,false,true);
    	HashMap<String,HashMap<String,Arc>> annotatedArcs = null;
    	
    	double numPosts = 0;
    	double numSentences = 0;
    	
    	if (annotationDirectory != null) {
	    	annotatedArcs = load(newClassifier, annotationDirectory, annotationOutputDirectory, modelDirectory, false);
			
    	}    	
    	List<Thread> threads = new ArrayList<Thread>();
    	
    	int i = 0;
    	double depth = 0;
    	
		for (String docId : files) {
        	
			/*if (i < 55 || i > 60) {
				i++;
				continue;
			}*/
			if (size != -1 && i >= size) break;
			
			try {
			
	        	System.out.println((i+1) + ". " + docId);
	        	
	    		try {
		    		Thread thread = loadDirectChainThread(dataGenerator, annotationDirectory != null ? annotatedArcs.get("/proj/nlp/fluke/SCIL/corpora/wikipedia/WD_batch2_year3LJ/" + docId) : null, 
		    				newClassifier, dataDirectory, docId, processingDirectory, outputDirectory, modelDirectory, size,
		    				runOpinion, runPolarity, runPOS, runSS, runPhraseSS, unannotated);
	    		
		    		if (thread != null && ((thread.numArcs() > 1 && thread.getDepth() >= 2) || unannotated)) { // || dataDirectory.indexOf("create_debate") == -1)) {
		    			threads.add(thread);
		    			numSentences += thread.averageSentenceCount();
		    			numPosts += thread.numNodes();
		    			depth += thread.getDepth();
		    			i++;
		    		}
		    		else {
		    			//if (thread != null) 
		  		    	//	System.out.println("Skipping: " + docId + ", depth: " + thread.getDepth() + ", arcs:" + thread.numArcs());
		    			//else System.out.println("Skipping: " + docId);
		    		}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			continue;
	    		}
			} catch (Exception e) {
				System.err.println("Error Processing " + docId + ". skipping...");
				e.printStackTrace();
				continue;
			}
        }
		System.out.println("# Posts & Average # Posts & Average # Sentences & Avg Depth");
		System.out.println(numPosts + " & " + (numPosts/threads.size()) + " & " + (numSentences/threads.size()) + " & " + (depth/threads.size()));
        return threads;

    }
    
    public Thread loadDirectChainThread(DataGenerator dataGenerator, HashMap<String,Arc> annotations, boolean newClassifier, String dataDirectory, String docId, String processingDirectory, String outputDirectory, String modelDirectory, int size,
    		boolean runOpinion, boolean runPolarity, boolean runPOS, boolean runSS, boolean runPhraseSS, boolean unannotated) throws Exception {
 			
			try {
		    	
	        	String fullProcessingPath = processingDirectory + "/processed-" + new File(docId).getName() + "/" + new File(docId).getName();
	        	
	    		processFile(dataGenerator, dataDirectory + "/" + docId, processingDirectory, modelDirectory, runOpinion, runPolarity, runSS, runPhraseSS, unannotated);
	    		
	    		List<String> ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
	    		// skip... file too long
	    		if (ids.size() > 500 && !unannotated) return null; // && dataDirectory.indexOf("create_debate") >= 0) return null; 
	    		if (!runPolarity && !runPhraseSS && processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op").size() == 0) return null; 
	    				
	    		List <String> opinion = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.sbj");
	    		List <String> polarity = (runPolarity || runPhraseSS) ? processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op") : null;
	    		List <String> pos = runPOS ? processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.pos") : null;
	    		List <String> ss = runSS ? processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss") : null;
	    		List<List<String []>> phraseSS = runPhraseSS ? DataGenerator.readPhraseSimilarityFromFile(fullProcessingPath + ".txt.phrase.emo.ss") : null;
	    		
	    		if (opinion.size() == 0) return null;
	    		
	    		try {
		    		return dataGenerator.generateDirectArcsByPost(
							dataDirectory + "/" + docId /*fullProcessingPath + ".sentences"*/, ids, opinion, polarity, pos, ss, phraseSS, annotations, dataDirectory);
	    		
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			return null;
	    		}
			} catch (Exception e) {
				System.err.println("Error Processing " + docId + ". skipping...");
				e.printStackTrace();
				return null;
			}
    }
    
    /**
     * This generates a chain for the thread where each arc is a pair of posts
     * @param newClassifier
     * @param type
     * @param files
     * @return
     * @throws Exception
     */
    public List<Thread> loadPostChain(boolean newClassifier, String type, String dataDirectory, String modelDirectory) throws Exception {
		
		ArrayList<Thread> threads = new ArrayList<Thread>();
		
		System.out.print("[AgreementClassifier] LOAD ARCS...");
        DataGenerator dataGenerator = new DataGenerator(_dataDirectory,false,true);
        HashMap<String,HashMap<String,Arc>> annotatedArcs = load(newClassifier, dataDirectory, _dataDirectory + "res/" + type + "/", modelDirectory, true);
        System.out.println(annotatedArcs.size());
        
        List<String> opinion = null;
		List<String> polarity = null;
		List<String> pos = null;
		List<String> ss = null;
		List<String> ids = null;
		List<List<String []>> phraseSS = null;
        
		System.out.println("[AgreementClassifier] GENERATE CHAINS");
		
		annotatedArcs = splitSentences(annotatedArcs);

        for (String docId : annotatedArcs.keySet()) {
        	
        	System.out.println(docId);
        	
        	HashMap<String,Arc> doc = annotatedArcs.get(docId);
        	
        	String fullProcessingPath = _dataDirectory + "/res/processed/" + "/processed-" + new File(docId).getName() + "/" + new File(docId).getName();
        	
    		processFile(dataGenerator, docId, _dataDirectory + "/res/", modelDirectory, true, true, true, true, false);
    		
    		ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
    		opinion = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.sbj");
    		polarity = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op");
    		pos = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.pos");
    		ss = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss");
    		phraseSS = DataGenerator.readPhraseSimilarityFromFile(fullProcessingPath + ".txt.phrase.ss");
    		
    		Thread thread = dataGenerator.generateAllPostArcs(
					fullProcessingPath + ".sentences", doc, ids, opinion, polarity, pos, ss, phraseSS, fullProcessingPath);

        	threads.add(thread);
        }
        return threads;
    	
    }
    
    /**
     * This generates a chain for the thread where each arc is a pair of sentences
     * @param newClassifier
     * @param type
     * @param files
     * @param differentiatePosts
     * @return
     * @throws Exception
     */
	public HashMap<String,List<Arc>> loadChain(boolean newClassifier, String type, String files, String modelDirectory, boolean differentiatePosts) throws Exception {
		
		HashMap<String,List<Arc>> arcs = new HashMap<String,List<Arc>>();
		
		System.out.print("[AgreementClassifier] LOAD ARCS...");
        DataGenerator dataGenerator = new DataGenerator(_dataDirectory,false,true);
        HashMap<String,HashMap<String,Arc>> annotatedArcs = load(newClassifier, files, _dataDirectory + "/res/" + type + "/", modelDirectory, true);
        System.out.println(annotatedArcs.size());
        
        HashSet<String> visited = new HashSet<String>();
        
        List<String> ids = null;
        List<String> opinion = null;
		List<String> polarity = null;
		List<String> pos = null;
		List<String> ss = null;
		List<List<String []>> phraseSS = null;
        
		System.out.println("[AgreementClassifier] GENERATE CHAINS");
		
		annotatedArcs = splitSentences(annotatedArcs);

        for (String docId : annotatedArcs.keySet()) {
        	//HashMap<String,Arc> doc : annotatedArcs.values()) {
        	System.out.println(docId);
        	processFile(dataGenerator, docId, _dataDirectory + "/res/", modelDirectory, true, true, true, true, false);
            	
            String fullProcessingPath = _dataDirectory + "/res/processed/" + "/processed-" + new File(docId).getName() + "/" + new File(docId).getName();
            	
            ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
    		opinion = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.sbj");
    		polarity = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op");
    		pos = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.pos");
    		ss = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss");
    		phraseSS = DataGenerator.readPhraseSimilarityFromFile(fullProcessingPath + ".txt.phrase.ss");
    		
        	HashMap<String,Arc> doc = annotatedArcs.get(docId);
        	
        	blog.Blog blog = ProcessBlog.readBlog(fullProcessingPath + ".sentences", docId,docId);
            
        	
        	for (Arc arc : doc.values()) {
        		if (visited.contains(docId + ":" + arc.getSubject().getId() + ":" + arc.getTarget().getId())) continue;

        		List<Arc> postArcs = arcs.get(docId);
        		
        		if (postArcs == null) postArcs = new ArrayList<Arc>();
        		
        		List<Arc> newArcs = dataGenerator.generateSentenceArcsForPost(
        				docId, doc, blog, arc.getSubject().getId(), arc.getTarget().getId(), ids, opinion, polarity, pos, ss, phraseSS, arc.isParent());
        		postArcs.addAll(newArcs);
        		if (differentiatePosts) postArcs.add(null);
        		arcs.put(docId,postArcs);
        		
        		// empty arc to indicate end of chain
        		visited.add(docId + ":" + arc.getSubject().getId() + ":" + arc.getTarget().getId());
        	}
        }
        return arcs;
	}
    
    public AgreementClassifier(String dataDirectory, boolean thread, boolean usePOS, boolean balanced, boolean opinion, boolean polarity,
    		boolean question, boolean sentenceLength, boolean ngram, boolean sentenceSimilarity, boolean phraseSimilarity, boolean twoWay, boolean liwc, boolean sm, boolean entrainment) {
        this._dataDirectory = dataDirectory;
        _thread = thread;
        _POS = usePOS;
        //_balanced = balanced;
        _opinion = opinion;
        _polarity = polarity;
        _question = question;
        _sentenceLength = sentenceLength;
        _ngram = ngram;
        _sentenceSimilarity = sentenceSimilarity;
        _phraseSimilarity = phraseSimilarity;
        _entrainment = entrainment;
        _2way = twoWay;
        
		String liwcDir = null;
		String emoticonDirectory = null;
		String dictionary = null;
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			liwcDir = prop.getProperty("liwc");
			emoticonDirectory = prop.getProperty("emoticons_directory");
			dictionary = prop.getProperty("dictionary");
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (liwc) _liwc = new LIWC(liwcDir);
		if (sm) _sm = new Features(dictionary,emoticonDirectory + "emoticon_sub.txt", emoticonDirectory + "acronyms.txt");
    }

    /**
     * Process the training files to generate the sentence pairs and tag them with
     * pos, opinion, sentence similarity, etc...
     * @param trainingFiles
     * @param res
     * @return
     */
    protected List<String> process(boolean retrain, String trainingDirectory, String outputDirectory, String modelDirectory) {
    	DataGenerator dataGenerator = new DataGenerator(_dataDirectory ,_livejournal, _wikipedia);

    	boolean balanced = true;
    	int numArcs = 0;
    	
    	if (retrain || !new File(outputDirectory + "agreement.sbj.txt").exists() || 
				!new File(outputDirectory + "agreement.tar.txt").exists() || 
				!new File(outputDirectory + "agreement.arcs").exists()) {
		
			HashMap<String,HashMap<String,Arc>> arcs = dataGenerator.generate(balanced, trainingDirectory);
			//arcs = dataGenerator.addDocumentFeatures(arcs);
			dataGenerator.writeArcsToFile(outputDirectory, balanced, arcs);
			numArcs = arcs.size();
		}
    	else {
    		numArcs = processing.GeneralUtils.readLines(outputDirectory + "agreement.arcs").size();
    	}

    	/*if (this._sentenceSimilarity) {
			if (retrain || !new File(outputDirectory + "agreement.sbj.txt.ss").exists()) {
				dataGenerator.computeSentenceSimilarity(outputDirectory + "agreement.sbj.txt");
			}
			if (retrain || !new File(outputDirectory + "agreement.tar.txt.ss").exists()) {
				dataGenerator.computeSentenceSimilarity(outputDirectory + "agreement.tar.txt");
			}
    	}
		
		if (this._opinion) {
			if (retrain || !new File(outputDirectory + "agreement.sbj.txt.emo.con.chk.emo.sbj").exists())
				dataGenerator.addSentiment(outputDirectory + "agreement.sbj.txt",modelDirectory);
			if (retrain || !new File(outputDirectory + "agreement.tar.txt.emo.con.chk.emo.sbj").exists())
				dataGenerator.addSentiment(outputDirectory + "agreement.tar.txt",modelDirectory);
		}
		
		if (this._polarity) {
			if (retrain || !new File(outputDirectory + "agreement.sbj.txt.emo.con.chk.emo.op").exists())
				dataGenerator.addPolarity(outputDirectory + "agreement.sbj.txt",modelDirectory);
			if (!new File(outputDirectory + "agreement.tar.txt.emo.con.chk.emo.op").exists())
				dataGenerator.addPolarity(outputDirectory + "agreement.tar.txt",modelDirectory);
		}
		
		if (this._phraseSimilarity) {
		
			int sizeSBJ = new File(outputDirectory + "agreement.sbj.txt.parse").exists() ? 
					processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.parse").size() : 0;
			int sizeTAR = new File(outputDirectory + "agreement.tar.txt.parse").exists() ? 
							processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.parse").size() : 0;
			
			// add parsing
			if (retrain || !new File(outputDirectory + "agreement.sbj.txt.parse").exists() || 
					sizeSBJ < numArcs)
				OpinionRunner.addParsingandPhrases(outputDirectory +  "agreement.sbj.txt",sizeSBJ);
			if (retrain || !new File(outputDirectory + "agreement.tar.txt.parse").exists() || 
					sizeTAR < numArcs)
				OpinionRunner.addParsingandPhrases(outputDirectory + "agreement.tar.txt",sizeTAR);
			
			// phrase similarity
			if (this._phraseSimilarity && (retrain || !new File(outputDirectory + "agreement.txt.phrase.ss").exists() || 
					processing.GeneralUtils.readLines(outputDirectory + "agreement.txt.phrase.ss").size() < numArcs)) {
				dataGenerator.computePhraseSimilarity(outputDirectory + "agreement.sbj.txt.emo.con.chk",
						outputDirectory + "agreement.tar.txt.emo.con.chk",
						outputDirectory + "agreement.txt.phrase.ss");
			}
		}*/
		List<String> arcs = processing.GeneralUtils.readLines(outputDirectory + "agreement.arcs");	
		
		return arcs;
    }
    
    /**
     * Read all the arc data for training purposes
     * @param res
     * @param arcs
     * @return
     * @throws Exception
     */
    protected HashMap<String,HashMap<String,Arc>> populate(String outputDirectory, List<String> arcs) throws Exception {

    	HashMap<String,HashMap<String,Arc>> trainArcs = new HashMap<String,HashMap<String,Arc>>();
		List<String> phraseSimilarity = null;
		List<String> subjects = null;
		List<String> targets = null;
		// List<String> subjectsParse = null;
		// List<String> targetsParse = null;
		List<String> subjectsOpinion = null;
		List<String> targetsOpinion = null;
		List<String> subjectsPolarity = null;
		List<String> targetsPolarity = null;
   		List<String> subjectsPOS = null;
		List<String> targetsPOS = null;
   		List<String> subjectsSS = null;
		List<String> targetsSS = null;
		
		// boolean balanced = true;
		
	 	subjects = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt");
 		targets = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt");
		//subjectsParse = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.parse");
		//targetsParse = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.parse");
				
		/*if (_opinion || _phraseSimilarity) {
			subjectsOpinion = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.emo.con.chk.emo.sbj");
    		targetsOpinion = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.emo.con.chk.emo.sbj");
		}
		
		if (_polarity || _phraseSimilarity) {
			subjectsPolarity = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.emo.con.chk.emo.op");
    		targetsPolarity = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.emo.con.chk.emo.op");
		}
		
		
		if (_POS) {
			subjectsPOS = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.emo.con.pos");
    		targetsPOS = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.emo.con.pos");				
		}
		
		if (_sentenceSimilarity) {
			subjectsSS = processing.GeneralUtils.readLines(outputDirectory + "agreement.sbj.txt.ss");
			targetsSS = processing.GeneralUtils.readLines(outputDirectory + "agreement.tar.txt.ss");
		}
		
		if (_phraseSimilarity) {
			phraseSimilarity = processing.GeneralUtils.readLines(outputDirectory + "agreement.txt.phrase.ss");
		}*/
		
		for (int index = 0; index < arcs.size(); index++) {
			String [] arcData = arcs.get(index).split(",");
			
			Node s = new Node(arcData[3],subjects.get(index),arcData[4],Integer.valueOf(arcData[5]),Integer.valueOf(arcData[9]),Integer.valueOf(arcData[12]),-1);
			Node t = new Node(arcData[6],targets.get(index),arcData[7],Integer.valueOf(arcData[8]),Integer.valueOf(arcData[10]),Integer.valueOf(arcData[11]),-1);
			
			//a.addParse(targetsParse.get(index),subjectsParse.get(index));
			
			/*if (_opinion || _phraseSimilarity) {
				s.addSingleOpinion(SentimentResult.ProcessLabeledSentence(subjectsOpinion.get(index)));
				t.addSingleOpinion(SentimentResult.ProcessLabeledSentence(targetsOpinion.get(index)));
			}
			if (_polarity | _phraseSimilarity) {
				s.addSinglePolarity(SentimentResult.ProcessLabeledSentence(subjectsPolarity.get(index)));
				t.addSinglePolarity(SentimentResult.ProcessLabeledSentence(targetsPolarity.get(index)));
			}
			if (_POS) {
				s.addSinglePOS(subjectsPOS.get(index));
				t.addSinglePOS(targetsPOS.get(index));
			}*/
			
			// String subjectId, String targetId, String subjectBody, String targetBody, String sbjAuthor, 
			// String tarAuthor, int depthSbj, int depthTar, boolean isParent, int type, int docId
			Arc a = new Arc(s, t, arcData[2].equals("true") ? true : false,Integer.valueOf(arcData[1]));
			
			/*if (_sentenceSimilarity) {
				a.addSingleSentenceSimilarity(
						SentenceSimilarity.cosineSimilarity(subjectsSS.get(index+1).split(" "), targetsSS.get(index+1).split(" ")));
			}
			if (_phraseSimilarity) {
				a.addSinglePhraseSimilarityFromString(phraseSimilarity.get(index));
			}	*/					
			
			String docId = arcData[0];
			
			HashMap<String,Arc> docArcs = trainArcs.containsKey(docId) ? trainArcs.get(docId) : new HashMap<String,Arc>();
			docArcs.put(a.getKey(),a);
			trainArcs.put(docId,docArcs);
		}
		return trainArcs;
    }
    
    private HashMap<String,HashMap<String,Arc>> splitSentences(HashMap<String,HashMap<String,Arc>> arcs) throws Exception {
    	
    	List<Arc> toAdd = new ArrayList<Arc>();
    	List<Arc> toRemove = new ArrayList<Arc>();
    	
    	for (String docId : arcs.keySet()) {
    		
    		HashMap<String,Arc> doc = arcs.get(docId);
    		
    		for (Arc a : doc.values()) {
    			String [] subjectText = DataGenerator.normalizeSentence(a.getSubject().getBody()).split("DELIM");
    			String [] targetText = DataGenerator.normalizeSentence(a.getTarget().getBody()).split("DELIM");
    			
    			if (subjectText.length == 1 && targetText.length == 1) continue;
    			
    			Node subject = a.getSubject();
    			Node target = a.getTarget();
				
    			for (int i = 0; i < subjectText.length; i++) {
    				
 					Node s = new Node(subject.getId(),subject.getBody(),subject.getAuthor(),subject.getDepth(),
 							updateSentencePosition(i, subjectText.length-1, subject.getApproxSentencePosition()),subject.getNumResponses(),i);
    				
    				for (int j = 0; j < targetText.length; j++) {
    					
    					Node t = new Node(target.getId(),target.getBody(),target.getAuthor(),target.getDepth(),
     							updateSentencePosition(i, targetText.length-1, target.getApproxSentencePosition()),target.getNumResponses(),j);
    					
    					Arc n = new Arc(s,t,a.isAncestor(),a.getType());
    					toAdd.add(n);
    				}
    			}
    			toRemove.add(a);
    		}
    		
    		for (Arc n : toAdd) {
        		arcs.get(docId).put(n.getKey(), n);
        	}
        	for (Arc n : toRemove) {
        		arcs.get(docId).remove(n.getKey());
        	}
    	}
    	
    	return arcs;
    }
    
    private int updateSentencePosition(int position, int length, int current) {
    	if (position == 0 && current != 2) return current;
    	else if (position == length && current != 0) return current;
    	return 1;
    }
    
    
    //@SuppressWarnings("unchecked")
    public HashMap<String,HashMap<String,Arc>> load(boolean retrain, String trainingDirectory, String outputDirectory, String modelDirectory, boolean splitSentences) throws Exception {
    	
    	HashMap<String,HashMap<String,Arc>> trainArcs = new HashMap<String,HashMap<String,Arc>>();
    	
    	//String res = "/res/train/";
    	
    	// process the training files and populate 
    	// the data into the arc data structure to be used for training
    	//if (retrain) {	
    			
    		List<String> arcs = process(retrain, trainingDirectory, outputDirectory, modelDirectory);
    		trainArcs = populate(outputDirectory, arcs);
    	//}
    	
    	/*else {
	     	ObjectInputStream istream = new ObjectInputStream(
	    	                new FileInputStream(_dataDirectory + "/" + RES_INSTANCES));
	    	     List<Arc> arcs = (List<Arc>)istream.readObject();
	    	     istream.close();
	    	     
	    	     for (Arc a : arcs) {
	    	    	 HashMap<String,Arc> docArcs = trainArcs.containsKey(a.getDocId()) ? trainArcs.get(a.getDocId()) : new HashMap<String,Arc>();
	    	    	 docArcs.put(a.getSubjectId() + ";" + a.getTargetId() + ";" + a.getSubjectBody() + ";" + a.getTargetBody(),a);
	    	    	 trainArcs.put(a.getDocId(), docArcs);
	    	     }
	    }*/
    	if (splitSentences) trainArcs = splitSentences(trainArcs);
		return trainArcs;
    }
    
    public HashMap<String,HashMap<String,Arc>> load(boolean retrain, String trainingDirectory, String outputDirectory, String modelDirectory) throws Exception {
    	return load(retrain, trainingDirectory, outputDirectory, modelDirectory, false);
    }
    
    protected HashMap<String,List<Arc>> loadAll(HashMap<String,HashMap<String,Arc>> arcs, String processingDirectory, String modelDirectory, boolean differentiatePosts) throws Exception {
    	HashMap<String,List<Arc>> allArcs = new HashMap<String,List<Arc>>();
    	DataGenerator dataGenerator = new DataGenerator(_dataDirectory,_livejournal,_wikipedia);
    	
    	for (String docId : arcs.keySet()) {
    		String fullProcessingPath = processingDirectory + "/processed-" + new File(docId).getName() + "/" + new File(docId).getName();
    		System.err.println("LOADING ALL ARCS FROM: " + docId);
    		
    		processFile(dataGenerator,docId, _dataDirectory + "/res/", modelDirectory, true, true, true, true, false);
    		
    		// generate whether we use or not.
    		List<String> ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
    		List<String> opinion = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.sbj");
    		List<String> polarity = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op");
    		List<String> pos = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.con.pos");	
    		List<String>ss = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss");
    		List<List<String []>> phraseSS = DataGenerator.readPhraseSimilarityFromFile(fullProcessingPath + ".txt.phrase.ss");

        	arcs = splitSentences(arcs);
    		List<Arc> newArcs = dataGenerator.generateAllSentenceArcs(
    				fullProcessingPath + ".sentences", arcs != null ? arcs.get(docId) : null, ids, opinion, polarity, pos, ss, phraseSS, differentiatePosts, processingDirectory);
    		printStats(newArcs,arcs.get(docId));
    		allArcs.put(docId,newArcs);
    		//break;
    	}
    	return allArcs;
    }
    
    protected void printStats(List<Arc> arcs, HashMap<String,Arc> annotated) {
    	int agree = 0;
    	for (Arc a : arcs) {
    		if (a.getType() == Arc.AGREEMENT || a.getType() == Arc.DISAGREEMENT) {
    			agree++;
    	   		//System.out.println(a);
    		}
    	}
    	
    	//System.out.println("\n----\n");
    	
    	int agree_a = 0;
    	
    	for (Arc a : annotated.values()) {
    		if (a.getType() == Arc.AGREEMENT || a.getType() == Arc.DISAGREEMENT) {
    			agree_a++;
        		//System.out.println(a);
    		}
    	}
    	
    	System.out.println(agree + " arcs in list. " + agree_a + " in annotations. " + arcs.size() + " arcs in total.");
    }
    
    protected List<Arc> loadTest(String testFile, String processingDirectory, String modelDirectory, boolean differentiatePosts) {
    	DataGenerator dataGenerator = new DataGenerator(_dataDirectory,_livejournal,_wikipedia);
    	String fullProcessingPath = processingDirectory + "/processed-" + new File(testFile).getName() + "/" + new File(testFile).getName();
    	
		processFile(dataGenerator,testFile,processingDirectory, modelDirectory, true, true, true, true, false);
		
		// generate whether we use or not.
		List<String> ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
		List<String> opinion = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.sbj");
		List<String> polarity = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op");
		List<String> pos = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.emo.con.pos");	
		List<String>ss = processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss");
		List<List<String []>> phraseSS = DataGenerator.readPhraseSimilarityFromFile(fullProcessingPath + ".txt.phrase.ss");

				
    	// 2. generate arcs and as going along add in preprocessed items that are wanted
    	List<Arc> testArcs = dataGenerator.generateAllSentenceArcs(fullProcessingPath + ".sentences", null, ids, opinion, polarity, pos, ss, phraseSS, differentiatePosts, _dataDirectory);
    	
    	// 3. return arcs
    	return testArcs;
    }
    
    public static void processFile(DataGenerator dataGenerator, String file, String processingDirectory, String modelDirectory,
    		boolean subjectivity, boolean polarity, boolean ss, boolean phraseSS, boolean unannotated) {
    	
    	
    	String fullProcessingPath = processingDirectory + "/processed-" + new File(file).getName() + "/" + new File(file).getName();
    	
    	new File(processingDirectory + "/processed-" + new File(file).getName() + "/").mkdirs();
    	
    	if (!new File(fullProcessingPath + ".sentences").exists()) {
	    	try {
	    		XMLUtilities.addSentenceTagsToBlogs(file,fullProcessingPath + ".sentences",new EmoticonDictionary(),new Contractions(),DataGenerator.SENT_DELIMITERS);
	    	} catch (Exception e) {
	    		System.err.println("[AgreementClassifier] " + e);
	    		e.printStackTrace();
	    	}
    	}
    	if (!new File(fullProcessingPath + ".txt").exists()) {
			Document entry = XMLUtilities.getDocument(fullProcessingPath + ".sentences");
			ArrayList<String> sentences = XMLUtilities.getSentences(entry);
			XMLUtilities.writeSentencesToTxtFile(fullProcessingPath,sentences);
    	}
    	if (!new File(fullProcessingPath + ".ids").exists()) {
    		Document entry = XMLUtilities.getDocument(fullProcessingPath + ".sentences");
    		ArrayList<String> ids = XMLUtilities.getIDs(entry);
			XMLUtilities.writeSentencesToTxtFile(fullProcessingPath,".ids",ids);
    	}
    	
    	List<String> ids = processing.GeneralUtils.readLines(fullProcessingPath + ".ids");
		// skip... file too long
		if (ids.size() > 500 && !unannotated) return; // && file.indexOf("create_debate") >= 0) return; 
    	
    	// 1. preprocess file for pos, sentiment etc...
    	if (subjectivity && !new File(fullProcessingPath + ".txt.sbj").exists() || processing.GeneralUtils.readLines(fullProcessingPath + ".txt.sbj").size() == 0) {
    		dataGenerator.addSentiment(fullProcessingPath + ".txt", modelDirectory);
		}
		if (polarity && !new File(fullProcessingPath + ".txt.op").exists() || processing.GeneralUtils.readLines(fullProcessingPath + ".txt.op").size() == 0) {
			dataGenerator.addPolarity(fullProcessingPath + ".txt", modelDirectory);
		}
		// 2. add sentence similarity
		if (ss && !new File(fullProcessingPath + ".txt.ss").exists() ||  processing.GeneralUtils.readLines(fullProcessingPath + ".txt.ss").size() == 0) {
			dataGenerator.computeSentenceSimilarity(fullProcessingPath + ".txt");
		}
		// 3. add phrase ss
		if (phraseSS && !new File(fullProcessingPath + ".txt.phrase.emo.ss").exists() ||  processing.GeneralUtils.readLines(fullProcessingPath + ".txt.phrase.emo.ss").size() == 0) { // || !new File(fullProcessingPath + ".txt.phrase.emo").exists()) {
			//OpinionRunner.preprocess(fullProcessingPath + ".txt",false);
			dataGenerator.computePhraseSimilarity(fullProcessingPath + ".txt.emo.phrase",fullProcessingPath + ".txt.phrase");
		}
    }
}
